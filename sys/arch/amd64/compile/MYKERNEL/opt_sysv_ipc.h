#define	SYSV_IPC	1
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_SYSV_IPC
 .global _KERNEL_OPT_SYSV_IPC
 .equiv _KERNEL_OPT_SYSV_IPC,0x1
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_SYSV_IPC\n .global _KERNEL_OPT_SYSV_IPC\n .equiv _KERNEL_OPT_SYSV_IPC,0x1\n .endif");
#endif
