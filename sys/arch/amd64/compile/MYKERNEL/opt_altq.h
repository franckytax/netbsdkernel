/* option `WFQ_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_WFQ_DEBUG
 .global _KERNEL_OPT_WFQ_DEBUG
 .equiv _KERNEL_OPT_WFQ_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_WFQ_DEBUG\n .global _KERNEL_OPT_WFQ_DEBUG\n .equiv _KERNEL_OPT_WFQ_DEBUG,0x6e074def\n .endif");
#endif
/* option `RIO_STATS' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_RIO_STATS
 .global _KERNEL_OPT_RIO_STATS
 .equiv _KERNEL_OPT_RIO_STATS,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_RIO_STATS\n .global _KERNEL_OPT_RIO_STATS\n .equiv _KERNEL_OPT_RIO_STATS,0x6e074def\n .endif");
#endif
/* option `RED_STATS' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_RED_STATS
 .global _KERNEL_OPT_RED_STATS
 .equiv _KERNEL_OPT_RED_STATS,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_RED_STATS\n .global _KERNEL_OPT_RED_STATS\n .equiv _KERNEL_OPT_RED_STATS,0x6e074def\n .endif");
#endif
/* option `FV_STATS' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FV_STATS
 .global _KERNEL_OPT_FV_STATS
 .equiv _KERNEL_OPT_FV_STATS,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FV_STATS\n .global _KERNEL_OPT_FV_STATS\n .equiv _KERNEL_OPT_FV_STATS,0x6e074def\n .endif");
#endif
/* option `FIFOQ_STATS' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FIFOQ_STATS
 .global _KERNEL_OPT_FIFOQ_STATS
 .equiv _KERNEL_OPT_FIFOQ_STATS,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FIFOQ_STATS\n .global _KERNEL_OPT_FIFOQ_STATS\n .equiv _KERNEL_OPT_FIFOQ_STATS,0x6e074def\n .endif");
#endif
/* option `CBQ_TRACE' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_CBQ_TRACE
 .global _KERNEL_OPT_CBQ_TRACE
 .equiv _KERNEL_OPT_CBQ_TRACE,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_CBQ_TRACE\n .global _KERNEL_OPT_CBQ_TRACE\n .equiv _KERNEL_OPT_CBQ_TRACE,0x6e074def\n .endif");
#endif
/* option `BORROW_OFFTIME' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_BORROW_OFFTIME
 .global _KERNEL_OPT_BORROW_OFFTIME
 .equiv _KERNEL_OPT_BORROW_OFFTIME,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_BORROW_OFFTIME\n .global _KERNEL_OPT_BORROW_OFFTIME\n .equiv _KERNEL_OPT_BORROW_OFFTIME,0x6e074def\n .endif");
#endif
/* option `ADJUST_CUTOFF' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_ADJUST_CUTOFF
 .global _KERNEL_OPT_ADJUST_CUTOFF
 .equiv _KERNEL_OPT_ADJUST_CUTOFF,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_ADJUST_CUTOFF\n .global _KERNEL_OPT_ADJUST_CUTOFF\n .equiv _KERNEL_OPT_ADJUST_CUTOFF,0x6e074def\n .endif");
#endif
/* option `ALTQ_WFQ' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_ALTQ_WFQ
 .global _KERNEL_OPT_ALTQ_WFQ
 .equiv _KERNEL_OPT_ALTQ_WFQ,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_ALTQ_WFQ\n .global _KERNEL_OPT_ALTQ_WFQ\n .equiv _KERNEL_OPT_ALTQ_WFQ,0x6e074def\n .endif");
#endif
/* option `ALTQ_JOBS' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_ALTQ_JOBS
 .global _KERNEL_OPT_ALTQ_JOBS
 .equiv _KERNEL_OPT_ALTQ_JOBS,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_ALTQ_JOBS\n .global _KERNEL_OPT_ALTQ_JOBS\n .equiv _KERNEL_OPT_ALTQ_JOBS,0x6e074def\n .endif");
#endif
/* option `ALTQ_PRIQ' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_ALTQ_PRIQ
 .global _KERNEL_OPT_ALTQ_PRIQ
 .equiv _KERNEL_OPT_ALTQ_PRIQ,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_ALTQ_PRIQ\n .global _KERNEL_OPT_ALTQ_PRIQ\n .equiv _KERNEL_OPT_ALTQ_PRIQ,0x6e074def\n .endif");
#endif
/* option `ALTQ_RIO' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_ALTQ_RIO
 .global _KERNEL_OPT_ALTQ_RIO
 .equiv _KERNEL_OPT_ALTQ_RIO,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_ALTQ_RIO\n .global _KERNEL_OPT_ALTQ_RIO\n .equiv _KERNEL_OPT_ALTQ_RIO,0x6e074def\n .endif");
#endif
/* option `ALTQ_RED' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_ALTQ_RED
 .global _KERNEL_OPT_ALTQ_RED
 .equiv _KERNEL_OPT_ALTQ_RED,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_ALTQ_RED\n .global _KERNEL_OPT_ALTQ_RED\n .equiv _KERNEL_OPT_ALTQ_RED,0x6e074def\n .endif");
#endif
/* option `ALTQ_LOCALQ' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_ALTQ_LOCALQ
 .global _KERNEL_OPT_ALTQ_LOCALQ
 .equiv _KERNEL_OPT_ALTQ_LOCALQ,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_ALTQ_LOCALQ\n .global _KERNEL_OPT_ALTQ_LOCALQ\n .equiv _KERNEL_OPT_ALTQ_LOCALQ,0x6e074def\n .endif");
#endif
/* option `ALTQ_IPSEC' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_ALTQ_IPSEC
 .global _KERNEL_OPT_ALTQ_IPSEC
 .equiv _KERNEL_OPT_ALTQ_IPSEC,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_ALTQ_IPSEC\n .global _KERNEL_OPT_ALTQ_IPSEC\n .equiv _KERNEL_OPT_ALTQ_IPSEC,0x6e074def\n .endif");
#endif
/* option `ALTQ_HFSC' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_ALTQ_HFSC
 .global _KERNEL_OPT_ALTQ_HFSC
 .equiv _KERNEL_OPT_ALTQ_HFSC,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_ALTQ_HFSC\n .global _KERNEL_OPT_ALTQ_HFSC\n .equiv _KERNEL_OPT_ALTQ_HFSC,0x6e074def\n .endif");
#endif
/* option `ALTQ_FLOWVALVE' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_ALTQ_FLOWVALVE
 .global _KERNEL_OPT_ALTQ_FLOWVALVE
 .equiv _KERNEL_OPT_ALTQ_FLOWVALVE,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_ALTQ_FLOWVALVE\n .global _KERNEL_OPT_ALTQ_FLOWVALVE\n .equiv _KERNEL_OPT_ALTQ_FLOWVALVE,0x6e074def\n .endif");
#endif
/* option `ALTQ_FIFOQ' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_ALTQ_FIFOQ
 .global _KERNEL_OPT_ALTQ_FIFOQ
 .equiv _KERNEL_OPT_ALTQ_FIFOQ,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_ALTQ_FIFOQ\n .global _KERNEL_OPT_ALTQ_FIFOQ\n .equiv _KERNEL_OPT_ALTQ_FIFOQ,0x6e074def\n .endif");
#endif
/* option `ALTQ_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_ALTQ_DEBUG
 .global _KERNEL_OPT_ALTQ_DEBUG
 .equiv _KERNEL_OPT_ALTQ_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_ALTQ_DEBUG\n .global _KERNEL_OPT_ALTQ_DEBUG\n .equiv _KERNEL_OPT_ALTQ_DEBUG,0x6e074def\n .endif");
#endif
/* option `ALTQ_CDNR' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_ALTQ_CDNR
 .global _KERNEL_OPT_ALTQ_CDNR
 .equiv _KERNEL_OPT_ALTQ_CDNR,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_ALTQ_CDNR\n .global _KERNEL_OPT_ALTQ_CDNR\n .equiv _KERNEL_OPT_ALTQ_CDNR,0x6e074def\n .endif");
#endif
/* option `ALTQ_CBQ' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_ALTQ_CBQ
 .global _KERNEL_OPT_ALTQ_CBQ
 .equiv _KERNEL_OPT_ALTQ_CBQ,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_ALTQ_CBQ\n .global _KERNEL_OPT_ALTQ_CBQ\n .equiv _KERNEL_OPT_ALTQ_CBQ,0x6e074def\n .endif");
#endif
/* option `ALTQ_BLUE' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_ALTQ_BLUE
 .global _KERNEL_OPT_ALTQ_BLUE
 .equiv _KERNEL_OPT_ALTQ_BLUE,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_ALTQ_BLUE\n .global _KERNEL_OPT_ALTQ_BLUE\n .equiv _KERNEL_OPT_ALTQ_BLUE,0x6e074def\n .endif");
#endif
/* option `ALTQ_AFMAP' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_ALTQ_AFMAP
 .global _KERNEL_OPT_ALTQ_AFMAP
 .equiv _KERNEL_OPT_ALTQ_AFMAP,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_ALTQ_AFMAP\n .global _KERNEL_OPT_ALTQ_AFMAP\n .equiv _KERNEL_OPT_ALTQ_AFMAP,0x6e074def\n .endif");
#endif
