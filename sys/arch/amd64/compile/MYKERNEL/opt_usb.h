/* option `USB_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_USB_DEBUG
 .global _KERNEL_OPT_USB_DEBUG
 .equiv _KERNEL_OPT_USB_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_USB_DEBUG\n .global _KERNEL_OPT_USB_DEBUG\n .equiv _KERNEL_OPT_USB_DEBUG,0x6e074def\n .endif");
#endif
/* option `USBHIST_SIZE' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_USBHIST_SIZE
 .global _KERNEL_OPT_USBHIST_SIZE
 .equiv _KERNEL_OPT_USBHIST_SIZE,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_USBHIST_SIZE\n .global _KERNEL_OPT_USBHIST_SIZE\n .equiv _KERNEL_OPT_USBHIST_SIZE,0x6e074def\n .endif");
#endif
/* option `USB_FRAG_DMA_WORKAROUND' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_USB_FRAG_DMA_WORKAROUND
 .global _KERNEL_OPT_USB_FRAG_DMA_WORKAROUND
 .equiv _KERNEL_OPT_USB_FRAG_DMA_WORKAROUND,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_USB_FRAG_DMA_WORKAROUND\n .global _KERNEL_OPT_USB_FRAG_DMA_WORKAROUND\n .equiv _KERNEL_OPT_USB_FRAG_DMA_WORKAROUND,0x6e074def\n .endif");
#endif
/* option `EHCI_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_EHCI_DEBUG
 .global _KERNEL_OPT_EHCI_DEBUG
 .equiv _KERNEL_OPT_EHCI_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_EHCI_DEBUG\n .global _KERNEL_OPT_EHCI_DEBUG\n .equiv _KERNEL_OPT_EHCI_DEBUG,0x6e074def\n .endif");
#endif
/* option `OHCI_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_OHCI_DEBUG
 .global _KERNEL_OPT_OHCI_DEBUG
 .equiv _KERNEL_OPT_OHCI_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_OHCI_DEBUG\n .global _KERNEL_OPT_OHCI_DEBUG\n .equiv _KERNEL_OPT_OHCI_DEBUG,0x6e074def\n .endif");
#endif
/* option `UHCI_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UHCI_DEBUG
 .global _KERNEL_OPT_UHCI_DEBUG
 .equiv _KERNEL_OPT_UHCI_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UHCI_DEBUG\n .global _KERNEL_OPT_UHCI_DEBUG\n .equiv _KERNEL_OPT_UHCI_DEBUG,0x6e074def\n .endif");
#endif
/* option `DWC2_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_DWC2_DEBUG
 .global _KERNEL_OPT_DWC2_DEBUG
 .equiv _KERNEL_OPT_DWC2_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_DWC2_DEBUG\n .global _KERNEL_OPT_DWC2_DEBUG\n .equiv _KERNEL_OPT_DWC2_DEBUG,0x6e074def\n .endif");
#endif
/* option `XHCI_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_XHCI_DEBUG
 .global _KERNEL_OPT_XHCI_DEBUG
 .equiv _KERNEL_OPT_XHCI_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_XHCI_DEBUG\n .global _KERNEL_OPT_XHCI_DEBUG\n .equiv _KERNEL_OPT_XHCI_DEBUG,0x6e074def\n .endif");
#endif
/* option `MOTG_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_MOTG_DEBUG
 .global _KERNEL_OPT_MOTG_DEBUG
 .equiv _KERNEL_OPT_MOTG_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_MOTG_DEBUG\n .global _KERNEL_OPT_MOTG_DEBUG\n .equiv _KERNEL_OPT_MOTG_DEBUG,0x6e074def\n .endif");
#endif
/* option `SLHCI_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_SLHCI_DEBUG
 .global _KERNEL_OPT_SLHCI_DEBUG
 .equiv _KERNEL_OPT_SLHCI_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_SLHCI_DEBUG\n .global _KERNEL_OPT_SLHCI_DEBUG\n .equiv _KERNEL_OPT_SLHCI_DEBUG,0x6e074def\n .endif");
#endif
/* option `UHUB_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UHUB_DEBUG
 .global _KERNEL_OPT_UHUB_DEBUG
 .equiv _KERNEL_OPT_UHUB_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UHUB_DEBUG\n .global _KERNEL_OPT_UHUB_DEBUG\n .equiv _KERNEL_OPT_UHUB_DEBUG,0x6e074def\n .endif");
#endif
/* option `ATU_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_ATU_DEBUG
 .global _KERNEL_OPT_ATU_DEBUG
 .equiv _KERNEL_OPT_ATU_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_ATU_DEBUG\n .global _KERNEL_OPT_ATU_DEBUG\n .equiv _KERNEL_OPT_ATU_DEBUG,0x6e074def\n .endif");
#endif
/* option `AUE_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_AUE_DEBUG
 .global _KERNEL_OPT_AUE_DEBUG
 .equiv _KERNEL_OPT_AUE_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_AUE_DEBUG\n .global _KERNEL_OPT_AUE_DEBUG\n .equiv _KERNEL_OPT_AUE_DEBUG,0x6e074def\n .endif");
#endif
/* option `AUVITEK_I2C_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_AUVITEK_I2C_DEBUG
 .global _KERNEL_OPT_AUVITEK_I2C_DEBUG
 .equiv _KERNEL_OPT_AUVITEK_I2C_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_AUVITEK_I2C_DEBUG\n .global _KERNEL_OPT_AUVITEK_I2C_DEBUG\n .equiv _KERNEL_OPT_AUVITEK_I2C_DEBUG,0x6e074def\n .endif");
#endif
/* option `AXE_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_AXE_DEBUG
 .global _KERNEL_OPT_AXE_DEBUG
 .equiv _KERNEL_OPT_AXE_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_AXE_DEBUG\n .global _KERNEL_OPT_AXE_DEBUG\n .equiv _KERNEL_OPT_AXE_DEBUG,0x6e074def\n .endif");
#endif
/* option `AXEN_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_AXEN_DEBUG
 .global _KERNEL_OPT_AXEN_DEBUG
 .equiv _KERNEL_OPT_AXEN_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_AXEN_DEBUG\n .global _KERNEL_OPT_AXEN_DEBUG\n .equiv _KERNEL_OPT_AXEN_DEBUG,0x6e074def\n .endif");
#endif
/* option `CUE_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_CUE_DEBUG
 .global _KERNEL_OPT_CUE_DEBUG
 .equiv _KERNEL_OPT_CUE_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_CUE_DEBUG\n .global _KERNEL_OPT_CUE_DEBUG\n .equiv _KERNEL_OPT_CUE_DEBUG,0x6e074def\n .endif");
#endif
/* option `EZLOAD_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_EZLOAD_DEBUG
 .global _KERNEL_OPT_EZLOAD_DEBUG
 .equiv _KERNEL_OPT_EZLOAD_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_EZLOAD_DEBUG\n .global _KERNEL_OPT_EZLOAD_DEBUG\n .equiv _KERNEL_OPT_EZLOAD_DEBUG,0x6e074def\n .endif");
#endif
/* option `KUE_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_KUE_DEBUG
 .global _KERNEL_OPT_KUE_DEBUG
 .equiv _KERNEL_OPT_KUE_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_KUE_DEBUG\n .global _KERNEL_OPT_KUE_DEBUG\n .equiv _KERNEL_OPT_KUE_DEBUG,0x6e074def\n .endif");
#endif
/* option `OTUS_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_OTUS_DEBUG
 .global _KERNEL_OPT_OTUS_DEBUG
 .equiv _KERNEL_OPT_OTUS_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_OTUS_DEBUG\n .global _KERNEL_OPT_OTUS_DEBUG\n .equiv _KERNEL_OPT_OTUS_DEBUG,0x6e074def\n .endif");
#endif
/* option `RUM_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_RUM_DEBUG
 .global _KERNEL_OPT_RUM_DEBUG
 .equiv _KERNEL_OPT_RUM_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_RUM_DEBUG\n .global _KERNEL_OPT_RUM_DEBUG\n .equiv _KERNEL_OPT_RUM_DEBUG,0x6e074def\n .endif");
#endif
/* option `RUN_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_RUN_DEBUG
 .global _KERNEL_OPT_RUN_DEBUG
 .equiv _KERNEL_OPT_RUN_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_RUN_DEBUG\n .global _KERNEL_OPT_RUN_DEBUG\n .equiv _KERNEL_OPT_RUN_DEBUG,0x6e074def\n .endif");
#endif
/* option `UARK_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UARK_DEBUG
 .global _KERNEL_OPT_UARK_DEBUG
 .equiv _KERNEL_OPT_UARK_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UARK_DEBUG\n .global _KERNEL_OPT_UARK_DEBUG\n .equiv _KERNEL_OPT_UARK_DEBUG,0x6e074def\n .endif");
#endif
/* option `UATP_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UATP_DEBUG
 .global _KERNEL_OPT_UATP_DEBUG
 .equiv _KERNEL_OPT_UATP_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UATP_DEBUG\n .global _KERNEL_OPT_UATP_DEBUG\n .equiv _KERNEL_OPT_UATP_DEBUG,0x6e074def\n .endif");
#endif
/* option `UAUDIO_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UAUDIO_DEBUG
 .global _KERNEL_OPT_UAUDIO_DEBUG
 .equiv _KERNEL_OPT_UAUDIO_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UAUDIO_DEBUG\n .global _KERNEL_OPT_UAUDIO_DEBUG\n .equiv _KERNEL_OPT_UAUDIO_DEBUG,0x6e074def\n .endif");
#endif
/* option `UBERRY_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UBERRY_DEBUG
 .global _KERNEL_OPT_UBERRY_DEBUG
 .equiv _KERNEL_OPT_UBERRY_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UBERRY_DEBUG\n .global _KERNEL_OPT_UBERRY_DEBUG\n .equiv _KERNEL_OPT_UBERRY_DEBUG,0x6e074def\n .endif");
#endif
/* option `UBSA_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UBSA_DEBUG
 .global _KERNEL_OPT_UBSA_DEBUG
 .equiv _KERNEL_OPT_UBSA_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UBSA_DEBUG\n .global _KERNEL_OPT_UBSA_DEBUG\n .equiv _KERNEL_OPT_UBSA_DEBUG,0x6e074def\n .endif");
#endif
/* option `UBT_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UBT_DEBUG
 .global _KERNEL_OPT_UBT_DEBUG
 .equiv _KERNEL_OPT_UBT_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UBT_DEBUG\n .global _KERNEL_OPT_UBT_DEBUG\n .equiv _KERNEL_OPT_UBT_DEBUG,0x6e074def\n .endif");
#endif
/* option `UCHCOM_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UCHCOM_DEBUG
 .global _KERNEL_OPT_UCHCOM_DEBUG
 .equiv _KERNEL_OPT_UCHCOM_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UCHCOM_DEBUG\n .global _KERNEL_OPT_UCHCOM_DEBUG\n .equiv _KERNEL_OPT_UCHCOM_DEBUG,0x6e074def\n .endif");
#endif
/* option `UCOM_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UCOM_DEBUG
 .global _KERNEL_OPT_UCOM_DEBUG
 .equiv _KERNEL_OPT_UCOM_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UCOM_DEBUG\n .global _KERNEL_OPT_UCOM_DEBUG\n .equiv _KERNEL_OPT_UCOM_DEBUG,0x6e074def\n .endif");
#endif
/* option `UCYCOM_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UCYCOM_DEBUG
 .global _KERNEL_OPT_UCYCOM_DEBUG
 .equiv _KERNEL_OPT_UCYCOM_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UCYCOM_DEBUG\n .global _KERNEL_OPT_UCYCOM_DEBUG\n .equiv _KERNEL_OPT_UCYCOM_DEBUG,0x6e074def\n .endif");
#endif
/* option `UDAV_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UDAV_DEBUG
 .global _KERNEL_OPT_UDAV_DEBUG
 .equiv _KERNEL_OPT_UDAV_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UDAV_DEBUG\n .global _KERNEL_OPT_UDAV_DEBUG\n .equiv _KERNEL_OPT_UDAV_DEBUG,0x6e074def\n .endif");
#endif
/* option `UDL_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UDL_DEBUG
 .global _KERNEL_OPT_UDL_DEBUG
 .equiv _KERNEL_OPT_UDL_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UDL_DEBUG\n .global _KERNEL_OPT_UDL_DEBUG\n .equiv _KERNEL_OPT_UDL_DEBUG,0x6e074def\n .endif");
#endif
/* option `UDSBR_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UDSBR_DEBUG
 .global _KERNEL_OPT_UDSBR_DEBUG
 .equiv _KERNEL_OPT_UDSBR_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UDSBR_DEBUG\n .global _KERNEL_OPT_UDSBR_DEBUG\n .equiv _KERNEL_OPT_UDSBR_DEBUG,0x6e074def\n .endif");
#endif
/* option `UFTDI_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UFTDI_DEBUG
 .global _KERNEL_OPT_UFTDI_DEBUG
 .equiv _KERNEL_OPT_UFTDI_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UFTDI_DEBUG\n .global _KERNEL_OPT_UFTDI_DEBUG\n .equiv _KERNEL_OPT_UFTDI_DEBUG,0x6e074def\n .endif");
#endif
/* option `UGENSA_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UGENSA_DEBUG
 .global _KERNEL_OPT_UGENSA_DEBUG
 .equiv _KERNEL_OPT_UGENSA_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UGENSA_DEBUG\n .global _KERNEL_OPT_UGENSA_DEBUG\n .equiv _KERNEL_OPT_UGENSA_DEBUG,0x6e074def\n .endif");
#endif
/* option `UGEN_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UGEN_DEBUG
 .global _KERNEL_OPT_UGEN_DEBUG
 .equiv _KERNEL_OPT_UGEN_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UGEN_DEBUG\n .global _KERNEL_OPT_UGEN_DEBUG\n .equiv _KERNEL_OPT_UGEN_DEBUG,0x6e074def\n .endif");
#endif
/* option `UHIDEV_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UHIDEV_DEBUG
 .global _KERNEL_OPT_UHIDEV_DEBUG
 .equiv _KERNEL_OPT_UHIDEV_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UHIDEV_DEBUG\n .global _KERNEL_OPT_UHIDEV_DEBUG\n .equiv _KERNEL_OPT_UHIDEV_DEBUG,0x6e074def\n .endif");
#endif
/* option `UHID_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UHID_DEBUG
 .global _KERNEL_OPT_UHID_DEBUG
 .equiv _KERNEL_OPT_UHID_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UHID_DEBUG\n .global _KERNEL_OPT_UHID_DEBUG\n .equiv _KERNEL_OPT_UHID_DEBUG,0x6e074def\n .endif");
#endif
/* option `UHMODEM_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UHMODEM_DEBUG
 .global _KERNEL_OPT_UHMODEM_DEBUG
 .equiv _KERNEL_OPT_UHMODEM_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UHMODEM_DEBUG\n .global _KERNEL_OPT_UHMODEM_DEBUG\n .equiv _KERNEL_OPT_UHMODEM_DEBUG,0x6e074def\n .endif");
#endif
/* option `UHSO_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UHSO_DEBUG
 .global _KERNEL_OPT_UHSO_DEBUG
 .equiv _KERNEL_OPT_UHSO_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UHSO_DEBUG\n .global _KERNEL_OPT_UHSO_DEBUG\n .equiv _KERNEL_OPT_UHSO_DEBUG,0x6e074def\n .endif");
#endif
/* option `UIPAD_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UIPAD_DEBUG
 .global _KERNEL_OPT_UIPAD_DEBUG
 .equiv _KERNEL_OPT_UIPAD_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UIPAD_DEBUG\n .global _KERNEL_OPT_UIPAD_DEBUG\n .equiv _KERNEL_OPT_UIPAD_DEBUG,0x6e074def\n .endif");
#endif
/* option `UIPAQ_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UIPAQ_DEBUG
 .global _KERNEL_OPT_UIPAQ_DEBUG
 .equiv _KERNEL_OPT_UIPAQ_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UIPAQ_DEBUG\n .global _KERNEL_OPT_UIPAQ_DEBUG\n .equiv _KERNEL_OPT_UIPAQ_DEBUG,0x6e074def\n .endif");
#endif
/* option `UIRDA_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UIRDA_DEBUG
 .global _KERNEL_OPT_UIRDA_DEBUG
 .equiv _KERNEL_OPT_UIRDA_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UIRDA_DEBUG\n .global _KERNEL_OPT_UIRDA_DEBUG\n .equiv _KERNEL_OPT_UIRDA_DEBUG,0x6e074def\n .endif");
#endif
/* option `UISDATA_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UISDATA_DEBUG
 .global _KERNEL_OPT_UISDATA_DEBUG
 .equiv _KERNEL_OPT_UISDATA_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UISDATA_DEBUG\n .global _KERNEL_OPT_UISDATA_DEBUG\n .equiv _KERNEL_OPT_UISDATA_DEBUG,0x6e074def\n .endif");
#endif
/* option `UKBD_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UKBD_DEBUG
 .global _KERNEL_OPT_UKBD_DEBUG
 .equiv _KERNEL_OPT_UKBD_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UKBD_DEBUG\n .global _KERNEL_OPT_UKBD_DEBUG\n .equiv _KERNEL_OPT_UKBD_DEBUG,0x6e074def\n .endif");
#endif
/* option `UKYOPON_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UKYOPON_DEBUG
 .global _KERNEL_OPT_UKYOPON_DEBUG
 .equiv _KERNEL_OPT_UKYOPON_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UKYOPON_DEBUG\n .global _KERNEL_OPT_UKYOPON_DEBUG\n .equiv _KERNEL_OPT_UKYOPON_DEBUG,0x6e074def\n .endif");
#endif
/* option `ULPT_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_ULPT_DEBUG
 .global _KERNEL_OPT_ULPT_DEBUG
 .equiv _KERNEL_OPT_ULPT_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_ULPT_DEBUG\n .global _KERNEL_OPT_ULPT_DEBUG\n .equiv _KERNEL_OPT_ULPT_DEBUG,0x6e074def\n .endif");
#endif
/* option `UMASS_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UMASS_DEBUG
 .global _KERNEL_OPT_UMASS_DEBUG
 .equiv _KERNEL_OPT_UMASS_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UMASS_DEBUG\n .global _KERNEL_OPT_UMASS_DEBUG\n .equiv _KERNEL_OPT_UMASS_DEBUG,0x6e074def\n .endif");
#endif
/* option `UMCT_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UMCT_DEBUG
 .global _KERNEL_OPT_UMCT_DEBUG
 .equiv _KERNEL_OPT_UMCT_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UMCT_DEBUG\n .global _KERNEL_OPT_UMCT_DEBUG\n .equiv _KERNEL_OPT_UMCT_DEBUG,0x6e074def\n .endif");
#endif
/* option `UMIDIQUIRK_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UMIDIQUIRK_DEBUG
 .global _KERNEL_OPT_UMIDIQUIRK_DEBUG
 .equiv _KERNEL_OPT_UMIDIQUIRK_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UMIDIQUIRK_DEBUG\n .global _KERNEL_OPT_UMIDIQUIRK_DEBUG\n .equiv _KERNEL_OPT_UMIDIQUIRK_DEBUG,0x6e074def\n .endif");
#endif
/* option `UMIDI_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UMIDI_DEBUG
 .global _KERNEL_OPT_UMIDI_DEBUG
 .equiv _KERNEL_OPT_UMIDI_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UMIDI_DEBUG\n .global _KERNEL_OPT_UMIDI_DEBUG\n .equiv _KERNEL_OPT_UMIDI_DEBUG,0x6e074def\n .endif");
#endif
/* option `UMODEM_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UMODEM_DEBUG
 .global _KERNEL_OPT_UMODEM_DEBUG
 .equiv _KERNEL_OPT_UMODEM_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UMODEM_DEBUG\n .global _KERNEL_OPT_UMODEM_DEBUG\n .equiv _KERNEL_OPT_UMODEM_DEBUG,0x6e074def\n .endif");
#endif
/* option `UMS_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UMS_DEBUG
 .global _KERNEL_OPT_UMS_DEBUG
 .equiv _KERNEL_OPT_UMS_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UMS_DEBUG\n .global _KERNEL_OPT_UMS_DEBUG\n .equiv _KERNEL_OPT_UMS_DEBUG,0x6e074def\n .endif");
#endif
/* option `UPGT_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UPGT_DEBUG
 .global _KERNEL_OPT_UPGT_DEBUG
 .equiv _KERNEL_OPT_UPGT_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UPGT_DEBUG\n .global _KERNEL_OPT_UPGT_DEBUG\n .equiv _KERNEL_OPT_UPGT_DEBUG,0x6e074def\n .endif");
#endif
/* option `UPLCOM_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UPLCOM_DEBUG
 .global _KERNEL_OPT_UPLCOM_DEBUG
 .equiv _KERNEL_OPT_UPLCOM_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UPLCOM_DEBUG\n .global _KERNEL_OPT_UPLCOM_DEBUG\n .equiv _KERNEL_OPT_UPLCOM_DEBUG,0x6e074def\n .endif");
#endif
/* option `UPL_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UPL_DEBUG
 .global _KERNEL_OPT_UPL_DEBUG
 .equiv _KERNEL_OPT_UPL_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UPL_DEBUG\n .global _KERNEL_OPT_UPL_DEBUG\n .equiv _KERNEL_OPT_UPL_DEBUG,0x6e074def\n .endif");
#endif
/* option `URAL_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_URAL_DEBUG
 .global _KERNEL_OPT_URAL_DEBUG
 .equiv _KERNEL_OPT_URAL_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_URAL_DEBUG\n .global _KERNEL_OPT_URAL_DEBUG\n .equiv _KERNEL_OPT_URAL_DEBUG,0x6e074def\n .endif");
#endif
/* option `URIO_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_URIO_DEBUG
 .global _KERNEL_OPT_URIO_DEBUG
 .equiv _KERNEL_OPT_URIO_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_URIO_DEBUG\n .global _KERNEL_OPT_URIO_DEBUG\n .equiv _KERNEL_OPT_URIO_DEBUG,0x6e074def\n .endif");
#endif
/* option `URL_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_URL_DEBUG
 .global _KERNEL_OPT_URL_DEBUG
 .equiv _KERNEL_OPT_URL_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_URL_DEBUG\n .global _KERNEL_OPT_URL_DEBUG\n .equiv _KERNEL_OPT_URL_DEBUG,0x6e074def\n .endif");
#endif
/* option `URNDIS_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_URNDIS_DEBUG
 .global _KERNEL_OPT_URNDIS_DEBUG
 .equiv _KERNEL_OPT_URNDIS_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_URNDIS_DEBUG\n .global _KERNEL_OPT_URNDIS_DEBUG\n .equiv _KERNEL_OPT_URNDIS_DEBUG,0x6e074def\n .endif");
#endif
/* option `URTWN_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_URTWN_DEBUG
 .global _KERNEL_OPT_URTWN_DEBUG
 .equiv _KERNEL_OPT_URTWN_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_URTWN_DEBUG\n .global _KERNEL_OPT_URTWN_DEBUG\n .equiv _KERNEL_OPT_URTWN_DEBUG,0x6e074def\n .endif");
#endif
/* option `URTW_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_URTW_DEBUG
 .global _KERNEL_OPT_URTW_DEBUG
 .equiv _KERNEL_OPT_URTW_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_URTW_DEBUG\n .global _KERNEL_OPT_URTW_DEBUG\n .equiv _KERNEL_OPT_URTW_DEBUG,0x6e074def\n .endif");
#endif
/* option `USCANNER_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_USCANNER_DEBUG
 .global _KERNEL_OPT_USCANNER_DEBUG
 .equiv _KERNEL_OPT_USCANNER_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_USCANNER_DEBUG\n .global _KERNEL_OPT_USCANNER_DEBUG\n .equiv _KERNEL_OPT_USCANNER_DEBUG,0x6e074def\n .endif");
#endif
/* option `USLSA_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_USLSA_DEBUG
 .global _KERNEL_OPT_USLSA_DEBUG
 .equiv _KERNEL_OPT_USLSA_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_USLSA_DEBUG\n .global _KERNEL_OPT_USLSA_DEBUG\n .equiv _KERNEL_OPT_USLSA_DEBUG,0x6e074def\n .endif");
#endif
/* option `USMSC_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_USMSC_DEBUG
 .global _KERNEL_OPT_USMSC_DEBUG
 .equiv _KERNEL_OPT_USMSC_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_USMSC_DEBUG\n .global _KERNEL_OPT_USMSC_DEBUG\n .equiv _KERNEL_OPT_USMSC_DEBUG,0x6e074def\n .endif");
#endif
/* option `USSCANNER_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_USSCANNER_DEBUG
 .global _KERNEL_OPT_USSCANNER_DEBUG
 .equiv _KERNEL_OPT_USSCANNER_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_USSCANNER_DEBUG\n .global _KERNEL_OPT_USSCANNER_DEBUG\n .equiv _KERNEL_OPT_USSCANNER_DEBUG,0x6e074def\n .endif");
#endif
/* option `USTIR_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_USTIR_DEBUG
 .global _KERNEL_OPT_USTIR_DEBUG
 .equiv _KERNEL_OPT_USTIR_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_USTIR_DEBUG\n .global _KERNEL_OPT_USTIR_DEBUG\n .equiv _KERNEL_OPT_USTIR_DEBUG,0x6e074def\n .endif");
#endif
/* option `UTHUM_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UTHUM_DEBUG
 .global _KERNEL_OPT_UTHUM_DEBUG
 .equiv _KERNEL_OPT_UTHUM_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UTHUM_DEBUG\n .global _KERNEL_OPT_UTHUM_DEBUG\n .equiv _KERNEL_OPT_UTHUM_DEBUG,0x6e074def\n .endif");
#endif
/* option `UTOPPY_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UTOPPY_DEBUG
 .global _KERNEL_OPT_UTOPPY_DEBUG
 .equiv _KERNEL_OPT_UTOPPY_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UTOPPY_DEBUG\n .global _KERNEL_OPT_UTOPPY_DEBUG\n .equiv _KERNEL_OPT_UTOPPY_DEBUG,0x6e074def\n .endif");
#endif
/* option `UTS_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UTS_DEBUG
 .global _KERNEL_OPT_UTS_DEBUG
 .equiv _KERNEL_OPT_UTS_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UTS_DEBUG\n .global _KERNEL_OPT_UTS_DEBUG\n .equiv _KERNEL_OPT_UTS_DEBUG,0x6e074def\n .endif");
#endif
/* option `UVIDEO_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UVIDEO_DEBUG
 .global _KERNEL_OPT_UVIDEO_DEBUG
 .equiv _KERNEL_OPT_UVIDEO_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UVIDEO_DEBUG\n .global _KERNEL_OPT_UVIDEO_DEBUG\n .equiv _KERNEL_OPT_UVIDEO_DEBUG,0x6e074def\n .endif");
#endif
/* option `UVISOR_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UVISOR_DEBUG
 .global _KERNEL_OPT_UVISOR_DEBUG
 .equiv _KERNEL_OPT_UVISOR_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UVISOR_DEBUG\n .global _KERNEL_OPT_UVISOR_DEBUG\n .equiv _KERNEL_OPT_UVISOR_DEBUG,0x6e074def\n .endif");
#endif
/* option `UVSCOM_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UVSCOM_DEBUG
 .global _KERNEL_OPT_UVSCOM_DEBUG
 .equiv _KERNEL_OPT_UVSCOM_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UVSCOM_DEBUG\n .global _KERNEL_OPT_UVSCOM_DEBUG\n .equiv _KERNEL_OPT_UVSCOM_DEBUG,0x6e074def\n .endif");
#endif
/* option `UYUREX_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_UYUREX_DEBUG
 .global _KERNEL_OPT_UYUREX_DEBUG
 .equiv _KERNEL_OPT_UYUREX_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_UYUREX_DEBUG\n .global _KERNEL_OPT_UYUREX_DEBUG\n .equiv _KERNEL_OPT_UYUREX_DEBUG,0x6e074def\n .endif");
#endif
/* option `ZYD_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_ZYD_DEBUG
 .global _KERNEL_OPT_ZYD_DEBUG
 .equiv _KERNEL_OPT_ZYD_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_ZYD_DEBUG\n .global _KERNEL_OPT_ZYD_DEBUG\n .equiv _KERNEL_OPT_ZYD_DEBUG,0x6e074def\n .endif");
#endif
