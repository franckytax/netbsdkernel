/* option `FONT_SPLEEN8x16' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_SPLEEN8x16
 .global _KERNEL_OPT_FONT_SPLEEN8x16
 .equiv _KERNEL_OPT_FONT_SPLEEN8x16,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_SPLEEN8x16\n .global _KERNEL_OPT_FONT_SPLEEN8x16\n .equiv _KERNEL_OPT_FONT_SPLEEN8x16,0x6e074def\n .endif");
#endif
/* option `FONT_SPLEEN5x8' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_SPLEEN5x8
 .global _KERNEL_OPT_FONT_SPLEEN5x8
 .equiv _KERNEL_OPT_FONT_SPLEEN5x8,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_SPLEEN5x8\n .global _KERNEL_OPT_FONT_SPLEEN5x8\n .equiv _KERNEL_OPT_FONT_SPLEEN5x8,0x6e074def\n .endif");
#endif
/* option `FONT_SPLEEN32x64' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_SPLEEN32x64
 .global _KERNEL_OPT_FONT_SPLEEN32x64
 .equiv _KERNEL_OPT_FONT_SPLEEN32x64,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_SPLEEN32x64\n .global _KERNEL_OPT_FONT_SPLEEN32x64\n .equiv _KERNEL_OPT_FONT_SPLEEN32x64,0x6e074def\n .endif");
#endif
/* option `FONT_SPLEEN16x32' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_SPLEEN16x32
 .global _KERNEL_OPT_FONT_SPLEEN16x32
 .equiv _KERNEL_OPT_FONT_SPLEEN16x32,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_SPLEEN16x32\n .global _KERNEL_OPT_FONT_SPLEEN16x32\n .equiv _KERNEL_OPT_FONT_SPLEEN16x32,0x6e074def\n .endif");
#endif
/* option `FONT_SPLEEN12x24' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_SPLEEN12x24
 .global _KERNEL_OPT_FONT_SPLEEN12x24
 .equiv _KERNEL_OPT_FONT_SPLEEN12x24,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_SPLEEN12x24\n .global _KERNEL_OPT_FONT_SPLEEN12x24\n .equiv _KERNEL_OPT_FONT_SPLEEN12x24,0x6e074def\n .endif");
#endif
/* option `FONT_GO_MONO12x23' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_GO_MONO12x23
 .global _KERNEL_OPT_FONT_GO_MONO12x23
 .equiv _KERNEL_OPT_FONT_GO_MONO12x23,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_GO_MONO12x23\n .global _KERNEL_OPT_FONT_GO_MONO12x23\n .equiv _KERNEL_OPT_FONT_GO_MONO12x23,0x6e074def\n .endif");
#endif
/* option `FONT_DROID_SANS_MONO19x36' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_DROID_SANS_MONO19x36
 .global _KERNEL_OPT_FONT_DROID_SANS_MONO19x36
 .equiv _KERNEL_OPT_FONT_DROID_SANS_MONO19x36,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_DROID_SANS_MONO19x36\n .global _KERNEL_OPT_FONT_DROID_SANS_MONO19x36\n .equiv _KERNEL_OPT_FONT_DROID_SANS_MONO19x36,0x6e074def\n .endif");
#endif
/* option `FONT_DROID_SANS_MONO9x18' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_DROID_SANS_MONO9x18
 .global _KERNEL_OPT_FONT_DROID_SANS_MONO9x18
 .equiv _KERNEL_OPT_FONT_DROID_SANS_MONO9x18,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_DROID_SANS_MONO9x18\n .global _KERNEL_OPT_FONT_DROID_SANS_MONO9x18\n .equiv _KERNEL_OPT_FONT_DROID_SANS_MONO9x18,0x6e074def\n .endif");
#endif
/* option `FONT_DROID_SANS_MONO12x22' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_DROID_SANS_MONO12x22
 .global _KERNEL_OPT_FONT_DROID_SANS_MONO12x22
 .equiv _KERNEL_OPT_FONT_DROID_SANS_MONO12x22,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_DROID_SANS_MONO12x22\n .global _KERNEL_OPT_FONT_DROID_SANS_MONO12x22\n .equiv _KERNEL_OPT_FONT_DROID_SANS_MONO12x22,0x6e074def\n .endif");
#endif
/* option `FONT_DEJAVU_SANS_MONO12x22' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_DEJAVU_SANS_MONO12x22
 .global _KERNEL_OPT_FONT_DEJAVU_SANS_MONO12x22
 .equiv _KERNEL_OPT_FONT_DEJAVU_SANS_MONO12x22,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_DEJAVU_SANS_MONO12x22\n .global _KERNEL_OPT_FONT_DEJAVU_SANS_MONO12x22\n .equiv _KERNEL_OPT_FONT_DEJAVU_SANS_MONO12x22,0x6e074def\n .endif");
#endif
/* option `FONT_OMRON12x20' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_OMRON12x20
 .global _KERNEL_OPT_FONT_OMRON12x20
 .equiv _KERNEL_OPT_FONT_OMRON12x20,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_OMRON12x20\n .global _KERNEL_OPT_FONT_OMRON12x20\n .equiv _KERNEL_OPT_FONT_OMRON12x20,0x6e074def\n .endif");
#endif
/* option `FONT_SONY12x24' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_SONY12x24
 .global _KERNEL_OPT_FONT_SONY12x24
 .equiv _KERNEL_OPT_FONT_SONY12x24,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_SONY12x24\n .global _KERNEL_OPT_FONT_SONY12x24\n .equiv _KERNEL_OPT_FONT_SONY12x24,0x6e074def\n .endif");
#endif
/* option `FONT_SONY8x16' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_SONY8x16
 .global _KERNEL_OPT_FONT_SONY8x16
 .equiv _KERNEL_OPT_FONT_SONY8x16,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_SONY8x16\n .global _KERNEL_OPT_FONT_SONY8x16\n .equiv _KERNEL_OPT_FONT_SONY8x16,0x6e074def\n .endif");
#endif
/* option `FONT_VT220KOI8x10_KOI8_U' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_VT220KOI8x10_KOI8_U
 .global _KERNEL_OPT_FONT_VT220KOI8x10_KOI8_U
 .equiv _KERNEL_OPT_FONT_VT220KOI8x10_KOI8_U,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_VT220KOI8x10_KOI8_U\n .global _KERNEL_OPT_FONT_VT220KOI8x10_KOI8_U\n .equiv _KERNEL_OPT_FONT_VT220KOI8x10_KOI8_U,0x6e074def\n .endif");
#endif
/* option `FONT_VT220KOI8x10_KOI8_R' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_VT220KOI8x10_KOI8_R
 .global _KERNEL_OPT_FONT_VT220KOI8x10_KOI8_R
 .equiv _KERNEL_OPT_FONT_VT220KOI8x10_KOI8_R,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_VT220KOI8x10_KOI8_R\n .global _KERNEL_OPT_FONT_VT220KOI8x10_KOI8_R\n .equiv _KERNEL_OPT_FONT_VT220KOI8x10_KOI8_R,0x6e074def\n .endif");
#endif
/* option `FONT_VT220ISO8x16' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_VT220ISO8x16
 .global _KERNEL_OPT_FONT_VT220ISO8x16
 .equiv _KERNEL_OPT_FONT_VT220ISO8x16,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_VT220ISO8x16\n .global _KERNEL_OPT_FONT_VT220ISO8x16\n .equiv _KERNEL_OPT_FONT_VT220ISO8x16,0x6e074def\n .endif");
#endif
/* option `FONT_VT220ISO8x8' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_VT220ISO8x8
 .global _KERNEL_OPT_FONT_VT220ISO8x8
 .equiv _KERNEL_OPT_FONT_VT220ISO8x8,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_VT220ISO8x8\n .global _KERNEL_OPT_FONT_VT220ISO8x8\n .equiv _KERNEL_OPT_FONT_VT220ISO8x8,0x6e074def\n .endif");
#endif
/* option `FONT_VT220L8x16' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_VT220L8x16
 .global _KERNEL_OPT_FONT_VT220L8x16
 .equiv _KERNEL_OPT_FONT_VT220L8x16,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_VT220L8x16\n .global _KERNEL_OPT_FONT_VT220L8x16\n .equiv _KERNEL_OPT_FONT_VT220L8x16,0x6e074def\n .endif");
#endif
/* option `FONT_VT220L8x10' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_VT220L8x10
 .global _KERNEL_OPT_FONT_VT220L8x10
 .equiv _KERNEL_OPT_FONT_VT220L8x10,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_VT220L8x10\n .global _KERNEL_OPT_FONT_VT220L8x10\n .equiv _KERNEL_OPT_FONT_VT220L8x10,0x6e074def\n .endif");
#endif
/* option `FONT_VT220L8x8' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_VT220L8x8
 .global _KERNEL_OPT_FONT_VT220L8x8
 .equiv _KERNEL_OPT_FONT_VT220L8x8,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_VT220L8x8\n .global _KERNEL_OPT_FONT_VT220L8x8\n .equiv _KERNEL_OPT_FONT_VT220L8x8,0x6e074def\n .endif");
#endif
/* option `FONT_LUCIDA16x29' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_LUCIDA16x29
 .global _KERNEL_OPT_FONT_LUCIDA16x29
 .equiv _KERNEL_OPT_FONT_LUCIDA16x29,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_LUCIDA16x29\n .global _KERNEL_OPT_FONT_LUCIDA16x29\n .equiv _KERNEL_OPT_FONT_LUCIDA16x29,0x6e074def\n .endif");
#endif
/* option `FONT_QVSS8x15' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_QVSS8x15
 .global _KERNEL_OPT_FONT_QVSS8x15
 .equiv _KERNEL_OPT_FONT_QVSS8x15,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_QVSS8x15\n .global _KERNEL_OPT_FONT_QVSS8x15\n .equiv _KERNEL_OPT_FONT_QVSS8x15,0x6e074def\n .endif");
#endif
/* option `FONT_GALLANT12x22' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_GALLANT12x22
 .global _KERNEL_OPT_FONT_GALLANT12x22
 .equiv _KERNEL_OPT_FONT_GALLANT12x22,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_GALLANT12x22\n .global _KERNEL_OPT_FONT_GALLANT12x22\n .equiv _KERNEL_OPT_FONT_GALLANT12x22,0x6e074def\n .endif");
#endif
/* option `FONT_GLASS10x25' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_GLASS10x25
 .global _KERNEL_OPT_FONT_GLASS10x25
 .equiv _KERNEL_OPT_FONT_GLASS10x25,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_GLASS10x25\n .global _KERNEL_OPT_FONT_GLASS10x25\n .equiv _KERNEL_OPT_FONT_GLASS10x25,0x6e074def\n .endif");
#endif
/* option `FONT_GLASS10x19' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_GLASS10x19
 .global _KERNEL_OPT_FONT_GLASS10x19
 .equiv _KERNEL_OPT_FONT_GLASS10x19,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_GLASS10x19\n .global _KERNEL_OPT_FONT_GLASS10x19\n .equiv _KERNEL_OPT_FONT_GLASS10x19,0x6e074def\n .endif");
#endif
#define	FONT_BOLD16x32	1
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_BOLD16x32
 .global _KERNEL_OPT_FONT_BOLD16x32
 .equiv _KERNEL_OPT_FONT_BOLD16x32,0x1
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_BOLD16x32\n .global _KERNEL_OPT_FONT_BOLD16x32\n .equiv _KERNEL_OPT_FONT_BOLD16x32,0x1\n .endif");
#endif
#define	FONT_BOLD8x16	1
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_FONT_BOLD8x16
 .global _KERNEL_OPT_FONT_BOLD8x16
 .equiv _KERNEL_OPT_FONT_BOLD8x16,0x1
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_FONT_BOLD8x16\n .global _KERNEL_OPT_FONT_BOLD8x16\n .equiv _KERNEL_OPT_FONT_BOLD8x16,0x1\n .endif");
#endif
