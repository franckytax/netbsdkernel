/* option `BKTR_USE_PLL' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_BKTR_USE_PLL
 .global _KERNEL_OPT_BKTR_USE_PLL
 .equiv _KERNEL_OPT_BKTR_USE_PLL,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_BKTR_USE_PLL\n .global _KERNEL_OPT_BKTR_USE_PLL\n .equiv _KERNEL_OPT_BKTR_USE_PLL,0x6e074def\n .endif");
#endif
/* option `BKTR_SIS_VIA_MODE' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_BKTR_SIS_VIA_MODE
 .global _KERNEL_OPT_BKTR_SIS_VIA_MODE
 .equiv _KERNEL_OPT_BKTR_SIS_VIA_MODE,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_BKTR_SIS_VIA_MODE\n .global _KERNEL_OPT_BKTR_SIS_VIA_MODE\n .equiv _KERNEL_OPT_BKTR_SIS_VIA_MODE,0x6e074def\n .endif");
#endif
/* option `BKTR_REVERSE_MUTE' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_BKTR_REVERSE_MUTE
 .global _KERNEL_OPT_BKTR_REVERSE_MUTE
 .equiv _KERNEL_OPT_BKTR_REVERSE_MUTE,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_BKTR_REVERSE_MUTE\n .global _KERNEL_OPT_BKTR_REVERSE_MUTE\n .equiv _KERNEL_OPT_BKTR_REVERSE_MUTE,0x6e074def\n .endif");
#endif
/* option `BKTR_NO_MSP_RESET' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_BKTR_NO_MSP_RESET
 .global _KERNEL_OPT_BKTR_NO_MSP_RESET
 .equiv _KERNEL_OPT_BKTR_NO_MSP_RESET,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_BKTR_NO_MSP_RESET\n .global _KERNEL_OPT_BKTR_NO_MSP_RESET\n .equiv _KERNEL_OPT_BKTR_NO_MSP_RESET,0x6e074def\n .endif");
#endif
/* option `BKTR_GPIO_ACCESS' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_BKTR_GPIO_ACCESS
 .global _KERNEL_OPT_BKTR_GPIO_ACCESS
 .equiv _KERNEL_OPT_BKTR_GPIO_ACCESS,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_BKTR_GPIO_ACCESS\n .global _KERNEL_OPT_BKTR_GPIO_ACCESS\n .equiv _KERNEL_OPT_BKTR_GPIO_ACCESS,0x6e074def\n .endif");
#endif
/* option `BKTR_430_FX_MODE' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_BKTR_430_FX_MODE
 .global _KERNEL_OPT_BKTR_430_FX_MODE
 .equiv _KERNEL_OPT_BKTR_430_FX_MODE,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_BKTR_430_FX_MODE\n .global _KERNEL_OPT_BKTR_430_FX_MODE\n .equiv _KERNEL_OPT_BKTR_430_FX_MODE,0x6e074def\n .endif");
#endif
/* option `BKTR_SYSTEM_DEFAULT' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_BKTR_SYSTEM_DEFAULT
 .global _KERNEL_OPT_BKTR_SYSTEM_DEFAULT
 .equiv _KERNEL_OPT_BKTR_SYSTEM_DEFAULT,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_BKTR_SYSTEM_DEFAULT\n .global _KERNEL_OPT_BKTR_SYSTEM_DEFAULT\n .equiv _KERNEL_OPT_BKTR_SYSTEM_DEFAULT,0x6e074def\n .endif");
#endif
/* option `BKTR_OVERRIDE_MSP' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_BKTR_OVERRIDE_MSP
 .global _KERNEL_OPT_BKTR_OVERRIDE_MSP
 .equiv _KERNEL_OPT_BKTR_OVERRIDE_MSP,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_BKTR_OVERRIDE_MSP\n .global _KERNEL_OPT_BKTR_OVERRIDE_MSP\n .equiv _KERNEL_OPT_BKTR_OVERRIDE_MSP,0x6e074def\n .endif");
#endif
/* option `BKTR_OVERRIDE_DBX' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_BKTR_OVERRIDE_DBX
 .global _KERNEL_OPT_BKTR_OVERRIDE_DBX
 .equiv _KERNEL_OPT_BKTR_OVERRIDE_DBX,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_BKTR_OVERRIDE_DBX\n .global _KERNEL_OPT_BKTR_OVERRIDE_DBX\n .equiv _KERNEL_OPT_BKTR_OVERRIDE_DBX,0x6e074def\n .endif");
#endif
/* option `BKTR_OVERRIDE_TUNER' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_BKTR_OVERRIDE_TUNER
 .global _KERNEL_OPT_BKTR_OVERRIDE_TUNER
 .equiv _KERNEL_OPT_BKTR_OVERRIDE_TUNER,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_BKTR_OVERRIDE_TUNER\n .global _KERNEL_OPT_BKTR_OVERRIDE_TUNER\n .equiv _KERNEL_OPT_BKTR_OVERRIDE_TUNER,0x6e074def\n .endif");
#endif
/* option `BKTR_OVERRIDE_CARD' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_BKTR_OVERRIDE_CARD
 .global _KERNEL_OPT_BKTR_OVERRIDE_CARD
 .equiv _KERNEL_OPT_BKTR_OVERRIDE_CARD,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_BKTR_OVERRIDE_CARD\n .global _KERNEL_OPT_BKTR_OVERRIDE_CARD\n .equiv _KERNEL_OPT_BKTR_OVERRIDE_CARD,0x6e074def\n .endif");
#endif
