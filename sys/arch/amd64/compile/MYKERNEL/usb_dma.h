#define	NUSB_DMA	1
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_NUSB_DMA
 .global _KERNEL_OPT_NUSB_DMA
 .equiv _KERNEL_OPT_NUSB_DMA,0x1
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_NUSB_DMA\n .global _KERNEL_OPT_NUSB_DMA\n .equiv _KERNEL_OPT_NUSB_DMA,0x1\n .endif");
#endif
