#define	MAXUSERS	64
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_MAXUSERS
 .global _KERNEL_OPT_MAXUSERS
 .equiv _KERNEL_OPT_MAXUSERS,0x40
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_MAXUSERS\n .global _KERNEL_OPT_MAXUSERS\n .equiv _KERNEL_OPT_MAXUSERS,0x40\n .endif");
#endif
