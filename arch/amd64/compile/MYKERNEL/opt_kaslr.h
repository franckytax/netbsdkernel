/* option `KASLR' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_KASLR
 .global _KERNEL_OPT_KASLR
 .equiv _KERNEL_OPT_KASLR,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_KASLR\n .global _KERNEL_OPT_KASLR\n .equiv _KERNEL_OPT_KASLR,0x6e074def\n .endif");
#endif
/* option `NO_X86_ASLR' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_NO_X86_ASLR
 .global _KERNEL_OPT_NO_X86_ASLR
 .equiv _KERNEL_OPT_NO_X86_ASLR,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_NO_X86_ASLR\n .global _KERNEL_OPT_NO_X86_ASLR\n .equiv _KERNEL_OPT_NO_X86_ASLR,0x6e074def\n .endif");
#endif
