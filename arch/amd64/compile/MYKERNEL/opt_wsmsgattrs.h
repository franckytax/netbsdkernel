/* option `WSDISPLAY_CUSTOM_OUTPUT' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_WSDISPLAY_CUSTOM_OUTPUT
 .global _KERNEL_OPT_WSDISPLAY_CUSTOM_OUTPUT
 .equiv _KERNEL_OPT_WSDISPLAY_CUSTOM_OUTPUT,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_WSDISPLAY_CUSTOM_OUTPUT\n .global _KERNEL_OPT_WSDISPLAY_CUSTOM_OUTPUT\n .equiv _KERNEL_OPT_WSDISPLAY_CUSTOM_OUTPUT,0x6e074def\n .endif");
#endif
/* option `WSDISPLAY_SCROLLCOMBO' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_WSDISPLAY_SCROLLCOMBO
 .global _KERNEL_OPT_WSDISPLAY_SCROLLCOMBO
 .equiv _KERNEL_OPT_WSDISPLAY_SCROLLCOMBO,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_WSDISPLAY_SCROLLCOMBO\n .global _KERNEL_OPT_WSDISPLAY_SCROLLCOMBO\n .equiv _KERNEL_OPT_WSDISPLAY_SCROLLCOMBO,0x6e074def\n .endif");
#endif
#define	WS_KERNEL_FG	WSCOL_GREEN
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_WS_KERNEL_FG
 .global _KERNEL_OPT_WS_KERNEL_FG
 .equiv _KERNEL_OPT_WS_KERNEL_FG,0xa41d6f2a
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_WS_KERNEL_FG\n .global _KERNEL_OPT_WS_KERNEL_FG\n .equiv _KERNEL_OPT_WS_KERNEL_FG,0xa41d6f2a\n .endif");
#endif
/* option `WS_KERNEL_BG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_WS_KERNEL_BG
 .global _KERNEL_OPT_WS_KERNEL_BG
 .equiv _KERNEL_OPT_WS_KERNEL_BG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_WS_KERNEL_BG\n .global _KERNEL_OPT_WS_KERNEL_BG\n .equiv _KERNEL_OPT_WS_KERNEL_BG,0x6e074def\n .endif");
#endif
/* option `WS_KERNEL_MONOATTR' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_WS_KERNEL_MONOATTR
 .global _KERNEL_OPT_WS_KERNEL_MONOATTR
 .equiv _KERNEL_OPT_WS_KERNEL_MONOATTR,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_WS_KERNEL_MONOATTR\n .global _KERNEL_OPT_WS_KERNEL_MONOATTR\n .equiv _KERNEL_OPT_WS_KERNEL_MONOATTR,0x6e074def\n .endif");
#endif
/* option `WS_KERNEL_COLATTR' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_WS_KERNEL_COLATTR
 .global _KERNEL_OPT_WS_KERNEL_COLATTR
 .equiv _KERNEL_OPT_WS_KERNEL_COLATTR,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_WS_KERNEL_COLATTR\n .global _KERNEL_OPT_WS_KERNEL_COLATTR\n .equiv _KERNEL_OPT_WS_KERNEL_COLATTR,0x6e074def\n .endif");
#endif
/* option `WS_DEFAULT_FG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_WS_DEFAULT_FG
 .global _KERNEL_OPT_WS_DEFAULT_FG
 .equiv _KERNEL_OPT_WS_DEFAULT_FG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_WS_DEFAULT_FG\n .global _KERNEL_OPT_WS_DEFAULT_FG\n .equiv _KERNEL_OPT_WS_DEFAULT_FG,0x6e074def\n .endif");
#endif
/* option `WS_DEFAULT_BG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_WS_DEFAULT_BG
 .global _KERNEL_OPT_WS_DEFAULT_BG
 .equiv _KERNEL_OPT_WS_DEFAULT_BG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_WS_DEFAULT_BG\n .global _KERNEL_OPT_WS_DEFAULT_BG\n .equiv _KERNEL_OPT_WS_DEFAULT_BG,0x6e074def\n .endif");
#endif
/* option `WS_DEFAULT_MONOATTR' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_WS_DEFAULT_MONOATTR
 .global _KERNEL_OPT_WS_DEFAULT_MONOATTR
 .equiv _KERNEL_OPT_WS_DEFAULT_MONOATTR,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_WS_DEFAULT_MONOATTR\n .global _KERNEL_OPT_WS_DEFAULT_MONOATTR\n .equiv _KERNEL_OPT_WS_DEFAULT_MONOATTR,0x6e074def\n .endif");
#endif
/* option `WS_DEFAULT_COLATTR' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_WS_DEFAULT_COLATTR
 .global _KERNEL_OPT_WS_DEFAULT_COLATTR
 .equiv _KERNEL_OPT_WS_DEFAULT_COLATTR,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_WS_DEFAULT_COLATTR\n .global _KERNEL_OPT_WS_DEFAULT_COLATTR\n .equiv _KERNEL_OPT_WS_DEFAULT_COLATTR,0x6e074def\n .endif");
#endif
