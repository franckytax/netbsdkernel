/* option `SCTP_MCORE_INPUT' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_SCTP_MCORE_INPUT
 .global _KERNEL_OPT_SCTP_MCORE_INPUT
 .equiv _KERNEL_OPT_SCTP_MCORE_INPUT,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_SCTP_MCORE_INPUT\n .global _KERNEL_OPT_SCTP_MCORE_INPUT\n .equiv _KERNEL_OPT_SCTP_MCORE_INPUT,0x6e074def\n .endif");
#endif
/* option `SCTP_USE_PERCPU_STAT' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_SCTP_USE_PERCPU_STAT
 .global _KERNEL_OPT_SCTP_USE_PERCPU_STAT
 .equiv _KERNEL_OPT_SCTP_USE_PERCPU_STAT,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_SCTP_USE_PERCPU_STAT\n .global _KERNEL_OPT_SCTP_USE_PERCPU_STAT\n .equiv _KERNEL_OPT_SCTP_USE_PERCPU_STAT,0x6e074def\n .endif");
#endif
/* option `SCTP_LTRACE_ERRORS' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_SCTP_LTRACE_ERRORS
 .global _KERNEL_OPT_SCTP_LTRACE_ERRORS
 .equiv _KERNEL_OPT_SCTP_LTRACE_ERRORS,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_SCTP_LTRACE_ERRORS\n .global _KERNEL_OPT_SCTP_LTRACE_ERRORS\n .equiv _KERNEL_OPT_SCTP_LTRACE_ERRORS,0x6e074def\n .endif");
#endif
/* option `SCTP_LTRACE_CHUNKS' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_SCTP_LTRACE_CHUNKS
 .global _KERNEL_OPT_SCTP_LTRACE_CHUNKS
 .equiv _KERNEL_OPT_SCTP_LTRACE_CHUNKS,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_SCTP_LTRACE_CHUNKS\n .global _KERNEL_OPT_SCTP_LTRACE_CHUNKS\n .equiv _KERNEL_OPT_SCTP_LTRACE_CHUNKS,0x6e074def\n .endif");
#endif
/* option `SCTP_PACKET_LOGGING' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_SCTP_PACKET_LOGGING
 .global _KERNEL_OPT_SCTP_PACKET_LOGGING
 .equiv _KERNEL_OPT_SCTP_PACKET_LOGGING,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_SCTP_PACKET_LOGGING\n .global _KERNEL_OPT_SCTP_PACKET_LOGGING\n .equiv _KERNEL_OPT_SCTP_PACKET_LOGGING,0x6e074def\n .endif");
#endif
/* option `SCTP_MBCNT_LOGGING' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_SCTP_MBCNT_LOGGING
 .global _KERNEL_OPT_SCTP_MBCNT_LOGGING
 .equiv _KERNEL_OPT_SCTP_MBCNT_LOGGING,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_SCTP_MBCNT_LOGGING\n .global _KERNEL_OPT_SCTP_MBCNT_LOGGING\n .equiv _KERNEL_OPT_SCTP_MBCNT_LOGGING,0x6e074def\n .endif");
#endif
/* option `SCTP_MBUF_LOGGING' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_SCTP_MBUF_LOGGING
 .global _KERNEL_OPT_SCTP_MBUF_LOGGING
 .equiv _KERNEL_OPT_SCTP_MBUF_LOGGING,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_SCTP_MBUF_LOGGING\n .global _KERNEL_OPT_SCTP_MBUF_LOGGING\n .equiv _KERNEL_OPT_SCTP_MBUF_LOGGING,0x6e074def\n .endif");
#endif
/* option `SCTP_LOCK_LOGGING' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_SCTP_LOCK_LOGGING
 .global _KERNEL_OPT_SCTP_LOCK_LOGGING
 .equiv _KERNEL_OPT_SCTP_LOCK_LOGGING,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_SCTP_LOCK_LOGGING\n .global _KERNEL_OPT_SCTP_LOCK_LOGGING\n .equiv _KERNEL_OPT_SCTP_LOCK_LOGGING,0x6e074def\n .endif");
#endif
/* option `SCTP_WITH_NO_CSUM' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_SCTP_WITH_NO_CSUM
 .global _KERNEL_OPT_SCTP_WITH_NO_CSUM
 .equiv _KERNEL_OPT_SCTP_WITH_NO_CSUM,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_SCTP_WITH_NO_CSUM\n .global _KERNEL_OPT_SCTP_WITH_NO_CSUM\n .equiv _KERNEL_OPT_SCTP_WITH_NO_CSUM,0x6e074def\n .endif");
#endif
/* option `SCTP_DEBUG' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_SCTP_DEBUG
 .global _KERNEL_OPT_SCTP_DEBUG
 .equiv _KERNEL_OPT_SCTP_DEBUG,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_SCTP_DEBUG\n .global _KERNEL_OPT_SCTP_DEBUG\n .equiv _KERNEL_OPT_SCTP_DEBUG,0x6e074def\n .endif");
#endif
/* option `SCTP' not defined */
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_SCTP
 .global _KERNEL_OPT_SCTP
 .equiv _KERNEL_OPT_SCTP,0x6e074def
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_SCTP\n .global _KERNEL_OPT_SCTP\n .equiv _KERNEL_OPT_SCTP,0x6e074def\n .endif");
#endif
