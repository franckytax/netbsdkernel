#define	OPEN_MAX	1024
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_OPEN_MAX
 .global _KERNEL_OPT_OPEN_MAX
 .equiv _KERNEL_OPT_OPEN_MAX,0x400
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_OPEN_MAX\n .global _KERNEL_OPT_OPEN_MAX\n .equiv _KERNEL_OPT_OPEN_MAX,0x400\n .endif");
#endif
#define	CHILD_MAX	1024
#ifdef _LOCORE
 .ifndef _KERNEL_OPT_CHILD_MAX
 .global _KERNEL_OPT_CHILD_MAX
 .equiv _KERNEL_OPT_CHILD_MAX,0x400
 .endif
#else
__asm(" .ifndef _KERNEL_OPT_CHILD_MAX\n .global _KERNEL_OPT_CHILD_MAX\n .equiv _KERNEL_OPT_CHILD_MAX,0x400\n .endif");
#endif
