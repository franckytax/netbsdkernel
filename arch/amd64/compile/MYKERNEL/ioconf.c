#include "ioconf.h"
/*
 * MACHINE GENERATED: DO NOT EDIT
 *
 * ioconf.c, from "MYKERNEL"
 */

#include <sys/param.h>
#include <sys/conf.h>
#include <sys/device.h>
#include <sys/mount.h>

static const struct cfiattrdata cardbuscf_iattrdata = {
	"cardbus", 1, {
		{ "function", "-1", -1 },
	}
};
static const struct cfiattrdata nfsmbccf_iattrdata = {
	"nfsmbc", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata wskbddevcf_iattrdata = {
	"wskbddev", 2, {
		{ "console", "-1", -1 },
		{ "mux", "1", 1 },
	}
};
static const struct cfiattrdata agpbuscf_iattrdata = {
	"agpbus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata pckbportcf_iattrdata = {
	"pckbport", 1, {
		{ "slot", "-1", -1 },
	}
};
static const struct cfiattrdata acpiecdtbuscf_iattrdata = {
	"acpiecdtbus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata wsmousedevcf_iattrdata = {
	"wsmousedev", 1, {
		{ "mux", "0", 0 },
	}
};
static const struct cfiattrdata svcf_iattrdata = {
	"sv", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata midibuscf_iattrdata = {
	"midibus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata sdmmccf_iattrdata = {
	"sdmmc", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata nouveaufbbuscf_iattrdata = {
	"nouveaufbbus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata pcppicf_iattrdata = {
	"pcppi", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata cpufeaturebuscf_iattrdata = {
	"cpufeaturebus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata onewirebuscf_iattrdata = {
	"onewirebus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata ihidbuscf_iattrdata = {
	"ihidbus", 1, {
		{ "reportid", "-1", -1 },
	}
};
static const struct cfiattrdata radeonfbbuscf_iattrdata = {
	"radeonfbbus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata fdccf_iattrdata = {
	"fdc", 1, {
		{ "drive", "-1", -1 },
	}
};
static const struct cfiattrdata bthidbuscf_iattrdata = {
	"bthidbus", 1, {
		{ "reportid", "-1", -1 },
	}
};
static const struct cfiattrdata nvmecf_iattrdata = {
	"nvme", 1, {
		{ "nsid", "-1", -1 },
	}
};
static const struct cfiattrdata pcmciabuscf_iattrdata = {
	"pcmciabus", 2, {
		{ "controller", "-1", -1 },
		{ "socket", "-1", -1 },
	}
};
static const struct cfiattrdata drmcf_iattrdata = {
	"drm", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata acpibuscf_iattrdata = {
	"acpibus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata puccf_iattrdata = {
	"puc", 1, {
		{ "port", "-1", -1 },
	}
};
static const struct cfiattrdata ieee1394ifcf_iattrdata = {
	"ieee1394if", 2, {
		{ "euihi", "-1", -1 },
		{ "euilo", "-1", -1 },
	}
};
static const struct cfiattrdata pcicf_iattrdata = {
	"pci", 2, {
		{ "dev", "-1", -1 },
		{ "function", "-1", -1 },
	}
};
static const struct cfiattrdata irbuscf_iattrdata = {
	"irbus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata fmscf_iattrdata = {
	"fms", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata wsbelldevcf_iattrdata = {
	"wsbelldev", 2, {
		{ "console", "-1", -1 },
		{ "mux", "1", 1 },
	}
};
static const struct cfiattrdata usbuscf_iattrdata = {
	"usbus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata acpihpetbuscf_iattrdata = {
	"acpihpetbus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata ioapicbuscf_iattrdata = {
	"ioapicbus", 1, {
		{ "apid", "-1", -1 },
	}
};
static const struct cfiattrdata pcibuscf_iattrdata = {
	"pcibus", 1, {
		{ "bus", "-1", -1 },
	}
};
static const struct cfiattrdata cbbuscf_iattrdata = {
	"cbbus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata eapcf_iattrdata = {
	"eap", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata virtiocf_iattrdata = {
	"virtio", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata pcmcomcf_iattrdata = {
	"pcmcom", 1, {
		{ "slave", "-1", -1 },
	}
};
static const struct cfiattrdata dtvbuscf_iattrdata = {
	"dtvbus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata atapibuscf_iattrdata = {
	"atapibus", 1, {
		{ "drive", "-1", -1 },
	}
};
static const struct cfiattrdata isacf_iattrdata = {
	"isa", 7, {
		{ "port", "-1", -1 },
		{ "size", "0", 0 },
		{ "iomem", "-1", -1 },
		{ "iosiz", "0", 0 },
		{ "irq", "-1", -1 },
		{ "drq", "-1", -1 },
		{ "drq2", "-1", -1 },
	}
};
static const struct cfiattrdata miicf_iattrdata = {
	"mii", 1, {
		{ "phy", "-1", -1 },
	}
};
static const struct cfiattrdata fwhichbuscf_iattrdata = {
	"fwhichbus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata acpiwdrtbuscf_iattrdata = {
	"acpiwdrtbus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata acpinodebuscf_iattrdata = {
	"acpinodebus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata sdmmcbuscf_iattrdata = {
	"sdmmcbus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata acpimadtbuscf_iattrdata = {
	"acpimadtbus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata intelfbbuscf_iattrdata = {
	"intelfbbus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata cpubuscf_iattrdata = {
	"cpubus", 1, {
		{ "apid", "-1", -1 },
	}
};
static const struct cfiattrdata onewirecf_iattrdata = {
	"onewire", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata btbuscf_iattrdata = {
	"btbus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata fwbuscf_iattrdata = {
	"fwbus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata esocf_iattrdata = {
	"eso", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata bthubcf_iattrdata = {
	"bthub", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata isabuscf_iattrdata = {
	"isabus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata ydscf_iattrdata = {
	"yds", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata wsemuldisplaydevcf_iattrdata = {
	"wsemuldisplaydev", 2, {
		{ "console", "-1", -1 },
		{ "kbdmux", "1", 1 },
	}
};
static const struct cfiattrdata mhzccf_iattrdata = {
	"mhzc", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata i2cbuscf_iattrdata = {
	"i2cbus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata atapicf_iattrdata = {
	"atapi", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata scsicf_iattrdata = {
	"scsi", 1, {
		{ "channel", "-1", -1 },
	}
};
static const struct cfiattrdata acpiapmbuscf_iattrdata = {
	"acpiapmbus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata cmpcicf_iattrdata = {
	"cmpci", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata pcmciacf_iattrdata = {
	"pcmcia", 1, {
		{ "function", "-1", -1 },
	}
};
static const struct cfiattrdata iopcf_iattrdata = {
	"iop", 1, {
		{ "tid", "-1", -1 },
	}
};
static const struct cfiattrdata hdaudiobuscf_iattrdata = {
	"hdaudiobus", 1, {
		{ "nid", "-1", -1 },
	}
};
static const struct cfiattrdata usbififcf_iattrdata = {
	"usbifif", 6, {
		{ "port", "-1", -1 },
		{ "configuration", "-1", -1 },
		{ "interface", "-1", -1 },
		{ "vendor", "-1", -1 },
		{ "product", "-1", -1 },
		{ "release", "-1", -1 },
	}
};
static const struct cfiattrdata amdnb_miscbuscf_iattrdata = {
	"amdnb_miscbus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata gpiobuscf_iattrdata = {
	"gpiobus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata acpivgacf_iattrdata = {
	"acpivga", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata scsibuscf_iattrdata = {
	"scsibus", 2, {
		{ "target", "-1", -1 },
		{ "lun", "-1", -1 },
	}
};
static const struct cfiattrdata atacf_iattrdata = {
	"ata", 1, {
		{ "channel", "-1", -1 },
	}
};
static const struct cfiattrdata usbdevifcf_iattrdata = {
	"usbdevif", 6, {
		{ "port", "-1", -1 },
		{ "configuration", "-1", -1 },
		{ "interface", "-1", -1 },
		{ "vendor", "-1", -1 },
		{ "product", "-1", -1 },
		{ "release", "-1", -1 },
	}
};
static const struct cfiattrdata acpigtdtbuscf_iattrdata = {
	"acpigtdtbus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata pcmciaslotcf_iattrdata = {
	"pcmciaslot", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata audiocf_iattrdata = {
	"audio", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata sbpcf_iattrdata = {
	"sbp", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata acpiwmibuscf_iattrdata = {
	"acpiwmibus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata amdsmnbuscf_iattrdata = {
	"amdsmnbus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata iiccf_iattrdata = {
	"iic", 1, {
		{ "addr", "-1", -1 },
	}
};
static const struct cfiattrdata audiobuscf_iattrdata = {
	"audiobus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata videobuscf_iattrdata = {
	"videobus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata ata_hlcf_iattrdata = {
	"ata_hl", 1, {
		{ "drive", "-1", -1 },
	}
};
static const struct cfiattrdata gpiocf_iattrdata = {
	"gpio", 3, {
		{ "offset", "-1", -1 },
		{ "mask", "0", 0 },
		{ "flag", "0", 0 },
	}
};
static const struct cfiattrdata uhidbuscf_iattrdata = {
	"uhidbus", 1, {
		{ "reportid", "-1", -1 },
	}
};
static const struct cfiattrdata tcoichbuscf_iattrdata = {
	"tcoichbus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata ataraidcf_iattrdata = {
	"ataraid", 2, {
		{ "vendtype", "-1", -1 },
		{ "unit", "-1", -1 },
	}
};
static const struct cfiattrdata jmide_hlcf_iattrdata = {
	"jmide_hl", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata amdpcibcf_iattrdata = {
	"amdpcib", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata hpetichbuscf_iattrdata = {
	"hpetichbus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata comcf_iattrdata = {
	"com", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata radiodevcf_iattrdata = {
	"radiodev", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata acpisdtbuscf_iattrdata = {
	"acpisdtbus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata ipmibuscf_iattrdata = {
	"ipmibus", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata usbroothubifcf_iattrdata = {
	"usbroothubif", 0, {
		{ NULL, NULL, 0 },
	}
};
static const struct cfiattrdata ucombuscf_iattrdata = {
	"ucombus", 1, {
		{ "portno", "-1", -1 },
	}
};

static const struct cfiattrdata * const spkr_attrs[] = { &wsbelldevcf_iattrdata, NULL };
CFDRIVER_DECL(spkr, DV_DULL, spkr_attrs);

static const struct cfiattrdata * const audio_attrs[] = { &audiocf_iattrdata, NULL };
CFDRIVER_DECL(audio, DV_AUDIODEV, audio_attrs);

static const struct cfiattrdata * const midi_attrs[] = { &audiocf_iattrdata, NULL };
CFDRIVER_DECL(midi, DV_DULL, midi_attrs);

static const struct cfiattrdata * const hdaudio_attrs[] = { &hdaudiobuscf_iattrdata, NULL };
CFDRIVER_DECL(hdaudio, DV_DULL, hdaudio_attrs);

static const struct cfiattrdata * const hdafg_attrs[] = { &audiobuscf_iattrdata, NULL };
CFDRIVER_DECL(hdafg, DV_DULL, hdafg_attrs);

CFDRIVER_DECL(video, DV_DULL, NULL);

CFDRIVER_DECL(dtv, DV_DULL, NULL);

static const struct cfiattrdata * const iic_attrs[] = { &iiccf_iattrdata, NULL };
CFDRIVER_DECL(iic, DV_DULL, iic_attrs);

static const struct cfiattrdata * const ihidev_attrs[] = { &ihidbuscf_iattrdata, NULL };
CFDRIVER_DECL(ihidev, DV_DULL, ihidev_attrs);

static const struct cfiattrdata * const ims_attrs[] = { &wsmousedevcf_iattrdata, NULL };
CFDRIVER_DECL(ims, DV_DULL, ims_attrs);

CFDRIVER_DECL(irframe, DV_DULL, NULL);

CFDRIVER_DECL(cir, DV_DULL, NULL);

CFDRIVER_DECL(lpt, DV_DULL, NULL);

CFDRIVER_DECL(tpm, DV_DULL, NULL);

CFDRIVER_DECL(ld, DV_DISK, NULL);

CFDRIVER_DECL(raid, DV_DISK, NULL);

CFDRIVER_DECL(cy, DV_TTY, NULL);

static const struct cfiattrdata * const sm_attrs[] = { &miicf_iattrdata, NULL };
CFDRIVER_DECL(sm, DV_IFNET, sm_attrs);

static const struct cfiattrdata * const com_attrs[] = { &comcf_iattrdata, NULL };
CFDRIVER_DECL(com, DV_TTY, com_attrs);

static const struct cfiattrdata * const pckbc_attrs[] = { &pckbportcf_iattrdata, NULL };
CFDRIVER_DECL(pckbc, DV_DULL, pckbc_attrs);

CFDRIVER_DECL(attimer, DV_DULL, NULL);

static const struct cfiattrdata * const opl_attrs[] = { &midibuscf_iattrdata, NULL };
CFDRIVER_DECL(opl, DV_DULL, opl_attrs);

static const struct cfiattrdata * const mpu_attrs[] = { &midibuscf_iattrdata, NULL };
CFDRIVER_DECL(mpu, DV_DULL, mpu_attrs);

static const struct cfiattrdata * const virtio_attrs[] = { &virtiocf_iattrdata, NULL };
CFDRIVER_DECL(virtio, DV_DULL, virtio_attrs);

CFDRIVER_DECL(viomb, DV_DULL, NULL);

CFDRIVER_DECL(vioif, DV_IFNET, NULL);

CFDRIVER_DECL(viornd, DV_DULL, NULL);

static const struct cfiattrdata * const vioscsi_attrs[] = { &scsicf_iattrdata, NULL };
CFDRIVER_DECL(vioscsi, DV_DULL, vioscsi_attrs);

static const struct cfiattrdata * const wdc_attrs[] = { &atacf_iattrdata, NULL };
CFDRIVER_DECL(wdc, DV_DULL, wdc_attrs);

static const struct cfiattrdata * const atabus_attrs[] = { &ata_hlcf_iattrdata, &atapicf_iattrdata, NULL };
CFDRIVER_DECL(atabus, DV_DULL, atabus_attrs);

static const struct cfiattrdata * const njata_attrs[] = { &atacf_iattrdata, NULL };
CFDRIVER_DECL(njata, DV_DULL, njata_attrs);

static const struct cfiattrdata * const ahcisata_attrs[] = { &atacf_iattrdata, NULL };
CFDRIVER_DECL(ahcisata, DV_DULL, ahcisata_attrs);

static const struct cfiattrdata * const siisata_attrs[] = { &atacf_iattrdata, NULL };
CFDRIVER_DECL(siisata, DV_DULL, siisata_attrs);

static const struct cfiattrdata * const mvsata_attrs[] = { &atacf_iattrdata, NULL };
CFDRIVER_DECL(mvsata, DV_DULL, mvsata_attrs);

static const struct cfiattrdata * const dwiic_attrs[] = { &i2cbuscf_iattrdata, NULL };
CFDRIVER_DECL(dwiic, DV_DULL, dwiic_attrs);

CFDRIVER_DECL(hpet, DV_DULL, NULL);

static const struct cfiattrdata * const vga_attrs[] = { &drmcf_iattrdata, &wsemuldisplaydevcf_iattrdata, NULL };
CFDRIVER_DECL(vga, DV_DISPLAYDEV, vga_attrs);

CFDRIVER_DECL(wsdisplay, DV_DULL, NULL);

CFDRIVER_DECL(wskbd, DV_DULL, NULL);

CFDRIVER_DECL(wsmouse, DV_DULL, NULL);

static const struct cfiattrdata * const genfb_attrs[] = { &drmcf_iattrdata, &wsemuldisplaydevcf_iattrdata, NULL };
CFDRIVER_DECL(genfb, DV_DULL, genfb_attrs);

static const struct cfiattrdata * const uhci_attrs[] = { &usbuscf_iattrdata, NULL };
CFDRIVER_DECL(uhci, DV_DULL, uhci_attrs);

static const struct cfiattrdata * const ohci_attrs[] = { &usbuscf_iattrdata, NULL };
CFDRIVER_DECL(ohci, DV_DULL, ohci_attrs);

static const struct cfiattrdata * const ehci_attrs[] = { &usbuscf_iattrdata, NULL };
CFDRIVER_DECL(ehci, DV_DULL, ehci_attrs);

static const struct cfiattrdata * const xhci_attrs[] = { &usbuscf_iattrdata, NULL };
CFDRIVER_DECL(xhci, DV_DULL, xhci_attrs);

static const struct cfiattrdata * const slhci_attrs[] = { &usbuscf_iattrdata, NULL };
CFDRIVER_DECL(slhci, DV_DULL, slhci_attrs);

static const struct cfiattrdata * const sdhc_attrs[] = { &sdmmcbuscf_iattrdata, NULL };
CFDRIVER_DECL(sdhc, DV_DULL, sdhc_attrs);

static const struct cfiattrdata * const rtsx_attrs[] = { &sdmmcbuscf_iattrdata, NULL };
CFDRIVER_DECL(rtsx, DV_DULL, rtsx_attrs);

CFDRIVER_DECL(radio, DV_DULL, NULL);

static const struct cfiattrdata * const fwohci_attrs[] = { &fwbuscf_iattrdata, NULL };
CFDRIVER_DECL(fwohci, DV_DULL, fwohci_attrs);

static const struct cfiattrdata * const nvme_attrs[] = { &nvmecf_iattrdata, NULL };
CFDRIVER_DECL(nvme, DV_DULL, nvme_attrs);

CFDRIVER_DECL(bwfm, DV_IFNET, NULL);

CFDRIVER_DECL(qemufwcfg, DV_DULL, NULL);

CFDRIVER_DECL(vnd, DV_DISK, NULL);

CFDRIVER_DECL(cgd, DV_DISK, NULL);

CFDRIVER_DECL(md, DV_DISK, NULL);

CFDRIVER_DECL(fss, DV_DISK, NULL);

CFDRIVER_DECL(tap, DV_IFNET, NULL);

CFDRIVER_DECL(l2tp, DV_IFNET, NULL);

CFDRIVER_DECL(ipmi, DV_DULL, NULL);

CFDRIVER_DECL(joy, DV_DULL, NULL);

static const struct cfiattrdata * const gpio_attrs[] = { &gpiocf_iattrdata, NULL };
CFDRIVER_DECL(gpio, DV_DULL, gpio_attrs);

static const struct cfiattrdata * const gpioow_attrs[] = { &onewirebuscf_iattrdata, NULL };
CFDRIVER_DECL(gpioow, DV_DULL, gpioow_attrs);

static const struct cfiattrdata * const onewire_attrs[] = { &onewirecf_iattrdata, NULL };
CFDRIVER_DECL(onewire, DV_DULL, onewire_attrs);

static const struct cfiattrdata * const pad_attrs[] = { &audiobuscf_iattrdata, NULL };
CFDRIVER_DECL(pad, DV_DULL, pad_attrs);

CFDRIVER_DECL(athn, DV_IFNET, NULL);

static const struct cfiattrdata * const cpu_attrs[] = { &cpufeaturebuscf_iattrdata, NULL };
CFDRIVER_DECL(cpu, DV_DULL, cpu_attrs);

CFDRIVER_DECL(acpicpu, DV_DULL, NULL);

CFDRIVER_DECL(coretemp, DV_DULL, NULL);

CFDRIVER_DECL(est, DV_DULL, NULL);

CFDRIVER_DECL(powernow, DV_DULL, NULL);

CFDRIVER_DECL(ioapic, DV_DULL, NULL);

static const struct cfiattrdata * const scsibus_attrs[] = { &scsibuscf_iattrdata, NULL };
CFDRIVER_DECL(scsibus, DV_DULL, scsibus_attrs);

static const struct cfiattrdata * const atapibus_attrs[] = { &atapibuscf_iattrdata, NULL };
CFDRIVER_DECL(atapibus, DV_DULL, atapibus_attrs);

CFDRIVER_DECL(cd, DV_DISK, NULL);

CFDRIVER_DECL(ch, DV_DULL, NULL);

CFDRIVER_DECL(sd, DV_DISK, NULL);

CFDRIVER_DECL(st, DV_TAPE, NULL);

CFDRIVER_DECL(ses, DV_DULL, NULL);

CFDRIVER_DECL(ss, DV_DULL, NULL);

CFDRIVER_DECL(uk, DV_DULL, NULL);

CFDRIVER_DECL(wd, DV_DISK, NULL);

static const struct cfiattrdata * const ataraid_attrs[] = { &ataraidcf_iattrdata, NULL };
CFDRIVER_DECL(ataraid, DV_DULL, ataraid_attrs);

static const struct cfiattrdata * const iop_attrs[] = { &iopcf_iattrdata, NULL };
CFDRIVER_DECL(iop, DV_DULL, iop_attrs);

static const struct cfiattrdata * const iopsp_attrs[] = { &scsicf_iattrdata, NULL };
CFDRIVER_DECL(iopsp, DV_DULL, iopsp_attrs);

static const struct cfiattrdata * const mainbus_attrs[] = { &ipmibuscf_iattrdata, &ioapicbuscf_iattrdata, &cpubuscf_iattrdata, &acpibuscf_iattrdata, &pcibuscf_iattrdata, &isabuscf_iattrdata, NULL };
CFDRIVER_DECL(mainbus, DV_DULL, mainbus_attrs);

static const struct cfiattrdata * const pci_attrs[] = { &pcicf_iattrdata, NULL };
CFDRIVER_DECL(pci, DV_DULL, pci_attrs);

static const struct cfiattrdata * const pciide_attrs[] = { &atacf_iattrdata, NULL };
CFDRIVER_DECL(pciide, DV_DULL, pciide_attrs);

static const struct cfiattrdata * const acardide_attrs[] = { &atacf_iattrdata, NULL };
CFDRIVER_DECL(acardide, DV_DULL, acardide_attrs);

static const struct cfiattrdata * const aceride_attrs[] = { &atacf_iattrdata, NULL };
CFDRIVER_DECL(aceride, DV_DULL, aceride_attrs);

static const struct cfiattrdata * const artsata_attrs[] = { &atacf_iattrdata, NULL };
CFDRIVER_DECL(artsata, DV_DULL, artsata_attrs);

static const struct cfiattrdata * const cmdide_attrs[] = { &atacf_iattrdata, NULL };
CFDRIVER_DECL(cmdide, DV_DULL, cmdide_attrs);

static const struct cfiattrdata * const cypide_attrs[] = { &atacf_iattrdata, NULL };
CFDRIVER_DECL(cypide, DV_DULL, cypide_attrs);

static const struct cfiattrdata * const hptide_attrs[] = { &atacf_iattrdata, NULL };
CFDRIVER_DECL(hptide, DV_DULL, hptide_attrs);

static const struct cfiattrdata * const iteide_attrs[] = { &atacf_iattrdata, NULL };
CFDRIVER_DECL(iteide, DV_DULL, iteide_attrs);

static const struct cfiattrdata * const jmide_attrs[] = { &jmide_hlcf_iattrdata, &atacf_iattrdata, NULL };
CFDRIVER_DECL(jmide, DV_DULL, jmide_attrs);

static const struct cfiattrdata * const optiide_attrs[] = { &atacf_iattrdata, NULL };
CFDRIVER_DECL(optiide, DV_DULL, optiide_attrs);

static const struct cfiattrdata * const piixide_attrs[] = { &atacf_iattrdata, NULL };
CFDRIVER_DECL(piixide, DV_DULL, piixide_attrs);

static const struct cfiattrdata * const pdcsata_attrs[] = { &atacf_iattrdata, NULL };
CFDRIVER_DECL(pdcsata, DV_DULL, pdcsata_attrs);

static const struct cfiattrdata * const pdcide_attrs[] = { &atacf_iattrdata, NULL };
CFDRIVER_DECL(pdcide, DV_DULL, pdcide_attrs);

static const struct cfiattrdata * const svwsata_attrs[] = { &atacf_iattrdata, NULL };
CFDRIVER_DECL(svwsata, DV_DULL, svwsata_attrs);

static const struct cfiattrdata * const satalink_attrs[] = { &atacf_iattrdata, NULL };
CFDRIVER_DECL(satalink, DV_DULL, satalink_attrs);

static const struct cfiattrdata * const siside_attrs[] = { &atacf_iattrdata, NULL };
CFDRIVER_DECL(siside, DV_DULL, siside_attrs);

static const struct cfiattrdata * const slide_attrs[] = { &atacf_iattrdata, NULL };
CFDRIVER_DECL(slide, DV_DULL, slide_attrs);

static const struct cfiattrdata * const viaide_attrs[] = { &atacf_iattrdata, NULL };
CFDRIVER_DECL(viaide, DV_DULL, viaide_attrs);

static const struct cfiattrdata * const ixpide_attrs[] = { &atacf_iattrdata, NULL };
CFDRIVER_DECL(ixpide, DV_DULL, ixpide_attrs);

static const struct cfiattrdata * const toshide_attrs[] = { &atacf_iattrdata, NULL };
CFDRIVER_DECL(toshide, DV_DULL, toshide_attrs);

static const struct cfiattrdata * const ppb_attrs[] = { &pcibuscf_iattrdata, NULL };
CFDRIVER_DECL(ppb, DV_DULL, ppb_attrs);

CFDRIVER_DECL(cz, DV_DULL, NULL);

static const struct cfiattrdata * const bktr_attrs[] = { &radiodevcf_iattrdata, NULL };
CFDRIVER_DECL(bktr, DV_DULL, bktr_attrs);

static const struct cfiattrdata * const clcs_attrs[] = { &midibuscf_iattrdata, &audiobuscf_iattrdata, NULL };
CFDRIVER_DECL(clcs, DV_DULL, clcs_attrs);

static const struct cfiattrdata * const clct_attrs[] = { &audiobuscf_iattrdata, NULL };
CFDRIVER_DECL(clct, DV_DULL, clct_attrs);

static const struct cfiattrdata * const fms_attrs[] = { &fmscf_iattrdata, &midibuscf_iattrdata, &audiobuscf_iattrdata, NULL };
CFDRIVER_DECL(fms, DV_DULL, fms_attrs);

static const struct cfiattrdata * const eap_attrs[] = { &eapcf_iattrdata, &midibuscf_iattrdata, &audiobuscf_iattrdata, NULL };
CFDRIVER_DECL(eap, DV_DULL, eap_attrs);

static const struct cfiattrdata * const auacer_attrs[] = { &audiobuscf_iattrdata, NULL };
CFDRIVER_DECL(auacer, DV_DULL, auacer_attrs);

static const struct cfiattrdata * const auich_attrs[] = { &audiobuscf_iattrdata, NULL };
CFDRIVER_DECL(auich, DV_DULL, auich_attrs);

static const struct cfiattrdata * const auvia_attrs[] = { &audiobuscf_iattrdata, NULL };
CFDRIVER_DECL(auvia, DV_DULL, auvia_attrs);

static const struct cfiattrdata * const auixp_attrs[] = { &audiobuscf_iattrdata, NULL };
CFDRIVER_DECL(auixp, DV_DULL, auixp_attrs);

static const struct cfiattrdata * const neo_attrs[] = { &audiobuscf_iattrdata, NULL };
CFDRIVER_DECL(neo, DV_DULL, neo_attrs);

static const struct cfiattrdata * const esa_attrs[] = { &audiobuscf_iattrdata, NULL };
CFDRIVER_DECL(esa, DV_DULL, esa_attrs);

static const struct cfiattrdata * const eso_attrs[] = { &esocf_iattrdata, &midibuscf_iattrdata, &audiobuscf_iattrdata, NULL };
CFDRIVER_DECL(eso, DV_DULL, eso_attrs);

static const struct cfiattrdata * const esm_attrs[] = { &audiobuscf_iattrdata, NULL };
CFDRIVER_DECL(esm, DV_DULL, esm_attrs);

static const struct cfiattrdata * const sv_attrs[] = { &svcf_iattrdata, &midibuscf_iattrdata, &audiobuscf_iattrdata, NULL };
CFDRIVER_DECL(sv, DV_DULL, sv_attrs);

static const struct cfiattrdata * const cmpci_attrs[] = { &cmpcicf_iattrdata, &audiobuscf_iattrdata, NULL };
CFDRIVER_DECL(cmpci, DV_DULL, cmpci_attrs);

static const struct cfiattrdata * const yds_attrs[] = { &ydscf_iattrdata, &audiobuscf_iattrdata, NULL };
CFDRIVER_DECL(yds, DV_DULL, yds_attrs);

static const struct cfiattrdata * const emuxki_attrs[] = { &audiobuscf_iattrdata, NULL };
CFDRIVER_DECL(emuxki, DV_DULL, emuxki_attrs);

static const struct cfiattrdata * const autri_attrs[] = { &midibuscf_iattrdata, &audiobuscf_iattrdata, NULL };
CFDRIVER_DECL(autri, DV_DULL, autri_attrs);

static const struct cfiattrdata * const puc_attrs[] = { &puccf_iattrdata, NULL };
CFDRIVER_DECL(puc, DV_DULL, puc_attrs);

static const struct cfiattrdata * const cbb_attrs[] = { &pcmciaslotcf_iattrdata, NULL };
CFDRIVER_DECL(cbb, DV_DULL, cbb_attrs);

static const struct cfiattrdata * const piixpm_attrs[] = { &i2cbuscf_iattrdata, NULL };
CFDRIVER_DECL(piixpm, DV_DULL, piixpm_attrs);

static const struct cfiattrdata * const amdpm_attrs[] = { &i2cbuscf_iattrdata, NULL };
CFDRIVER_DECL(amdpm, DV_DULL, amdpm_attrs);

CFDRIVER_DECL(hifn, DV_DULL, NULL);

CFDRIVER_DECL(ubsec, DV_DULL, NULL);

CFDRIVER_DECL(weasel, DV_DULL, NULL);

static const struct cfiattrdata * const nfsmbc_attrs[] = { &nfsmbccf_iattrdata, NULL };
CFDRIVER_DECL(nfsmbc, DV_DULL, nfsmbc_attrs);

static const struct cfiattrdata * const nfsmb_attrs[] = { &i2cbuscf_iattrdata, NULL };
CFDRIVER_DECL(nfsmb, DV_DULL, nfsmb_attrs);

static const struct cfiattrdata * const ichsmb_attrs[] = { &i2cbuscf_iattrdata, NULL };
CFDRIVER_DECL(ichsmb, DV_DULL, ichsmb_attrs);

static const struct cfiattrdata * const cxdtv_attrs[] = { &i2cbuscf_iattrdata, &dtvbuscf_iattrdata, NULL };
CFDRIVER_DECL(cxdtv, DV_DULL, cxdtv_attrs);

static const struct cfiattrdata * const coram_attrs[] = { &i2cbuscf_iattrdata, &dtvbuscf_iattrdata, NULL };
CFDRIVER_DECL(coram, DV_DULL, coram_attrs);

CFDRIVER_DECL(pwdog, DV_DULL, NULL);

static const struct cfiattrdata * const i915drmkms_attrs[] = { &intelfbbuscf_iattrdata, NULL };
CFDRIVER_DECL(i915drmkms, DV_DULL, i915drmkms_attrs);

static const struct cfiattrdata * const intelfb_attrs[] = { &wsemuldisplaydevcf_iattrdata, &intelfbbuscf_iattrdata, NULL };
CFDRIVER_DECL(intelfb, DV_DULL, intelfb_attrs);

static const struct cfiattrdata * const radeon_attrs[] = { &radeonfbbuscf_iattrdata, NULL };
CFDRIVER_DECL(radeon, DV_DULL, radeon_attrs);

static const struct cfiattrdata * const radeondrmkmsfb_attrs[] = { &wsemuldisplaydevcf_iattrdata, &radeonfbbuscf_iattrdata, NULL };
CFDRIVER_DECL(radeondrmkmsfb, DV_DULL, radeondrmkmsfb_attrs);

static const struct cfiattrdata * const nouveau_attrs[] = { &nouveaufbbuscf_iattrdata, NULL };
CFDRIVER_DECL(nouveau, DV_DULL, nouveau_attrs);

static const struct cfiattrdata * const nouveaufb_attrs[] = { &wsemuldisplaydevcf_iattrdata, &nouveaufbbuscf_iattrdata, NULL };
CFDRIVER_DECL(nouveaufb, DV_DULL, nouveaufb_attrs);

static const struct cfiattrdata * const ismt_attrs[] = { &i2cbuscf_iattrdata, NULL };
CFDRIVER_DECL(ismt, DV_DULL, ismt_attrs);

CFDRIVER_DECL(agp, DV_DULL, NULL);

CFDRIVER_DECL(aapic, DV_DULL, NULL);

static const struct cfiattrdata * const pchb_attrs[] = { &agpbuscf_iattrdata, &pcibuscf_iattrdata, NULL };
CFDRIVER_DECL(pchb, DV_DULL, pchb_attrs);

static const struct cfiattrdata * const pcib_attrs[] = { &isabuscf_iattrdata, NULL };
CFDRIVER_DECL(pcib, DV_DULL, pcib_attrs);

static const struct cfiattrdata * const amdpcib_attrs[] = { &amdpcibcf_iattrdata, &isabuscf_iattrdata, NULL };
CFDRIVER_DECL(amdpcib, DV_DULL, amdpcib_attrs);

static const struct cfiattrdata * const amdnb_misc_attrs[] = { &amdnb_miscbuscf_iattrdata, NULL };
CFDRIVER_DECL(amdnb_misc, DV_DULL, amdnb_misc_attrs);

static const struct cfiattrdata * const amdsmn_attrs[] = { &amdsmnbuscf_iattrdata, NULL };
CFDRIVER_DECL(amdsmn, DV_DULL, amdsmn_attrs);

CFDRIVER_DECL(amdzentemp, DV_DULL, NULL);

CFDRIVER_DECL(amdtemp, DV_DULL, NULL);

static const struct cfiattrdata * const ichlpcib_attrs[] = { &tcoichbuscf_iattrdata, &gpiobuscf_iattrdata, &hpetichbuscf_iattrdata, &fwhichbuscf_iattrdata, &isabuscf_iattrdata, NULL };
CFDRIVER_DECL(ichlpcib, DV_DULL, ichlpcib_attrs);

CFDRIVER_DECL(tco, DV_DULL, NULL);

CFDRIVER_DECL(fwhrng, DV_DULL, NULL);

static const struct cfiattrdata * const isa_attrs[] = { &isacf_iattrdata, NULL };
CFDRIVER_DECL(isa, DV_DULL, isa_attrs);

static const struct cfiattrdata * const pcppi_attrs[] = { &pcppicf_iattrdata, NULL };
CFDRIVER_DECL(pcppi, DV_DULL, pcppi_attrs);

static const struct cfiattrdata * const pckbd_attrs[] = { &wskbddevcf_iattrdata, NULL };
CFDRIVER_DECL(pckbd, DV_DULL, pckbd_attrs);

static const struct cfiattrdata * const pms_attrs[] = { &wsmousedevcf_iattrdata, NULL };
CFDRIVER_DECL(pms, DV_DULL, pms_attrs);

CFDRIVER_DECL(sysbeep, DV_DULL, NULL);

static const struct cfiattrdata * const fdc_attrs[] = { &fdccf_iattrdata, NULL };
CFDRIVER_DECL(fdc, DV_DULL, fdc_attrs);

CFDRIVER_DECL(fd, DV_DISK, NULL);

static const struct cfiattrdata * const cardslot_attrs[] = { &pcmciabuscf_iattrdata, &cbbuscf_iattrdata, NULL };
CFDRIVER_DECL(cardslot, DV_DULL, cardslot_attrs);

static const struct cfiattrdata * const cardbus_attrs[] = { &cardbuscf_iattrdata, NULL };
CFDRIVER_DECL(cardbus, DV_DULL, cardbus_attrs);

static const struct cfiattrdata * const pcmcia_attrs[] = { &pcmciacf_iattrdata, NULL };
CFDRIVER_DECL(pcmcia, DV_DULL, pcmcia_attrs);

static const struct cfiattrdata * const pcmcom_attrs[] = { &pcmcomcf_iattrdata, NULL };
CFDRIVER_DECL(pcmcom, DV_DULL, pcmcom_attrs);

static const struct cfiattrdata * const mhzc_attrs[] = { &mhzccf_iattrdata, NULL };
CFDRIVER_DECL(mhzc, DV_DULL, mhzc_attrs);

static const struct cfiattrdata * const bt3c_attrs[] = { &btbuscf_iattrdata, NULL };
CFDRIVER_DECL(bt3c, DV_DULL, bt3c_attrs);

static const struct cfiattrdata * const btbc_attrs[] = { &btbuscf_iattrdata, NULL };
CFDRIVER_DECL(btbc, DV_DULL, btbc_attrs);

static const struct cfiattrdata * const usb_attrs[] = { &usbroothubifcf_iattrdata, NULL };
CFDRIVER_DECL(usb, DV_DULL, usb_attrs);

static const struct cfiattrdata * const uhub_attrs[] = { &usbififcf_iattrdata, &usbdevifcf_iattrdata, NULL };
CFDRIVER_DECL(uhub, DV_DULL, uhub_attrs);

static const struct cfiattrdata * const uaudio_attrs[] = { &audiobuscf_iattrdata, NULL };
CFDRIVER_DECL(uaudio, DV_DULL, uaudio_attrs);

static const struct cfiattrdata * const umidi_attrs[] = { &midibuscf_iattrdata, NULL };
CFDRIVER_DECL(umidi, DV_DULL, umidi_attrs);

CFDRIVER_DECL(ucom, DV_DULL, NULL);

CFDRIVER_DECL(ugen, DV_DULL, NULL);

CFDRIVER_DECL(ugenif, DV_DULL, NULL);

static const struct cfiattrdata * const uhidev_attrs[] = { &uhidbuscf_iattrdata, NULL };
CFDRIVER_DECL(uhidev, DV_DULL, uhidev_attrs);

CFDRIVER_DECL(uhid, DV_DULL, NULL);

static const struct cfiattrdata * const ukbd_attrs[] = { &wskbddevcf_iattrdata, NULL };
CFDRIVER_DECL(ukbd, DV_DULL, ukbd_attrs);

static const struct cfiattrdata * const ums_attrs[] = { &wsmousedevcf_iattrdata, NULL };
CFDRIVER_DECL(ums, DV_DULL, ums_attrs);

static const struct cfiattrdata * const uts_attrs[] = { &wsmousedevcf_iattrdata, NULL };
CFDRIVER_DECL(uts, DV_DULL, uts_attrs);

static const struct cfiattrdata * const uep_attrs[] = { &wsmousedevcf_iattrdata, NULL };
CFDRIVER_DECL(uep, DV_DULL, uep_attrs);

CFDRIVER_DECL(ucycom, DV_DULL, NULL);

CFDRIVER_DECL(ulpt, DV_DULL, NULL);

static const struct cfiattrdata * const umass_attrs[] = { &ata_hlcf_iattrdata, &atapicf_iattrdata, &scsicf_iattrdata, NULL };
CFDRIVER_DECL(umass, DV_DULL, umass_attrs);

static const struct cfiattrdata * const uirda_attrs[] = { &irbuscf_iattrdata, NULL };
CFDRIVER_DECL(uirda, DV_DULL, uirda_attrs);

static const struct cfiattrdata * const stuirda_attrs[] = { &irbuscf_iattrdata, NULL };
CFDRIVER_DECL(stuirda, DV_DULL, stuirda_attrs);

static const struct cfiattrdata * const ustir_attrs[] = { &irbuscf_iattrdata, NULL };
CFDRIVER_DECL(ustir, DV_DULL, ustir_attrs);

static const struct cfiattrdata * const irmce_attrs[] = { &irbuscf_iattrdata, NULL };
CFDRIVER_DECL(irmce, DV_DULL, irmce_attrs);

static const struct cfiattrdata * const ubt_attrs[] = { &btbuscf_iattrdata, NULL };
CFDRIVER_DECL(ubt, DV_DULL, ubt_attrs);

CFDRIVER_DECL(aubtfwl, DV_DULL, NULL);

static const struct cfiattrdata * const pseye_attrs[] = { &videobuscf_iattrdata, NULL };
CFDRIVER_DECL(pseye, DV_DULL, pseye_attrs);

static const struct cfiattrdata * const uvideo_attrs[] = { &videobuscf_iattrdata, NULL };
CFDRIVER_DECL(uvideo, DV_DULL, uvideo_attrs);

static const struct cfiattrdata * const auvitek_attrs[] = { &usbififcf_iattrdata, &i2cbuscf_iattrdata, &dtvbuscf_iattrdata, &videobuscf_iattrdata, NULL };
CFDRIVER_DECL(auvitek, DV_DULL, auvitek_attrs);

static const struct cfiattrdata * const emdtv_attrs[] = { &irbuscf_iattrdata, &dtvbuscf_iattrdata, NULL };
CFDRIVER_DECL(emdtv, DV_DULL, emdtv_attrs);

CFDRIVER_DECL(umodeswitch, DV_DULL, NULL);

CFDRIVER_DECL(urio, DV_DULL, NULL);

CFDRIVER_DECL(uipad, DV_DULL, NULL);

CFDRIVER_DECL(uberry, DV_DULL, NULL);

static const struct cfiattrdata * const uvisor_attrs[] = { &ucombuscf_iattrdata, NULL };
CFDRIVER_DECL(uvisor, DV_DULL, uvisor_attrs);

static const struct cfiattrdata * const ugensa_attrs[] = { &ucombuscf_iattrdata, NULL };
CFDRIVER_DECL(ugensa, DV_DULL, ugensa_attrs);

static const struct cfiattrdata * const u3g_attrs[] = { &ucombuscf_iattrdata, NULL };
CFDRIVER_DECL(u3g, DV_DULL, u3g_attrs);

CFDRIVER_DECL(uyap, DV_DULL, NULL);

static const struct cfiattrdata * const udsbr_attrs[] = { &radiodevcf_iattrdata, NULL };
CFDRIVER_DECL(udsbr, DV_DULL, udsbr_attrs);

static const struct cfiattrdata * const slurm_attrs[] = { &radiodevcf_iattrdata, NULL };
CFDRIVER_DECL(slurm, DV_DULL, slurm_attrs);

CFDRIVER_DECL(uthum, DV_DULL, NULL);

static const struct cfiattrdata * const aue_attrs[] = { &miicf_iattrdata, NULL };
CFDRIVER_DECL(aue, DV_IFNET, aue_attrs);

CFDRIVER_DECL(cdce, DV_IFNET, NULL);

CFDRIVER_DECL(cue, DV_IFNET, NULL);

CFDRIVER_DECL(kue, DV_IFNET, NULL);

CFDRIVER_DECL(upl, DV_IFNET, NULL);

static const struct cfiattrdata * const url_attrs[] = { &miicf_iattrdata, NULL };
CFDRIVER_DECL(url, DV_IFNET, url_attrs);

static const struct cfiattrdata * const axe_attrs[] = { &miicf_iattrdata, NULL };
CFDRIVER_DECL(axe, DV_IFNET, axe_attrs);

static const struct cfiattrdata * const axen_attrs[] = { &miicf_iattrdata, NULL };
CFDRIVER_DECL(axen, DV_IFNET, axen_attrs);

static const struct cfiattrdata * const mue_attrs[] = { &miicf_iattrdata, NULL };
CFDRIVER_DECL(mue, DV_IFNET, mue_attrs);

static const struct cfiattrdata * const udav_attrs[] = { &miicf_iattrdata, NULL };
CFDRIVER_DECL(udav, DV_IFNET, udav_attrs);

CFDRIVER_DECL(otus, DV_IFNET, NULL);

static const struct cfiattrdata * const ure_attrs[] = { &miicf_iattrdata, NULL };
CFDRIVER_DECL(ure, DV_IFNET, ure_attrs);

static const struct cfiattrdata * const umodem_attrs[] = { &ucombuscf_iattrdata, NULL };
CFDRIVER_DECL(umodem, DV_DULL, umodem_attrs);

static const struct cfiattrdata * const uftdi_attrs[] = { &ucombuscf_iattrdata, NULL };
CFDRIVER_DECL(uftdi, DV_DULL, uftdi_attrs);

static const struct cfiattrdata * const uplcom_attrs[] = { &ucombuscf_iattrdata, NULL };
CFDRIVER_DECL(uplcom, DV_DULL, uplcom_attrs);

static const struct cfiattrdata * const umct_attrs[] = { &ucombuscf_iattrdata, NULL };
CFDRIVER_DECL(umct, DV_DULL, umct_attrs);

static const struct cfiattrdata * const umcs_attrs[] = { &ucombuscf_iattrdata, NULL };
CFDRIVER_DECL(umcs, DV_DULL, umcs_attrs);

static const struct cfiattrdata * const uvscom_attrs[] = { &ucombuscf_iattrdata, NULL };
CFDRIVER_DECL(uvscom, DV_DULL, uvscom_attrs);

static const struct cfiattrdata * const uxrcom_attrs[] = { &ucombuscf_iattrdata, NULL };
CFDRIVER_DECL(uxrcom, DV_DULL, uxrcom_attrs);

static const struct cfiattrdata * const ubsa_attrs[] = { &ucombuscf_iattrdata, NULL };
CFDRIVER_DECL(ubsa, DV_DULL, ubsa_attrs);

static const struct cfiattrdata * const uipaq_attrs[] = { &ucombuscf_iattrdata, NULL };
CFDRIVER_DECL(uipaq, DV_DULL, uipaq_attrs);

static const struct cfiattrdata * const ukyopon_attrs[] = { &ucombuscf_iattrdata, NULL };
CFDRIVER_DECL(ukyopon, DV_DULL, ukyopon_attrs);

static const struct cfiattrdata * const uark_attrs[] = { &ucombuscf_iattrdata, NULL };
CFDRIVER_DECL(uark, DV_DULL, uark_attrs);

static const struct cfiattrdata * const uslsa_attrs[] = { &ucombuscf_iattrdata, NULL };
CFDRIVER_DECL(uslsa, DV_DULL, uslsa_attrs);

static const struct cfiattrdata * const uchcom_attrs[] = { &ucombuscf_iattrdata, NULL };
CFDRIVER_DECL(uchcom, DV_DULL, uchcom_attrs);

static const struct cfiattrdata * const usscanner_attrs[] = { &scsicf_iattrdata, NULL };
CFDRIVER_DECL(usscanner, DV_DULL, usscanner_attrs);

CFDRIVER_DECL(atu, DV_IFNET, NULL);

CFDRIVER_DECL(upgt, DV_IFNET, NULL);

static const struct cfiattrdata * const usmsc_attrs[] = { &miicf_iattrdata, NULL };
CFDRIVER_DECL(usmsc, DV_IFNET, usmsc_attrs);

CFDRIVER_DECL(ural, DV_IFNET, NULL);

CFDRIVER_DECL(rum, DV_IFNET, NULL);

CFDRIVER_DECL(utoppy, DV_DULL, NULL);

CFDRIVER_DECL(zyd, DV_IFNET, NULL);

static const struct cfiattrdata * const udl_attrs[] = { &wsemuldisplaydevcf_iattrdata, NULL };
CFDRIVER_DECL(udl, DV_DULL, udl_attrs);

CFDRIVER_DECL(uhso, DV_IFNET, NULL);

CFDRIVER_DECL(urndis, DV_IFNET, NULL);

CFDRIVER_DECL(urtwn, DV_IFNET, NULL);

CFDRIVER_DECL(urtw, DV_IFNET, NULL);

CFDRIVER_DECL(run, DV_IFNET, NULL);

static const struct cfiattrdata * const udsir_attrs[] = { &irbuscf_iattrdata, NULL };
CFDRIVER_DECL(udsir, DV_DULL, udsir_attrs);

static const struct cfiattrdata * const bthub_attrs[] = { &bthubcf_iattrdata, NULL };
CFDRIVER_DECL(bthub, DV_DULL, bthub_attrs);

static const struct cfiattrdata * const bthidev_attrs[] = { &bthidbuscf_iattrdata, NULL };
CFDRIVER_DECL(bthidev, DV_DULL, bthidev_attrs);

static const struct cfiattrdata * const btkbd_attrs[] = { &wskbddevcf_iattrdata, NULL };
CFDRIVER_DECL(btkbd, DV_DULL, btkbd_attrs);

static const struct cfiattrdata * const btms_attrs[] = { &wsmousedevcf_iattrdata, NULL };
CFDRIVER_DECL(btms, DV_DULL, btms_attrs);

static const struct cfiattrdata * const btmagic_attrs[] = { &wsmousedevcf_iattrdata, NULL };
CFDRIVER_DECL(btmagic, DV_DULL, btmagic_attrs);

static const struct cfiattrdata * const btsco_attrs[] = { &audiobuscf_iattrdata, NULL };
CFDRIVER_DECL(btsco, DV_DULL, btsco_attrs);

static const struct cfiattrdata * const btuart_attrs[] = { &btbuscf_iattrdata, NULL };
CFDRIVER_DECL(btuart, DV_DULL, btuart_attrs);

static const struct cfiattrdata * const bcsp_attrs[] = { &btbuscf_iattrdata, NULL };
CFDRIVER_DECL(bcsp, DV_DULL, bcsp_attrs);

static const struct cfiattrdata * const sdmmc_attrs[] = { &sdmmccf_iattrdata, NULL };
CFDRIVER_DECL(sdmmc, DV_DULL, sdmmc_attrs);

static const struct cfiattrdata * const sbt_attrs[] = { &btbuscf_iattrdata, NULL };
CFDRIVER_DECL(sbt, DV_DULL, sbt_attrs);

static const struct cfiattrdata * const ieee1394if_attrs[] = { &ieee1394ifcf_iattrdata, NULL };
CFDRIVER_DECL(ieee1394if, DV_DULL, ieee1394if_attrs);

CFDRIVER_DECL(fwip, DV_IFNET, NULL);

static const struct cfiattrdata * const sbp_attrs[] = { &sbpcf_iattrdata, &scsicf_iattrdata, NULL };
CFDRIVER_DECL(sbp, DV_DULL, sbp_attrs);

static const struct cfiattrdata * const acpi_attrs[] = { &acpiwdrtbuscf_iattrdata, &acpihpetbuscf_iattrdata, &acpimadtbuscf_iattrdata, &acpigtdtbuscf_iattrdata, &acpisdtbuscf_iattrdata, &acpiecdtbuscf_iattrdata, &acpinodebuscf_iattrdata, &acpiapmbuscf_iattrdata, NULL };
CFDRIVER_DECL(acpi, DV_DULL, acpi_attrs);

CFDRIVER_DECL(acpiec, DV_DULL, NULL);

CFDRIVER_DECL(acpiecdt, DV_DULL, NULL);

CFDRIVER_DECL(acpilid, DV_DULL, NULL);

CFDRIVER_DECL(acpibut, DV_DULL, NULL);

CFDRIVER_DECL(acpiacad, DV_DULL, NULL);

CFDRIVER_DECL(acpibat, DV_DULL, NULL);

static const struct cfiattrdata * const acpivga_attrs[] = { &acpivgacf_iattrdata, NULL };
CFDRIVER_DECL(acpivga, DV_DULL, acpivga_attrs);

CFDRIVER_DECL(acpiout, DV_DULL, NULL);

CFDRIVER_DECL(acpifan, DV_DULL, NULL);

CFDRIVER_DECL(acpitz, DV_DULL, NULL);

CFDRIVER_DECL(acpiwdrt, DV_DULL, NULL);

CFDRIVER_DECL(acpidalb, DV_DULL, NULL);

static const struct cfiattrdata * const ipmi_acpi_attrs[] = { &ipmibuscf_iattrdata, NULL };
CFDRIVER_DECL(ipmi_acpi, DV_DULL, ipmi_acpi_attrs);

static const struct cfiattrdata * const acpiwmi_attrs[] = { &acpiwmibuscf_iattrdata, NULL };
CFDRIVER_DECL(acpiwmi, DV_DULL, acpiwmi_attrs);


struct cfdriver * const cfdriver_list_initial[] = {
	&spkr_cd,
	&audio_cd,
	&midi_cd,
	&hdaudio_cd,
	&hdafg_cd,
	&video_cd,
	&dtv_cd,
	&iic_cd,
	&ihidev_cd,
	&ims_cd,
	&irframe_cd,
	&cir_cd,
	&lpt_cd,
	&tpm_cd,
	&ld_cd,
	&raid_cd,
	&cy_cd,
	&sm_cd,
	&com_cd,
	&pckbc_cd,
	&attimer_cd,
	&opl_cd,
	&mpu_cd,
	&virtio_cd,
	&viomb_cd,
	&vioif_cd,
	&viornd_cd,
	&vioscsi_cd,
	&wdc_cd,
	&atabus_cd,
	&njata_cd,
	&ahcisata_cd,
	&siisata_cd,
	&mvsata_cd,
	&dwiic_cd,
	&hpet_cd,
	&vga_cd,
	&wsdisplay_cd,
	&wskbd_cd,
	&wsmouse_cd,
	&genfb_cd,
	&uhci_cd,
	&ohci_cd,
	&ehci_cd,
	&xhci_cd,
	&slhci_cd,
	&sdhc_cd,
	&rtsx_cd,
	&radio_cd,
	&fwohci_cd,
	&nvme_cd,
	&bwfm_cd,
	&qemufwcfg_cd,
	&vnd_cd,
	&cgd_cd,
	&md_cd,
	&fss_cd,
	&tap_cd,
	&l2tp_cd,
	&ipmi_cd,
	&joy_cd,
	&gpio_cd,
	&gpioow_cd,
	&onewire_cd,
	&pad_cd,
	&athn_cd,
	&cpu_cd,
	&acpicpu_cd,
	&coretemp_cd,
	&est_cd,
	&powernow_cd,
	&ioapic_cd,
	&scsibus_cd,
	&atapibus_cd,
	&cd_cd,
	&ch_cd,
	&sd_cd,
	&st_cd,
	&ses_cd,
	&ss_cd,
	&uk_cd,
	&wd_cd,
	&ataraid_cd,
	&iop_cd,
	&iopsp_cd,
	&mainbus_cd,
	&pci_cd,
	&pciide_cd,
	&acardide_cd,
	&aceride_cd,
	&artsata_cd,
	&cmdide_cd,
	&cypide_cd,
	&hptide_cd,
	&iteide_cd,
	&jmide_cd,
	&optiide_cd,
	&piixide_cd,
	&pdcsata_cd,
	&pdcide_cd,
	&svwsata_cd,
	&satalink_cd,
	&siside_cd,
	&slide_cd,
	&viaide_cd,
	&ixpide_cd,
	&toshide_cd,
	&ppb_cd,
	&cz_cd,
	&bktr_cd,
	&clcs_cd,
	&clct_cd,
	&fms_cd,
	&eap_cd,
	&auacer_cd,
	&auich_cd,
	&auvia_cd,
	&auixp_cd,
	&neo_cd,
	&esa_cd,
	&eso_cd,
	&esm_cd,
	&sv_cd,
	&cmpci_cd,
	&yds_cd,
	&emuxki_cd,
	&autri_cd,
	&puc_cd,
	&cbb_cd,
	&piixpm_cd,
	&amdpm_cd,
	&hifn_cd,
	&ubsec_cd,
	&weasel_cd,
	&nfsmbc_cd,
	&nfsmb_cd,
	&ichsmb_cd,
	&cxdtv_cd,
	&coram_cd,
	&pwdog_cd,
	&i915drmkms_cd,
	&intelfb_cd,
	&radeon_cd,
	&radeondrmkmsfb_cd,
	&nouveau_cd,
	&nouveaufb_cd,
	&ismt_cd,
	&agp_cd,
	&aapic_cd,
	&pchb_cd,
	&pcib_cd,
	&amdpcib_cd,
	&amdnb_misc_cd,
	&amdsmn_cd,
	&amdzentemp_cd,
	&amdtemp_cd,
	&ichlpcib_cd,
	&tco_cd,
	&fwhrng_cd,
	&isa_cd,
	&pcppi_cd,
	&pckbd_cd,
	&pms_cd,
	&sysbeep_cd,
	&fdc_cd,
	&fd_cd,
	&cardslot_cd,
	&cardbus_cd,
	&pcmcia_cd,
	&pcmcom_cd,
	&mhzc_cd,
	&bt3c_cd,
	&btbc_cd,
	&usb_cd,
	&uhub_cd,
	&uaudio_cd,
	&umidi_cd,
	&ucom_cd,
	&ugen_cd,
	&ugenif_cd,
	&uhidev_cd,
	&uhid_cd,
	&ukbd_cd,
	&ums_cd,
	&uts_cd,
	&uep_cd,
	&ucycom_cd,
	&ulpt_cd,
	&umass_cd,
	&uirda_cd,
	&stuirda_cd,
	&ustir_cd,
	&irmce_cd,
	&ubt_cd,
	&aubtfwl_cd,
	&pseye_cd,
	&uvideo_cd,
	&auvitek_cd,
	&emdtv_cd,
	&umodeswitch_cd,
	&urio_cd,
	&uipad_cd,
	&uberry_cd,
	&uvisor_cd,
	&ugensa_cd,
	&u3g_cd,
	&uyap_cd,
	&udsbr_cd,
	&slurm_cd,
	&uthum_cd,
	&aue_cd,
	&cdce_cd,
	&cue_cd,
	&kue_cd,
	&upl_cd,
	&url_cd,
	&axe_cd,
	&axen_cd,
	&mue_cd,
	&udav_cd,
	&otus_cd,
	&ure_cd,
	&umodem_cd,
	&uftdi_cd,
	&uplcom_cd,
	&umct_cd,
	&umcs_cd,
	&uvscom_cd,
	&uxrcom_cd,
	&ubsa_cd,
	&uipaq_cd,
	&ukyopon_cd,
	&uark_cd,
	&uslsa_cd,
	&uchcom_cd,
	&usscanner_cd,
	&atu_cd,
	&upgt_cd,
	&usmsc_cd,
	&ural_cd,
	&rum_cd,
	&utoppy_cd,
	&zyd_cd,
	&udl_cd,
	&uhso_cd,
	&urndis_cd,
	&urtwn_cd,
	&urtw_cd,
	&run_cd,
	&udsir_cd,
	&bthub_cd,
	&bthidev_cd,
	&btkbd_cd,
	&btms_cd,
	&btmagic_cd,
	&btsco_cd,
	&btuart_cd,
	&bcsp_cd,
	&sdmmc_cd,
	&sbt_cd,
	&ieee1394if_cd,
	&fwip_cd,
	&sbp_cd,
	&acpi_cd,
	&acpiec_cd,
	&acpiecdt_cd,
	&acpilid_cd,
	&acpibut_cd,
	&acpiacad_cd,
	&acpibat_cd,
	&acpivga_cd,
	&acpiout_cd,
	&acpifan_cd,
	&acpitz_cd,
	&acpiwdrt_cd,
	&acpidalb_cd,
	&ipmi_acpi_cd,
	&acpiwmi_cd,
	NULL
};

extern struct cfattach audio_ca;
extern struct cfattach midi_ca;
extern struct cfattach spkr_audio_ca;
extern struct cfattach hdafg_ca;
extern struct cfattach video_ca;
extern struct cfattach dtv_ca;
extern struct cfattach iic_ca;
extern struct cfattach ihidev_ca;
extern struct cfattach ims_ca;
extern struct cfattach irframe_ca;
extern struct cfattach cir_ca;
extern struct cfattach ld_virtio_ca;
extern struct cfattach viomb_ca;
extern struct cfattach vioif_ca;
extern struct cfattach viornd_ca;
extern struct cfattach vioscsi_ca;
extern struct cfattach atabus_ca;
extern struct cfattach wsdisplay_emul_ca;
extern struct cfattach wskbd_ca;
extern struct cfattach wsmouse_ca;
extern struct cfattach radio_ca;
extern struct cfattach ld_nvme_ca;
extern struct cfattach ipmi_ca;
extern struct cfattach gpio_ca;
extern struct cfattach gpioow_ca;
extern struct cfattach onewire_ca;
extern struct cfattach cpu_ca;
extern struct cfattach acpicpu_ca;
extern struct cfattach coretemp_ca;
extern struct cfattach est_ca;
extern struct cfattach powernow_ca;
extern struct cfattach ioapic_ca;
extern struct cfattach scsibus_ca;
extern struct cfattach atapibus_ca;
extern struct cfattach cd_ca;
extern struct cfattach ch_ca;
extern struct cfattach sd_ca;
extern struct cfattach st_scsibus_ca;
extern struct cfattach st_atapibus_ca;
extern struct cfattach ses_ca;
extern struct cfattach ss_ca;
extern struct cfattach uk_ca;
extern struct cfattach wd_ca;
extern struct cfattach ld_ataraid_ca;
extern struct cfattach ld_iop_ca;
extern struct cfattach iopsp_ca;
extern struct cfattach mainbus_ca;
extern struct cfattach pci_ca;
extern struct cfattach iop_pci_ca;
extern struct cfattach pciide_ca;
extern struct cfattach ahcisata_pci_ca;
extern struct cfattach acardide_ca;
extern struct cfattach aceride_ca;
extern struct cfattach artsata_ca;
extern struct cfattach cmdide_ca;
extern struct cfattach cypide_ca;
extern struct cfattach hptide_ca;
extern struct cfattach iteide_ca;
extern struct cfattach jmide_ca;
extern struct cfattach jmahci_ca;
extern struct cfattach optiide_ca;
extern struct cfattach piixide_ca;
extern struct cfattach pdcsata_ca;
extern struct cfattach pdcide_ca;
extern struct cfattach svwsata_ca;
extern struct cfattach satalink_ca;
extern struct cfattach siside_ca;
extern struct cfattach slide_ca;
extern struct cfattach viaide_ca;
extern struct cfattach ixpide_ca;
extern struct cfattach toshide_ca;
extern struct cfattach ppb_ca;
extern struct cfattach cy_pci_ca;
extern struct cfattach cz_ca;
extern struct cfattach vga_pci_ca;
extern struct cfattach bktr_ca;
extern struct cfattach clcs_ca;
extern struct cfattach clct_ca;
extern struct cfattach fms_ca;
extern struct cfattach opl_fms_ca;
extern struct cfattach eap_ca;
extern struct cfattach auacer_ca;
extern struct cfattach auich_ca;
extern struct cfattach auvia_ca;
extern struct cfattach auixp_ca;
extern struct cfattach neo_ca;
extern struct cfattach esa_ca;
extern struct cfattach eso_ca;
extern struct cfattach opl_eso_ca;
extern struct cfattach mpu_eso_ca;
extern struct cfattach esm_ca;
extern struct cfattach sv_ca;
extern struct cfattach opl_sv_ca;
extern struct cfattach cmpci_ca;
extern struct cfattach opl_cmpci_ca;
extern struct cfattach mpu_cmpci_ca;
extern struct cfattach yds_ca;
extern struct cfattach mpu_yds_ca;
extern struct cfattach emuxki_ca;
extern struct cfattach autri_ca;
extern struct cfattach puc_ca;
extern struct cfattach com_puc_ca;
extern struct cfattach lpt_puc_ca;
extern struct cfattach uhci_pci_ca;
extern struct cfattach ohci_pci_ca;
extern struct cfattach ehci_pci_ca;
extern struct cfattach xhci_pci_ca;
extern struct cfattach fwohci_pci_ca;
extern struct cfattach cbb_pci_ca;
extern struct cfattach piixpm_ca;
extern struct cfattach amdpm_ca;
extern struct cfattach hifn_ca;
extern struct cfattach ubsec_ca;
extern struct cfattach weasel_pci_ca;
extern struct cfattach sdhc_pci_ca;
extern struct cfattach genfb_pci_ca;
extern struct cfattach nfsmbc_ca;
extern struct cfattach nfsmb_ca;
extern struct cfattach ichsmb_ca;
extern struct cfattach siisata_pci_ca;
extern struct cfattach mvsata_pci_ca;
extern struct cfattach hdaudio_pci_ca;
extern struct cfattach cxdtv_ca;
extern struct cfattach coram_ca;
extern struct cfattach pwdog_ca;
extern struct cfattach virtio_pci_ca;
extern struct cfattach rtsx_pci_ca;
extern struct cfattach nvme_pci_ca;
extern struct cfattach i915drmkms_ca;
extern struct cfattach intelfb_ca;
extern struct cfattach radeon_ca;
extern struct cfattach radeondrmkmsfb_ca;
extern struct cfattach nouveau_pci_ca;
extern struct cfattach nouveaufb_ca;
extern struct cfattach ismt_ca;
extern struct cfattach agp_ca;
extern struct cfattach aapic_ca;
extern struct cfattach pchb_ca;
extern struct cfattach pcib_ca;
extern struct cfattach amdpcib_ca;
extern struct cfattach amdpcib_hpet_ca;
extern struct cfattach amdnb_misc_ca;
extern struct cfattach amdsmn_ca;
extern struct cfattach amdzentemp_ca;
extern struct cfattach amdtemp_ca;
extern struct cfattach pcidwiic_ca;
extern struct cfattach ichlpcib_ca;
extern struct cfattach tco_ca;
extern struct cfattach fwhrng_ca;
extern struct cfattach isa_ca;
extern struct cfattach com_isa_ca;
extern struct cfattach lpt_isa_ca;
extern struct cfattach wdc_isa_ca;
extern struct cfattach pckbc_isa_ca;
extern struct cfattach pcppi_ca;
extern struct cfattach spkr_pcppi_ca;
extern struct cfattach midi_pcppi_ca;
extern struct cfattach attimer_isa_ca;
extern struct cfattach tpm_isa_ca;
extern struct cfattach pckbd_ca;
extern struct cfattach pms_ca;
extern struct cfattach sysbeep_ca;
extern struct cfattach fdc_isa_ca;
extern struct cfattach fd_ca;
extern struct cfattach cardslot_ca;
extern struct cfattach cardbus_ca;
extern struct cfattach com_cardbus_ca;
extern struct cfattach ohci_cardbus_ca;
extern struct cfattach uhci_cardbus_ca;
extern struct cfattach ehci_cardbus_ca;
extern struct cfattach fwohci_cardbus_ca;
extern struct cfattach njata_cardbus_ca;
extern struct cfattach siisata_cardbus_ca;
extern struct cfattach sdhc_cardbus_ca;
extern struct cfattach pcmcia_ca;
extern struct cfattach com_pcmcia_ca;
extern struct cfattach wdc_pcmcia_ca;
extern struct cfattach pcmcom_ca;
extern struct cfattach com_pcmcom_ca;
extern struct cfattach mhzc_ca;
extern struct cfattach com_mhzc_ca;
extern struct cfattach sm_mhzc_ca;
extern struct cfattach bt3c_ca;
extern struct cfattach slhci_pcmcia_ca;
extern struct cfattach btbc_ca;
extern struct cfattach usb_ca;
extern struct cfattach uroothub_ca;
extern struct cfattach uhub_ca;
extern struct cfattach uaudio_ca;
extern struct cfattach umidi_ca;
extern struct cfattach ucom_ca;
extern struct cfattach ugen_ca;
extern struct cfattach ugenif_ca;
extern struct cfattach uhidev_ca;
extern struct cfattach uhid_ca;
extern struct cfattach ukbd_ca;
extern struct cfattach ums_ca;
extern struct cfattach uts_ca;
extern struct cfattach uep_ca;
extern struct cfattach ucycom_ca;
extern struct cfattach ulpt_ca;
extern struct cfattach umass_ca;
extern struct cfattach uirda_ca;
extern struct cfattach stuirda_ca;
extern struct cfattach ustir_ca;
extern struct cfattach irmce_ca;
extern struct cfattach ubt_ca;
extern struct cfattach aubtfwl_ca;
extern struct cfattach pseye_ca;
extern struct cfattach uvideo_ca;
extern struct cfattach auvitek_ca;
extern struct cfattach emdtv_ca;
extern struct cfattach umodeswitch_ca;
extern struct cfattach urio_ca;
extern struct cfattach uipad_ca;
extern struct cfattach uberry_ca;
extern struct cfattach uvisor_ca;
extern struct cfattach ugensa_ca;
extern struct cfattach u3g_ca;
extern struct cfattach uyap_ca;
extern struct cfattach udsbr_ca;
extern struct cfattach slurm_ca;
extern struct cfattach uthum_ca;
extern struct cfattach aue_ca;
extern struct cfattach cdce_ca;
extern struct cfattach cue_ca;
extern struct cfattach kue_ca;
extern struct cfattach upl_ca;
extern struct cfattach url_ca;
extern struct cfattach axe_ca;
extern struct cfattach axen_ca;
extern struct cfattach mue_ca;
extern struct cfattach udav_ca;
extern struct cfattach otus_ca;
extern struct cfattach ure_ca;
extern struct cfattach umodem_ca;
extern struct cfattach uftdi_ca;
extern struct cfattach uplcom_ca;
extern struct cfattach umct_ca;
extern struct cfattach umcs_ca;
extern struct cfattach uvscom_ca;
extern struct cfattach uxrcom_ca;
extern struct cfattach ubsa_ca;
extern struct cfattach uipaq_ca;
extern struct cfattach ukyopon_ca;
extern struct cfattach uark_ca;
extern struct cfattach uslsa_ca;
extern struct cfattach uchcom_ca;
extern struct cfattach usscanner_ca;
extern struct cfattach atu_ca;
extern struct cfattach upgt_ca;
extern struct cfattach usmsc_ca;
extern struct cfattach ural_ca;
extern struct cfattach rum_ca;
extern struct cfattach utoppy_ca;
extern struct cfattach zyd_ca;
extern struct cfattach udl_ca;
extern struct cfattach uhso_ca;
extern struct cfattach urndis_ca;
extern struct cfattach urtwn_ca;
extern struct cfattach urtw_ca;
extern struct cfattach run_ca;
extern struct cfattach athn_usb_ca;
extern struct cfattach udsir_ca;
extern struct cfattach bwfm_usb_ca;
extern struct cfattach bthub_ca;
extern struct cfattach bthidev_ca;
extern struct cfattach btkbd_ca;
extern struct cfattach btms_ca;
extern struct cfattach btmagic_ca;
extern struct cfattach btsco_ca;
extern struct cfattach sdmmc_ca;
extern struct cfattach ld_sdmmc_ca;
extern struct cfattach sbt_ca;
extern struct cfattach ieee1394if_ca;
extern struct cfattach fwip_ca;
extern struct cfattach sbp_ca;
extern struct cfattach acpi_ca;
extern struct cfattach acpiec_ca;
extern struct cfattach acpiecdt_ca;
extern struct cfattach acpilid_ca;
extern struct cfattach acpibut_ca;
extern struct cfattach acpiacad_ca;
extern struct cfattach acpibat_ca;
extern struct cfattach acpivga_ca;
extern struct cfattach acpiout_ca;
extern struct cfattach acpifan_ca;
extern struct cfattach acpitz_ca;
extern struct cfattach acpiwdrt_ca;
extern struct cfattach pckbc_acpi_ca;
extern struct cfattach mpu_acpi_ca;
extern struct cfattach joy_acpi_ca;
extern struct cfattach pcppi_acpi_ca;
extern struct cfattach attimer_acpi_ca;
extern struct cfattach hpet_acpi_dev_ca;
extern struct cfattach hpet_acpi_tab_ca;
extern struct cfattach acpidalb_ca;
extern struct cfattach sdhc_acpi_ca;
extern struct cfattach qemufwcfg_acpi_ca;
extern struct cfattach ipmi_acpi_ca;
extern struct cfattach acpiwmi_ca;

/* locators */
static int loc[811] = {
	0x378, 0, -1, 0, 7, -1, -1, 0x278,
	0, -1, 0, -1, -1, -1, -1, 0,
	0xfed40000, 0, 7, -1, -1, 0x3f8, 0, -1,
	0, 4, -1, -1, 0x2f8, 0, -1, 0,
	3, -1, -1, -1, 0, -1, 0, -1,
	-1, -1, -1, 0, -1, 0, -1, -1,
	-1, 0x1f0, 0, -1, 0, 14, -1, -1,
	0x170, 0, -1, 0, 15, -1, -1, -1,
	0, -1, 0, -1, -1, -1, 0x3f0, 0,
	-1, 0, 6, 2, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	1, 1, 0x1050, 0x114, -1, -1, 1, 1,
	0x1050, 0x115, -1, -1, 1, 2, 0x1050, 0x116,
	-1, -1, 1, 1, 0x1050, 0x405, -1, -1,
	1, 1, 0x1050, 0x406, -1, -1, 1, 2,
	0x1050, 0x407, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, 0, 0, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, 1, -1, 1, -1, 1, -1, 1,
	-1, 1, -1, 1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	0, 0, 0, 0, 0, 0, 0, 0,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1, -1, -1, -1, -1, -1,
	-1, -1, -1,
};

static const struct cfparent pspec0 = {
	"cpubus", "mainbus", DVUNIT_ANY
};
static const struct cfparent pspec1 = {
	"ioapicbus", "mainbus", DVUNIT_ANY
};
static const struct cfparent pspec2 = {
	"cpufeaturebus", "cpu", DVUNIT_ANY
};
static const struct cfparent pspec3 = {
	"cpufeaturebus", "cpu", 0
};
static const struct cfparent pspec4 = {
	"ipmibus", "mainbus", DVUNIT_ANY
};
static const struct cfparent pspec5 = {
	"acpinodebus", "acpi", DVUNIT_ANY
};
static const struct cfparent pspec6 = {
	"ipmibus", "ipmi_acpi", DVUNIT_ANY
};
static const struct cfparent pspec7 = {
	"acpibus", "mainbus", 0
};
static const struct cfparent pspec8 = {
	"acpiecdtbus", "acpi", DVUNIT_ANY
};
static const struct cfparent pspec9 = {
	"acpivga", "acpivga", DVUNIT_ANY
};
static const struct cfparent pspec10 = {
	"acpiwdrtbus", "acpi", DVUNIT_ANY
};
static const struct cfparent pspec11 = {
	"acpihpetbus", NULL, 0
};
static const struct cfparent pspec12 = {
	"acpinodebus", NULL, 0
};
static const struct cfparent pspec13 = {
	"pcibus", "mainbus", DVUNIT_ANY
};
static const struct cfparent pspec14 = {
	"pcibus", "pchb", DVUNIT_ANY
};
static const struct cfparent pspec15 = {
	"pcibus", "ppb", DVUNIT_ANY
};
static const struct cfparent pspec16 = {
	"pci", "pci", DVUNIT_ANY
};
static const struct cfparent pspec17 = {
	"amdpcib", "amdpcib", DVUNIT_ANY
};
static const struct cfparent pspec18 = {
	"fwhichbus", "ichlpcib", DVUNIT_ANY
};
static const struct cfparent pspec19 = {
	"tcoichbus", "ichlpcib", DVUNIT_ANY
};
static const struct cfparent pspec20 = {
	"agpbus", "pchb", DVUNIT_ANY
};
static const struct cfparent pspec21 = {
	"isabus", "mainbus", DVUNIT_ANY
};
static const struct cfparent pspec22 = {
	"isabus", "pcib", DVUNIT_ANY
};
static const struct cfparent pspec23 = {
	"isabus", "ichlpcib", DVUNIT_ANY
};
static const struct cfparent pspec24 = {
	"pcmciaslot", "cbb", DVUNIT_ANY
};
static const struct cfparent pspec25 = {
	"cbbus", "cardslot", DVUNIT_ANY
};
static const struct cfparent pspec26 = {
	"pcmciabus", "cardslot", DVUNIT_ANY
};
static const struct cfparent pspec27 = {
	"isa", "isa", DVUNIT_ANY
};
static const struct cfparent pspec28 = {
	"pckbport", "pckbc", DVUNIT_ANY
};
static const struct cfparent pspec29 = {
	"wsemuldisplaydev", "vga", DVUNIT_ANY
};
static const struct cfparent pspec30 = {
	"wsemuldisplaydev", NULL, 0
};
static const struct cfparent pspec31 = {
	"wskbddev", "pckbd", DVUNIT_ANY
};
static const struct cfparent pspec32 = {
	"wsmousedev", "pms", DVUNIT_ANY
};
static const struct cfparent pspec33 = {
	"wsmousedev", NULL, 0
};
static const struct cfparent pspec34 = {
	"pcppi", "pcppi", DVUNIT_ANY
};
static const struct cfparent pspec35 = {
	"intelfbbus", NULL, 0
};
static const struct cfparent pspec36 = {
	"radeonfbbus", NULL, 0
};
static const struct cfparent pspec37 = {
	"nouveaufbbus", NULL, 0
};
static const struct cfparent pspec38 = {
	"puc", "puc", DVUNIT_ANY
};
static const struct cfparent pspec39 = {
	"pcmcia", "pcmcia", DVUNIT_ANY
};
static const struct cfparent pspec40 = {
	"pcmcom", "pcmcom", DVUNIT_ANY
};
static const struct cfparent pspec41 = {
	"cardbus", "cardbus", DVUNIT_ANY
};
static const struct cfparent pspec42 = {
	"amdnb_miscbus", "amdnb_misc", DVUNIT_ANY
};
static const struct cfparent pspec43 = {
	"amdsmnbus", NULL, 0
};
static const struct cfparent pspec44 = {
	"nfsmbc", "nfsmbc", DVUNIT_ANY
};
static const struct cfparent pspec45 = {
	"i2cbus", "nfsmb", DVUNIT_ANY
};
static const struct cfparent pspec46 = {
	"i2cbus", "piixpm", DVUNIT_ANY
};
static const struct cfparent pspec47 = {
	"i2cbus", "ichsmb", DVUNIT_ANY
};
static const struct cfparent pspec48 = {
	"i2cbus", "ismt", DVUNIT_ANY
};
static const struct cfparent pspec49 = {
	"i2cbus", "dwiic", DVUNIT_ANY
};
static const struct cfparent pspec50 = {
	"iic", "iic", DVUNIT_ANY
};
static const struct cfparent pspec51 = {
	"ihidbus", "ihidev", DVUNIT_ANY
};
static const struct cfparent pspec52 = {
	"wsmousedev", "ims", DVUNIT_ANY
};
static const struct cfparent pspec53 = {
	"iop", "iop", DVUNIT_ANY
};
static const struct cfparent pspec54 = {
	"gpiobus", NULL, 0
};
static const struct cfparent pspec55 = {
	"gpio", "gpio", DVUNIT_ANY
};
static const struct cfparent pspec56 = {
	"onewirebus", "gpioow", DVUNIT_ANY
};
static const struct cfparent pspec57 = {
	"scsi", NULL, 0
};
static const struct cfparent pspec58 = {
	"scsibus", "scsibus", DVUNIT_ANY
};
static const struct cfparent pspec59 = {
	"jmide_hl", "jmide", DVUNIT_ANY
};
static const struct cfparent pspec60 = {
	"ata", NULL, 0
};
static const struct cfparent pspec61 = {
	"ata_hl", "atabus", DVUNIT_ANY
};
static const struct cfparent pspec62 = {
	"atapi", NULL, 0
};
static const struct cfparent pspec63 = {
	"ataraid", "ataraid", DVUNIT_ANY
};
static const struct cfparent pspec64 = {
	"atapibus", "atapibus", DVUNIT_ANY
};
static const struct cfparent pspec65 = {
	"nvme", "nvme", DVUNIT_ANY
};
static const struct cfparent pspec66 = {
	"fdc", "fdc", DVUNIT_ANY
};
static const struct cfparent pspec67 = {
	"mhzc", "mhzc", DVUNIT_ANY
};
static const struct cfparent pspec68 = {
	"usbus", "xhci", DVUNIT_ANY
};
static const struct cfparent pspec69 = {
	"usbus", "ehci", DVUNIT_ANY
};
static const struct cfparent pspec70 = {
	"usbus", "ohci", DVUNIT_ANY
};
static const struct cfparent pspec71 = {
	"usbus", "uhci", DVUNIT_ANY
};
static const struct cfparent pspec72 = {
	"usbus", "slhci", DVUNIT_ANY
};
static const struct cfparent pspec73 = {
	"usbroothubif", "usb", DVUNIT_ANY
};
static const struct cfparent pspec74 = {
	"usbdevif", "uhub", DVUNIT_ANY
};
static const struct cfparent pspec75 = {
	"usbifif", "uhub", DVUNIT_ANY
};
static const struct cfparent pspec76 = {
	"uhidbus", "uhidev", DVUNIT_ANY
};
static const struct cfparent pspec77 = {
	"wsmousedev", "ums", DVUNIT_ANY
};
static const struct cfparent pspec78 = {
	"wsmousedev", "uts", DVUNIT_ANY
};
static const struct cfparent pspec79 = {
	"wskbddev", "ukbd", DVUNIT_ANY
};
static const struct cfparent pspec80 = {
	"wsmousedev", "uep", DVUNIT_ANY
};
static const struct cfparent pspec81 = {
	"wsemuldisplaydev", "udl", DVUNIT_ANY
};
static const struct cfparent pspec82 = {
	"ucombus", "umodem", DVUNIT_ANY
};
static const struct cfparent pspec83 = {
	"scsi", "umass", DVUNIT_ANY
};
static const struct cfparent pspec84 = {
	"atapi", "umass", DVUNIT_ANY
};
static const struct cfparent pspec85 = {
	"usbifif", NULL, 0
};
static const struct cfparent pspec86 = {
	"audiobus", "uaudio", DVUNIT_ANY
};
static const struct cfparent pspec87 = {
	"irbus", "uirda", DVUNIT_ANY
};
static const struct cfparent pspec88 = {
	"irbus", "stuirda", DVUNIT_ANY
};
static const struct cfparent pspec89 = {
	"irbus", "ustir", DVUNIT_ANY
};
static const struct cfparent pspec90 = {
	"irbus", "udsir", DVUNIT_ANY
};
static const struct cfparent pspec91 = {
	"irbus", "irmce", DVUNIT_ANY
};
static const struct cfparent pspec92 = {
	"ucombus", "uark", DVUNIT_ANY
};
static const struct cfparent pspec93 = {
	"ucombus", "ubsa", DVUNIT_ANY
};
static const struct cfparent pspec94 = {
	"ucombus", "uchcom", DVUNIT_ANY
};
static const struct cfparent pspec95 = {
	"ucombus", "uftdi", DVUNIT_ANY
};
static const struct cfparent pspec96 = {
	"ucombus", "uipaq", DVUNIT_ANY
};
static const struct cfparent pspec97 = {
	"ucombus", "umct", DVUNIT_ANY
};
static const struct cfparent pspec98 = {
	"ucombus", "uplcom", DVUNIT_ANY
};
static const struct cfparent pspec99 = {
	"ucombus", "uslsa", DVUNIT_ANY
};
static const struct cfparent pspec100 = {
	"ucombus", "uvscom", DVUNIT_ANY
};
static const struct cfparent pspec101 = {
	"ucombus", "umcs", DVUNIT_ANY
};
static const struct cfparent pspec102 = {
	"ucombus", "uxrcom", DVUNIT_ANY
};
static const struct cfparent pspec103 = {
	"ucombus", "uvisor", DVUNIT_ANY
};
static const struct cfparent pspec104 = {
	"ucombus", "ukyopon", DVUNIT_ANY
};
static const struct cfparent pspec105 = {
	"scsi", "usscanner", DVUNIT_ANY
};
static const struct cfparent pspec106 = {
	"radiodev", "udsbr", DVUNIT_ANY
};
static const struct cfparent pspec107 = {
	"radiodev", "slurm", DVUNIT_ANY
};
static const struct cfparent pspec108 = {
	"ucombus", "u3g", DVUNIT_ANY
};
static const struct cfparent pspec109 = {
	"ucombus", "ugensa", DVUNIT_ANY
};
static const struct cfparent pspec110 = {
	"irbus", "emdtv", DVUNIT_ANY
};
static const struct cfparent pspec111 = {
	"videobus", NULL, 0
};
static const struct cfparent pspec112 = {
	"dtvbus", NULL, 0
};
static const struct cfparent pspec113 = {
	"btbus", "ubt", DVUNIT_ANY
};
static const struct cfparent pspec114 = {
	"fwbus", "fwohci", DVUNIT_ANY
};
static const struct cfparent pspec115 = {
	"ieee1394if", "ieee1394if", DVUNIT_ANY
};
static const struct cfparent pspec116 = {
	"cmpci", "cmpci", DVUNIT_ANY
};
static const struct cfparent pspec117 = {
	"eso", "eso", DVUNIT_ANY
};
static const struct cfparent pspec118 = {
	"fms", "fms", DVUNIT_ANY
};
static const struct cfparent pspec119 = {
	"sv", "sv", DVUNIT_ANY
};
static const struct cfparent pspec120 = {
	"hdaudiobus", NULL, 0
};
static const struct cfparent pspec121 = {
	"audiobus", NULL, 0
};
static const struct cfparent pspec122 = {
	"audio", "audio", DVUNIT_ANY
};
static const struct cfparent pspec123 = {
	"yds", "yds", DVUNIT_ANY
};
static const struct cfparent pspec124 = {
	"midibus", NULL, 0
};
static const struct cfparent pspec125 = {
	"radiodev", "bktr", DVUNIT_ANY
};
static const struct cfparent pspec126 = {
	"sdmmc", "sdmmc", DVUNIT_ANY
};
static const struct cfparent pspec127 = {
	"btbus", "bcsp", DVUNIT_ANY
};
static const struct cfparent pspec128 = {
	"btbus", "bt3c", DVUNIT_ANY
};
static const struct cfparent pspec129 = {
	"btbus", "btbc", DVUNIT_ANY
};
static const struct cfparent pspec130 = {
	"btbus", "btuart", DVUNIT_ANY
};
static const struct cfparent pspec131 = {
	"btbus", "sbt", DVUNIT_ANY
};
static const struct cfparent pspec132 = {
	"bthub", "bthub", DVUNIT_ANY
};
static const struct cfparent pspec133 = {
	"bthidbus", "bthidev", DVUNIT_ANY
};
static const struct cfparent pspec134 = {
	"wsmousedev", "btms", DVUNIT_ANY
};
static const struct cfparent pspec135 = {
	"wskbddev", "btkbd", DVUNIT_ANY
};
static const struct cfparent pspec136 = {
	"wsmousedev", "btmagic", DVUNIT_ANY
};
static const struct cfparent pspec137 = {
	"sdmmcbus", "sdhc", DVUNIT_ANY
};
static const struct cfparent pspec138 = {
	"sdmmcbus", "rtsx", DVUNIT_ANY
};
static const struct cfparent pspec139 = {
	"virtio", "virtio", DVUNIT_ANY
};

#define NORM FSTATE_NOTFOUND
#define STAR FSTATE_STAR

struct cfdata cfdata[] = {
    /* driver           attachment    unit state      loc   flags  pspec */
/*  0: spkr* at pcppi? */
    { "spkr",		"spkr_pcppi",	 0, STAR,    NULL,      0, &pspec34 },
/*  1: spkr* at audio? */
    { "spkr",		"spkr_audio",	 0, STAR,    NULL,      0, &pspec122 },
/*  2: audio* at uaudio? */
    { "audio",		"audio",	 0, STAR,    NULL,      0, &pspec86 },
/*  3: audio* at audiobus? */
    { "audio",		"audio",	 0, STAR,    NULL,      0, &pspec121 },
/*  4: midi* at midibus? */
    { "midi",		"midi",		 0, STAR,    NULL,      0, &pspec124 },
/*  5: midi* at pcppi? */
    { "midi",		"midi_pcppi",	 0, STAR,    NULL,      0, &pspec34 },
/*  6: hdaudio* at pci? dev -1 function -1 */
    { "hdaudio",	"hdaudio_pci",	 0, STAR, loc+542,      0, &pspec16 },
/*  7: hdafg* at hdaudiobus? nid -1 */
    { "hdafg",		"hdafg",	 0, STAR, loc+738,      0, &pspec120 },
/*  8: video* at videobus? */
    { "video",		"video",	 0, STAR,    NULL,      0, &pspec111 },
/*  9: dtv* at dtvbus? */
    { "dtv",		"dtv",		 0, STAR,    NULL,      0, &pspec112 },
/* 10: iic* at nfsmb? */
    { "iic",		"iic",		 0, STAR,    NULL,      0, &pspec45 },
/* 11: iic* at piixpm? */
    { "iic",		"iic",		 0, STAR,    NULL,      0, &pspec46 },
/* 12: iic* at ichsmb? */
    { "iic",		"iic",		 0, STAR,    NULL,      0, &pspec47 },
/* 13: iic* at ismt? */
    { "iic",		"iic",		 0, STAR,    NULL,      0, &pspec48 },
/* 14: iic* at dwiic? */
    { "iic",		"iic",		 0, STAR,    NULL,      0, &pspec49 },
/* 15: ihidev* at iic? addr -1 */
    { "ihidev",		"ihidev",	 0, STAR, loc+739,      0, &pspec50 },
/* 16: ims* at ihidev? reportid -1 */
    { "ims",		"ims",		 0, STAR, loc+740,      0, &pspec51 },
/* 17: irframe* at uirda? */
    { "irframe",	"irframe",	 0, STAR,    NULL,      0, &pspec87 },
/* 18: irframe* at stuirda? */
    { "irframe",	"irframe",	 0, STAR,    NULL,      0, &pspec88 },
/* 19: irframe* at ustir? */
    { "irframe",	"irframe",	 0, STAR,    NULL,      0, &pspec89 },
/* 20: irframe* at udsir? */
    { "irframe",	"irframe",	 0, STAR,    NULL,      0, &pspec90 },
/* 21: cir* at irmce? */
    { "cir",		"cir",		 0, STAR,    NULL,      0, &pspec91 },
/* 22: cir* at emdtv? */
    { "cir",		"cir",		 0, STAR,    NULL,      0, &pspec110 },
/* 23: lpt0 at isa? port 0x378 size 0 iomem -1 iosiz 0 irq 7 drq -1 drq2 -1 */
    { "lpt",		"lpt_isa",	 0, NORM, loc+  0,      0, &pspec27 },
/* 24: lpt1 at isa? port 0x278 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    { "lpt",		"lpt_isa",	 1, NORM, loc+  7,      0, &pspec27 },
/* 25: lpt* at puc? port -1 */
    { "lpt",		"lpt_puc",	 2, STAR, loc+741,      0, &pspec38 },
/* 26: tpm* at isa? port -1 size 0 iomem 0xfed40000 iosiz 0 irq 7 drq -1 drq2 -1 */
    { "tpm",		"tpm_isa",	 0, STAR, loc+ 14,      0, &pspec27 },
/* 27: ld* at iop? tid -1 */
    { "ld",		"ld_iop",	 0, STAR, loc+742,      0, &pspec53 },
/* 28: ld* at ataraid? vendtype -1 unit -1 */
    { "ld",		"ld_ataraid",	 0, STAR, loc+544,      0, &pspec63 },
/* 29: ld* at nvme? nsid -1 */
    { "ld",		"ld_nvme",	 0, STAR, loc+743,      0, &pspec65 },
/* 30: ld* at sdmmc? */
    { "ld",		"ld_sdmmc",	 0, STAR,    NULL,      0, &pspec126 },
/* 31: ld* at virtio? */
    { "ld",		"ld_virtio",	 0, STAR,    NULL,      0, &pspec139 },
/* 32: cy* at pci? dev -1 function -1 */
    { "cy",		"cy_pci",	 0, STAR, loc+546,      0, &pspec16 },
/* 33: sm* at mhzc? */
    { "sm",		"sm_mhzc",	 0, STAR,    NULL,      0, &pspec67 },
/* 34: com0 at isa? port 0x3f8 size 0 iomem -1 iosiz 0 irq 4 drq -1 drq2 -1 */
    { "com",		"com_isa",	 0, NORM, loc+ 21,      0, &pspec27 },
/* 35: com1 at isa? port 0x2f8 size 0 iomem -1 iosiz 0 irq 3 drq -1 drq2 -1 */
    { "com",		"com_isa",	 1, NORM, loc+ 28,      0, &pspec27 },
/* 36: com* at puc? port -1 */
    { "com",		"com_puc",	 2, STAR, loc+744,      0, &pspec38 },
/* 37: com* at pcmcia? function -1 */
    { "com",		"com_pcmcia",	 2, STAR, loc+745,      0, &pspec39 },
/* 38: com* at pcmcom? slave -1 */
    { "com",		"com_pcmcom",	 2, STAR, loc+746,      0, &pspec40 },
/* 39: com* at cardbus? function -1 */
    { "com",		"com_cardbus",	 2, STAR, loc+747,      0, &pspec41 },
/* 40: com* at mhzc? */
    { "com",		"com_mhzc",	 2, STAR,    NULL,      0, &pspec67 },
/* 41: pckbc0 at isa? port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    { "pckbc",		"pckbc_isa",	 0, NORM, loc+ 35,      0, &pspec27 },
/* 42: pckbc* at acpi? */
    { "pckbc",		"pckbc_acpi",	 1, STAR,    NULL,      0, &pspec5 },
/* 43: attimer0 at isa? port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    { "attimer",	"attimer_isa",	 0, NORM, loc+ 42,      0, &pspec27 },
/* 44: attimer* at acpi? */
    { "attimer",	"attimer_acpi",	 1, STAR,    NULL,      0, &pspec5 },
/* 45: opl* at cmpci? */
    { "opl",		"opl_cmpci",	 0, STAR,    NULL,    0x1, &pspec116 },
/* 46: opl* at eso? */
    { "opl",		"opl_eso",	 0, STAR,    NULL,      0, &pspec117 },
/* 47: opl* at fms? */
    { "opl",		"opl_fms",	 0, STAR,    NULL,      0, &pspec118 },
/* 48: opl* at sv? */
    { "opl",		"opl_sv",	 0, STAR,    NULL,      0, &pspec119 },
/* 49: mpu* at acpi? */
    { "mpu",		"mpu_acpi",	 0, STAR,    NULL,      0, &pspec5 },
/* 50: mpu* at cmpci? */
    { "mpu",		"mpu_cmpci",	 0, STAR,    NULL,      0, &pspec116 },
/* 51: mpu* at eso? */
    { "mpu",		"mpu_eso",	 0, STAR,    NULL,      0, &pspec117 },
/* 52: mpu* at yds? */
    { "mpu",		"mpu_yds",	 0, STAR,    NULL,      0, &pspec123 },
/* 53: virtio* at pci? dev -1 function -1 */
    { "virtio",		"virtio_pci",	 0, STAR, loc+548,      0, &pspec16 },
/* 54: viomb* at virtio? */
    { "viomb",		"viomb",	 0, STAR,    NULL,      0, &pspec139 },
/* 55: vioif* at virtio? */
    { "vioif",		"vioif",	 0, STAR,    NULL,      0, &pspec139 },
/* 56: viornd* at virtio? */
    { "viornd",		"viornd",	 0, STAR,    NULL,      0, &pspec139 },
/* 57: vioscsi* at virtio? */
    { "vioscsi",	"vioscsi",	 0, STAR,    NULL,      0, &pspec139 },
/* 58: wdc0 at isa? port 0x1f0 size 0 iomem -1 iosiz 0 irq 14 drq -1 drq2 -1 */
    { "wdc",		"wdc_isa",	 0, NORM, loc+ 49,      0, &pspec27 },
/* 59: wdc1 at isa? port 0x170 size 0 iomem -1 iosiz 0 irq 15 drq -1 drq2 -1 */
    { "wdc",		"wdc_isa",	 1, NORM, loc+ 56,      0, &pspec27 },
/* 60: wdc* at pcmcia? function -1 */
    { "wdc",		"wdc_pcmcia",	 2, STAR, loc+748,      0, &pspec39 },
/* 61: atabus* at ata? channel -1 */
    { "atabus",		"atabus",	 0, STAR, loc+749,      0, &pspec60 },
/* 62: njata* at cardbus? function -1 */
    { "njata",		"njata_cardbus",	 0, STAR, loc+750,    0x1, &pspec41 },
/* 63: ahcisata* at pci? dev -1 function -1 */
    { "ahcisata",	"ahcisata_pci",	 0, STAR, loc+550,      0, &pspec16 },
/* 64: ahcisata* at jmide? */
    { "ahcisata",	"jmahci",	 0, STAR,    NULL,      0, &pspec59 },
/* 65: siisata* at pci? dev -1 function -1 */
    { "siisata",	"siisata_pci",	 0, STAR, loc+552,      0, &pspec16 },
/* 66: siisata* at cardbus? function -1 */
    { "siisata",	"siisata_cardbus",	 0, STAR, loc+751,      0, &pspec41 },
/* 67: mvsata* at pci? dev -1 function -1 */
    { "mvsata",		"mvsata_pci",	 0, STAR, loc+554,      0, &pspec16 },
/* 68: dwiic* at pci? dev -1 function -1 */
    { "dwiic",		"pcidwiic",	 0, STAR, loc+556,      0, &pspec16 },
/* 69: hpet* at acpihpetbus? */
    { "hpet",		"hpet_acpi_tab",	 0, STAR,    NULL,      0, &pspec11 },
/* 70: hpet* at acpinodebus? */
    { "hpet",		"hpet_acpi_dev",	 0, STAR,    NULL,      0, &pspec12 },
/* 71: hpet* at amdpcib? */
    { "hpet",		"amdpcib_hpet",	 0, STAR,    NULL,      0, &pspec17 },
/* 72: vga* at pci? dev -1 function -1 */
    { "vga",		"vga_pci",	 0, STAR, loc+558,      0, &pspec16 },
/* 73: wsdisplay* at vga? console -1 kbdmux 1 */
    { "wsdisplay",	"wsdisplay_emul",	 0, STAR, loc+560,      0, &pspec29 },
/* 74: wsdisplay* at wsemuldisplaydev? console -1 kbdmux 1 */
    { "wsdisplay",	"wsdisplay_emul",	 0, STAR, loc+562,      0, &pspec30 },
/* 75: wsdisplay* at udl? console -1 kbdmux 1 */
    { "wsdisplay",	"wsdisplay_emul",	 0, STAR, loc+564,      0, &pspec81 },
/* 76: wskbd* at pckbd? console -1 mux 1 */
    { "wskbd",		"wskbd",	 0, STAR, loc+566,      0, &pspec31 },
/* 77: wskbd* at ukbd? console -1 mux 1 */
    { "wskbd",		"wskbd",	 0, STAR, loc+568,      0, &pspec79 },
/* 78: wskbd* at btkbd? console -1 mux 1 */
    { "wskbd",		"wskbd",	 0, STAR, loc+570,      0, &pspec135 },
/* 79: wsmouse* at pms? mux 0 */
    { "wsmouse",	"wsmouse",	 0, STAR, loc+752,      0, &pspec32 },
/* 80: wsmouse* at wsmousedev? mux 0 */
    { "wsmouse",	"wsmouse",	 0, STAR, loc+753,      0, &pspec33 },
/* 81: wsmouse* at ims? mux 0 */
    { "wsmouse",	"wsmouse",	 0, STAR, loc+754,      0, &pspec52 },
/* 82: wsmouse* at ums? mux 0 */
    { "wsmouse",	"wsmouse",	 0, STAR, loc+755,      0, &pspec77 },
/* 83: wsmouse* at uts? mux 0 */
    { "wsmouse",	"wsmouse",	 0, STAR, loc+756,      0, &pspec78 },
/* 84: wsmouse* at uep? mux 0 */
    { "wsmouse",	"wsmouse",	 0, STAR, loc+757,      0, &pspec80 },
/* 85: wsmouse* at btms? mux 0 */
    { "wsmouse",	"wsmouse",	 0, STAR, loc+758,      0, &pspec134 },
/* 86: wsmouse* at btmagic? mux 0 */
    { "wsmouse",	"wsmouse",	 0, STAR, loc+759,      0, &pspec136 },
/* 87: genfb* at pci? dev -1 function -1 */
    { "genfb",		"genfb_pci",	 0, STAR, loc+572,      0, &pspec16 },
/* 88: uhci* at pci? dev -1 function -1 */
    { "uhci",		"uhci_pci",	 0, STAR, loc+574,      0, &pspec16 },
/* 89: uhci* at cardbus? function -1 */
    { "uhci",		"uhci_cardbus",	 0, STAR, loc+760,      0, &pspec41 },
/* 90: ohci* at pci? dev -1 function -1 */
    { "ohci",		"ohci_pci",	 0, STAR, loc+576,      0, &pspec16 },
/* 91: ohci* at cardbus? function -1 */
    { "ohci",		"ohci_cardbus",	 0, STAR, loc+761,      0, &pspec41 },
/* 92: ehci* at pci? dev -1 function -1 */
    { "ehci",		"ehci_pci",	 0, STAR, loc+578,      0, &pspec16 },
/* 93: ehci* at cardbus? function -1 */
    { "ehci",		"ehci_cardbus",	 0, STAR, loc+762,      0, &pspec41 },
/* 94: xhci* at pci? dev -1 function -1 */
    { "xhci",		"xhci_pci",	 0, STAR, loc+580,      0, &pspec16 },
/* 95: slhci* at pcmcia? function -1 */
    { "slhci",		"slhci_pcmcia",	 0, STAR, loc+763,      0, &pspec39 },
/* 96: sdhc* at acpi? */
    { "sdhc",		"sdhc_acpi",	 0, STAR,    NULL,      0, &pspec5 },
/* 97: sdhc* at pci? dev -1 function -1 */
    { "sdhc",		"sdhc_pci",	 0, STAR, loc+582,      0, &pspec16 },
/* 98: sdhc* at cardbus? function -1 */
    { "sdhc",		"sdhc_cardbus",	 0, STAR, loc+764,      0, &pspec41 },
/* 99: rtsx* at pci? dev -1 function -1 */
    { "rtsx",		"rtsx_pci",	 0, STAR, loc+584,      0, &pspec16 },
/*100: radio* at udsbr? */
    { "radio",		"radio",	 0, STAR,    NULL,      0, &pspec106 },
/*101: radio* at slurm? */
    { "radio",		"radio",	 0, STAR,    NULL,      0, &pspec107 },
/*102: radio* at bktr? */
    { "radio",		"radio",	 0, STAR,    NULL,      0, &pspec125 },
/*103: fwohci* at pci? dev -1 function -1 */
    { "fwohci",		"fwohci_pci",	 0, STAR, loc+586,      0, &pspec16 },
/*104: fwohci* at cardbus? function -1 */
    { "fwohci",		"fwohci_cardbus",	 0, STAR, loc+765,      0, &pspec41 },
/*105: nvme* at pci? dev -1 function -1 */
    { "nvme",		"nvme_pci",	 0, STAR, loc+588,      0, &pspec16 },
/*106: bwfm* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "bwfm",		"bwfm_usb",	 0, STAR, loc+ 77,      0, &pspec74 },
/*107: qemufwcfg* at acpi? */
    { "qemufwcfg",	"qemufwcfg_acpi",	 0, STAR,    NULL,      0, &pspec5 },
/*108: ipmi0 at mainbus? */
    { "ipmi",		"ipmi",		 0, NORM,    NULL,      0, &pspec4 },
/*109: ipmi0 at ipmi_acpi? */
    { "ipmi",		"ipmi",		 0, NORM,    NULL,      0, &pspec6 },
/*110: joy* at acpi? */
    { "joy",		"joy_acpi",	 0, STAR,    NULL,      0, &pspec5 },
/*111: gpio* at gpiobus? */
    { "gpio",		"gpio",		 0, STAR,    NULL,      0, &pspec54 },
/*112: gpioow* at gpio? offset -1 mask 0 flag 0 */
    { "gpioow",		"gpioow",	 0, STAR, loc+539,      0, &pspec55 },
/*113: onewire* at gpioow? */
    { "onewire",	"onewire",	 0, STAR,    NULL,      0, &pspec56 },
/*114: athn* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "athn",		"athn_usb",	 0, STAR, loc+ 83,      0, &pspec74 },
/*115: cpu* at mainbus? apid -1 */
    { "cpu",		"cpu",		 0, STAR, loc+766,      0, &pspec0 },
/*116: acpicpu* at cpu? */
    { "acpicpu",	"acpicpu",	 0, STAR,    NULL,      0, &pspec2 },
/*117: coretemp* at cpu? */
    { "coretemp",	"coretemp",	 0, STAR,    NULL,      0, &pspec2 },
/*118: est0 at cpu0 */
    { "est",		"est",		 0, NORM,    NULL,      0, &pspec3 },
/*119: powernow0 at cpu0 */
    { "powernow",	"powernow",	 0, NORM,    NULL,      0, &pspec3 },
/*120: ioapic* at mainbus? apid -1 */
    { "ioapic",		"ioapic",	 0, STAR, loc+767,      0, &pspec1 },
/*121: scsibus* at scsi? channel -1 */
    { "scsibus",	"scsibus",	 0, STAR, loc+768,      0, &pspec57 },
/*122: scsibus* at umass? channel -1 */
    { "scsibus",	"scsibus",	 0, STAR, loc+769,      0, &pspec83 },
/*123: scsibus* at usscanner? channel -1 */
    { "scsibus",	"scsibus",	 0, STAR, loc+770,      0, &pspec105 },
/*124: atapibus* at atapi? */
    { "atapibus",	"atapibus",	 0, STAR,    NULL,      0, &pspec62 },
/*125: atapibus* at umass? */
    { "atapibus",	"atapibus",	 0, STAR,    NULL,      0, &pspec84 },
/*126: cd* at scsibus? target -1 lun -1 */
    { "cd",		"cd",		 0, STAR, loc+590,      0, &pspec58 },
/*127: cd* at atapibus? drive -1 */
    { "cd",		"cd",		 0, STAR, loc+771,      0, &pspec64 },
/*128: ch* at scsibus? target -1 lun -1 */
    { "ch",		"ch",		 0, STAR, loc+592,      0, &pspec58 },
/*129: sd* at scsibus? target -1 lun -1 */
    { "sd",		"sd",		 0, STAR, loc+594,      0, &pspec58 },
/*130: sd* at atapibus? drive -1 */
    { "sd",		"sd",		 0, STAR, loc+772,      0, &pspec64 },
/*131: st* at scsibus? target -1 lun -1 */
    { "st",		"st_scsibus",	 0, STAR, loc+596,      0, &pspec58 },
/*132: st* at atapibus? drive -1 */
    { "st",		"st_atapibus",	 0, STAR, loc+773,      0, &pspec64 },
/*133: ses* at scsibus? target -1 lun -1 */
    { "ses",		"ses",		 0, STAR, loc+598,      0, &pspec58 },
/*134: ss* at scsibus? target -1 lun -1 */
    { "ss",		"ss",		 0, STAR, loc+600,      0, &pspec58 },
/*135: uk* at scsibus? target -1 lun -1 */
    { "uk",		"uk",		 0, STAR, loc+602,      0, &pspec58 },
/*136: uk* at atapibus? drive -1 */
    { "uk",		"uk",		 0, STAR, loc+774,      0, &pspec64 },
/*137: wd* at atabus? drive -1 */
    { "wd",		"wd",		 0, STAR, loc+775,      0, &pspec61 },
/*138: iop* at pci? dev -1 function -1 */
    { "iop",		"iop_pci",	 0, STAR, loc+604,      0, &pspec16 },
/*139: iopsp* at iop? tid -1 */
    { "iopsp",		"iopsp",	 0, STAR, loc+776,      0, &pspec53 },
/*140: mainbus0 at root */
    { "mainbus",	"mainbus",	 0, NORM,    NULL,      0, NULL },
/*141: pci* at mainbus? bus -1 */
    { "pci",		"pci",		 0, STAR, loc+777,      0, &pspec13 },
/*142: pci* at pchb? bus -1 */
    { "pci",		"pci",		 0, STAR, loc+778,      0, &pspec14 },
/*143: pci* at ppb? bus -1 */
    { "pci",		"pci",		 0, STAR, loc+779,      0, &pspec15 },
/*144: pciide* at pci? dev -1 function -1 */
    { "pciide",		"pciide",	 0, STAR, loc+606,      0, &pspec16 },
/*145: acardide* at pci? dev -1 function -1 */
    { "acardide",	"acardide",	 0, STAR, loc+608,      0, &pspec16 },
/*146: aceride* at pci? dev -1 function -1 */
    { "aceride",	"aceride",	 0, STAR, loc+610,      0, &pspec16 },
/*147: artsata* at pci? dev -1 function -1 */
    { "artsata",	"artsata",	 0, STAR, loc+612,      0, &pspec16 },
/*148: cmdide* at pci? dev -1 function -1 */
    { "cmdide",		"cmdide",	 0, STAR, loc+614,      0, &pspec16 },
/*149: cypide* at pci? dev -1 function -1 */
    { "cypide",		"cypide",	 0, STAR, loc+616,      0, &pspec16 },
/*150: hptide* at pci? dev -1 function -1 */
    { "hptide",		"hptide",	 0, STAR, loc+618,      0, &pspec16 },
/*151: iteide* at pci? dev -1 function -1 */
    { "iteide",		"iteide",	 0, STAR, loc+620,      0, &pspec16 },
/*152: jmide* at pci? dev -1 function -1 */
    { "jmide",		"jmide",	 0, STAR, loc+622,      0, &pspec16 },
/*153: optiide* at pci? dev -1 function -1 */
    { "optiide",	"optiide",	 0, STAR, loc+624,      0, &pspec16 },
/*154: piixide* at pci? dev -1 function -1 */
    { "piixide",	"piixide",	 0, STAR, loc+626,      0, &pspec16 },
/*155: pdcsata* at pci? dev -1 function -1 */
    { "pdcsata",	"pdcsata",	 0, STAR, loc+628,      0, &pspec16 },
/*156: pdcide* at pci? dev -1 function -1 */
    { "pdcide",		"pdcide",	 0, STAR, loc+630,      0, &pspec16 },
/*157: svwsata* at pci? dev -1 function -1 */
    { "svwsata",	"svwsata",	 0, STAR, loc+632,      0, &pspec16 },
/*158: satalink* at pci? dev -1 function -1 */
    { "satalink",	"satalink",	 0, STAR, loc+634,      0, &pspec16 },
/*159: siside* at pci? dev -1 function -1 */
    { "siside",		"siside",	 0, STAR, loc+636,      0, &pspec16 },
/*160: slide* at pci? dev -1 function -1 */
    { "slide",		"slide",	 0, STAR, loc+638,      0, &pspec16 },
/*161: viaide* at pci? dev -1 function -1 */
    { "viaide",		"viaide",	 0, STAR, loc+640,      0, &pspec16 },
/*162: ixpide* at pci? dev -1 function -1 */
    { "ixpide",		"ixpide",	 0, STAR, loc+642,      0, &pspec16 },
/*163: toshide* at pci? dev -1 function -1 */
    { "toshide",	"toshide",	 0, STAR, loc+644,      0, &pspec16 },
/*164: ppb* at pci? dev -1 function -1 */
    { "ppb",		"ppb",		 0, STAR, loc+646,      0, &pspec16 },
/*165: cz* at pci? dev -1 function -1 */
    { "cz",		"cz",		 0, STAR, loc+648,      0, &pspec16 },
/*166: bktr* at pci? dev -1 function -1 */
    { "bktr",		"bktr",		 0, STAR, loc+650,      0, &pspec16 },
/*167: clcs* at pci? dev -1 function -1 */
    { "clcs",		"clcs",		 0, STAR, loc+652,      0, &pspec16 },
/*168: clct* at pci? dev -1 function -1 */
    { "clct",		"clct",		 0, STAR, loc+654,      0, &pspec16 },
/*169: fms* at pci? dev -1 function -1 */
    { "fms",		"fms",		 0, STAR, loc+656,      0, &pspec16 },
/*170: eap* at pci? dev -1 function -1 */
    { "eap",		"eap",		 0, STAR, loc+658,      0, &pspec16 },
/*171: auacer* at pci? dev -1 function -1 */
    { "auacer",		"auacer",	 0, STAR, loc+660,      0, &pspec16 },
/*172: auich* at pci? dev -1 function -1 */
    { "auich",		"auich",	 0, STAR, loc+662,      0, &pspec16 },
/*173: auvia* at pci? dev -1 function -1 */
    { "auvia",		"auvia",	 0, STAR, loc+664,      0, &pspec16 },
/*174: auixp* at pci? dev -1 function -1 */
    { "auixp",		"auixp",	 0, STAR, loc+666,      0, &pspec16 },
/*175: neo* at pci? dev -1 function -1 */
    { "neo",		"neo",		 0, STAR, loc+668,      0, &pspec16 },
/*176: esa* at pci? dev -1 function -1 */
    { "esa",		"esa",		 0, STAR, loc+670,      0, &pspec16 },
/*177: eso* at pci? dev -1 function -1 */
    { "eso",		"eso",		 0, STAR, loc+672,      0, &pspec16 },
/*178: esm* at pci? dev -1 function -1 */
    { "esm",		"esm",		 0, STAR, loc+674,      0, &pspec16 },
/*179: sv* at pci? dev -1 function -1 */
    { "sv",		"sv",		 0, STAR, loc+676,      0, &pspec16 },
/*180: cmpci* at pci? dev -1 function -1 */
    { "cmpci",		"cmpci",	 0, STAR, loc+678,      0, &pspec16 },
/*181: yds* at pci? dev -1 function -1 */
    { "yds",		"yds",		 0, STAR, loc+680,      0, &pspec16 },
/*182: emuxki* at pci? dev -1 function -1 */
    { "emuxki",		"emuxki",	 0, STAR, loc+682,      0, &pspec16 },
/*183: autri* at pci? dev -1 function -1 */
    { "autri",		"autri",	 0, STAR, loc+684,      0, &pspec16 },
/*184: puc* at pci? dev -1 function -1 */
    { "puc",		"puc",		 0, STAR, loc+686,      0, &pspec16 },
/*185: cbb* at pci? dev -1 function -1 */
    { "cbb",		"cbb_pci",	 0, STAR, loc+688,      0, &pspec16 },
/*186: piixpm* at pci? dev -1 function -1 */
    { "piixpm",		"piixpm",	 0, STAR, loc+690,      0, &pspec16 },
/*187: amdpm* at pci? dev -1 function -1 */
    { "amdpm",		"amdpm",	 0, STAR, loc+692,      0, &pspec16 },
/*188: hifn* at pci? dev -1 function -1 */
    { "hifn",		"hifn",		 0, STAR, loc+694,      0, &pspec16 },
/*189: ubsec* at pci? dev -1 function -1 */
    { "ubsec",		"ubsec",	 0, STAR, loc+696,      0, &pspec16 },
/*190: weasel* at pci? dev -1 function -1 */
    { "weasel",		"weasel_pci",	 0, STAR, loc+698,      0, &pspec16 },
/*191: nfsmbc* at pci? dev -1 function -1 */
    { "nfsmbc",		"nfsmbc",	 0, STAR, loc+700,      0, &pspec16 },
/*192: nfsmb* at nfsmbc? */
    { "nfsmb",		"nfsmb",	 0, STAR,    NULL,      0, &pspec44 },
/*193: ichsmb* at pci? dev -1 function -1 */
    { "ichsmb",		"ichsmb",	 0, STAR, loc+702,      0, &pspec16 },
/*194: cxdtv* at pci? dev -1 function -1 */
    { "cxdtv",		"cxdtv",	 0, STAR, loc+704,      0, &pspec16 },
/*195: coram* at pci? dev -1 function -1 */
    { "coram",		"coram",	 0, STAR, loc+706,      0, &pspec16 },
/*196: pwdog* at pci? dev -1 function -1 */
    { "pwdog",		"pwdog",	 0, STAR, loc+708,      0, &pspec16 },
/*197: i915drmkms* at pci? dev -1 function -1 */
    { "i915drmkms",	"i915drmkms",	 0, STAR, loc+710,      0, &pspec16 },
/*198: intelfb* at intelfbbus? */
    { "intelfb",	"intelfb",	 0, STAR,    NULL,      0, &pspec35 },
/*199: radeon* at pci? dev -1 function -1 */
    { "radeon",		"radeon",	 0, STAR, loc+712,      0, &pspec16 },
/*200: radeondrmkmsfb* at radeonfbbus? */
    { "radeondrmkmsfb",	"radeondrmkmsfb",	 0, STAR,    NULL,      0, &pspec36 },
/*201: nouveau* at pci? dev -1 function -1 */
    { "nouveau",	"nouveau_pci",	 0, STAR, loc+714,      0, &pspec16 },
/*202: nouveaufb* at nouveaufbbus? */
    { "nouveaufb",	"nouveaufb",	 0, STAR,    NULL,      0, &pspec37 },
/*203: ismt* at pci? dev -1 function -1 */
    { "ismt",		"ismt",		 0, STAR, loc+716,      0, &pspec16 },
/*204: agp* at pchb? */
    { "agp",		"agp",		 0, STAR,    NULL,      0, &pspec20 },
/*205: aapic* at pci? dev -1 function -1 */
    { "aapic",		"aapic",	 0, STAR, loc+718,      0, &pspec16 },
/*206: pchb* at pci? dev -1 function -1 */
    { "pchb",		"pchb",		 0, STAR, loc+720,      0, &pspec16 },
/*207: pcib* at pci? dev -1 function -1 */
    { "pcib",		"pcib",		 0, STAR, loc+722,      0, &pspec16 },
/*208: amdpcib* at pci? dev -1 function -1 */
    { "amdpcib",	"amdpcib",	 0, STAR, loc+724,      0, &pspec16 },
/*209: amdnb_misc* at pci? dev -1 function -1 */
    { "amdnb_misc",	"amdnb_misc",	 0, STAR, loc+726,      0, &pspec16 },
/*210: amdsmn* at pci? dev -1 function -1 */
    { "amdsmn",		"amdsmn",	 0, STAR, loc+728,      0, &pspec16 },
/*211: amdzentemp* at amdsmnbus? */
    { "amdzentemp",	"amdzentemp",	 0, STAR,    NULL,      0, &pspec43 },
/*212: amdtemp* at amdnb_misc? */
    { "amdtemp",	"amdtemp",	 0, STAR,    NULL,      0, &pspec42 },
/*213: ichlpcib* at pci? dev -1 function -1 */
    { "ichlpcib",	"ichlpcib",	 0, STAR, loc+730,      0, &pspec16 },
/*214: tco* at ichlpcib? */
    { "tco",		"tco",		 0, STAR,    NULL,      0, &pspec19 },
/*215: fwhrng* at ichlpcib? */
    { "fwhrng",		"fwhrng",	 0, STAR,    NULL,      0, &pspec18 },
/*216: isa0 at mainbus? */
    { "isa",		"isa",		 0, NORM,    NULL,      0, &pspec21 },
/*217: isa0 at pcib? */
    { "isa",		"isa",		 0, NORM,    NULL,      0, &pspec22 },
/*218: isa0 at ichlpcib? */
    { "isa",		"isa",		 0, NORM,    NULL,      0, &pspec23 },
/*219: pcppi0 at isa? port -1 size 0 iomem -1 iosiz 0 irq -1 drq -1 drq2 -1 */
    { "pcppi",		"pcppi",	 0, NORM, loc+ 63,      0, &pspec27 },
/*220: pcppi* at acpi? */
    { "pcppi",		"pcppi_acpi",	 1, STAR,    NULL,      0, &pspec5 },
/*221: pckbd* at pckbc? slot -1 */
    { "pckbd",		"pckbd",	 0, STAR, loc+780,      0, &pspec28 },
/*222: pms* at pckbc? slot -1 */
    { "pms",		"pms",		 0, STAR, loc+781,      0, &pspec28 },
/*223: sysbeep0 at pcppi? */
    { "sysbeep",	"sysbeep",	 0, NORM,    NULL,      0, &pspec34 },
/*224: fdc0 at isa? port 0x3f0 size 0 iomem -1 iosiz 0 irq 6 drq 2 drq2 -1 */
    { "fdc",		"fdc_isa",	 0, NORM, loc+ 70,      0, &pspec27 },
/*225: fd* at fdc? drive -1 */
    { "fd",		"fd",		 0, STAR, loc+782,      0, &pspec66 },
/*226: cardslot* at cbb? */
    { "cardslot",	"cardslot",	 0, STAR,    NULL,      0, &pspec24 },
/*227: cardbus* at cardslot? */
    { "cardbus",	"cardbus",	 0, STAR,    NULL,      0, &pspec25 },
/*228: pcmcia* at cardslot? controller -1 socket -1 */
    { "pcmcia",		"pcmcia",	 0, STAR, loc+732,      0, &pspec26 },
/*229: pcmcom* at pcmcia? function -1 */
    { "pcmcom",		"pcmcom",	 0, STAR, loc+783,      0, &pspec39 },
/*230: mhzc* at pcmcia? function -1 */
    { "mhzc",		"mhzc",		 0, STAR, loc+784,      0, &pspec39 },
/*231: bt3c* at pcmcia? function -1 */
    { "bt3c",		"bt3c",		 0, STAR, loc+785,      0, &pspec39 },
/*232: btbc* at pcmcia? function -1 */
    { "btbc",		"btbc",		 0, STAR, loc+786,      0, &pspec39 },
/*233: usb* at xhci? */
    { "usb",		"usb",		 0, STAR,    NULL,      0, &pspec68 },
/*234: usb* at ehci? */
    { "usb",		"usb",		 0, STAR,    NULL,      0, &pspec69 },
/*235: usb* at ohci? */
    { "usb",		"usb",		 0, STAR,    NULL,      0, &pspec70 },
/*236: usb* at uhci? */
    { "usb",		"usb",		 0, STAR,    NULL,      0, &pspec71 },
/*237: usb* at slhci? */
    { "usb",		"usb",		 0, STAR,    NULL,      0, &pspec72 },
/*238: uhub* at usb? */
    { "uhub",		"uroothub",	 0, STAR,    NULL,      0, &pspec73 },
/*239: uhub* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "uhub",		"uhub",		 0, STAR, loc+ 89,      0, &pspec74 },
/*240: uaudio* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "uaudio",		"uaudio",	 0, STAR, loc+ 95,      0, &pspec75 },
/*241: uaudio* at usbifif? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "uaudio",		"uaudio",	 0, STAR, loc+101,      0, &pspec85 },
/*242: umidi* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "umidi",		"umidi",	 0, STAR, loc+107,      0, &pspec75 },
/*243: ucom* at umodem? portno -1 */
    { "ucom",		"ucom",		 0, STAR, loc+787,      0, &pspec82 },
/*244: ucom* at uark? portno -1 */
    { "ucom",		"ucom",		 0, STAR, loc+788,      0, &pspec92 },
/*245: ucom* at ubsa? portno -1 */
    { "ucom",		"ucom",		 0, STAR, loc+789,      0, &pspec93 },
/*246: ucom* at uchcom? portno -1 */
    { "ucom",		"ucom",		 0, STAR, loc+790,      0, &pspec94 },
/*247: ucom* at uftdi? portno -1 */
    { "ucom",		"ucom",		 0, STAR, loc+791,      0, &pspec95 },
/*248: ucom* at uipaq? portno -1 */
    { "ucom",		"ucom",		 0, STAR, loc+792,      0, &pspec96 },
/*249: ucom* at umct? portno -1 */
    { "ucom",		"ucom",		 0, STAR, loc+793,      0, &pspec97 },
/*250: ucom* at uplcom? portno -1 */
    { "ucom",		"ucom",		 0, STAR, loc+794,      0, &pspec98 },
/*251: ucom* at uslsa? portno -1 */
    { "ucom",		"ucom",		 0, STAR, loc+795,      0, &pspec99 },
/*252: ucom* at uvscom? portno -1 */
    { "ucom",		"ucom",		 0, STAR, loc+796,      0, &pspec100 },
/*253: ucom* at umcs? portno -1 */
    { "ucom",		"ucom",		 0, STAR, loc+797,      0, &pspec101 },
/*254: ucom* at uxrcom? portno -1 */
    { "ucom",		"ucom",		 0, STAR, loc+798,      0, &pspec102 },
/*255: ucom* at uvisor? portno -1 */
    { "ucom",		"ucom",		 0, STAR, loc+799,      0, &pspec103 },
/*256: ucom* at ukyopon? portno -1 */
    { "ucom",		"ucom",		 0, STAR, loc+800,      0, &pspec104 },
/*257: ucom* at u3g? portno -1 */
    { "ucom",		"ucom",		 0, STAR, loc+801,      0, &pspec108 },
/*258: ucom* at ugensa? portno -1 */
    { "ucom",		"ucom",		 0, STAR, loc+802,      0, &pspec109 },
/*259: ugen* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "ugen",		"ugen",		 0, STAR, loc+113,      0, &pspec74 },
/*260: ugenif* at uhub? port -1 configuration 1 interface 1 vendor 0x1050 product 0x114 release -1 */
    { "ugenif",		"ugenif",	 0, STAR, loc+119,      0, &pspec75 },
/*261: ugenif* at uhub? port -1 configuration 1 interface 1 vendor 0x1050 product 0x115 release -1 */
    { "ugenif",		"ugenif",	 0, STAR, loc+125,      0, &pspec75 },
/*262: ugenif* at uhub? port -1 configuration 1 interface 2 vendor 0x1050 product 0x116 release -1 */
    { "ugenif",		"ugenif",	 0, STAR, loc+131,      0, &pspec75 },
/*263: ugenif* at uhub? port -1 configuration 1 interface 1 vendor 0x1050 product 0x405 release -1 */
    { "ugenif",		"ugenif",	 0, STAR, loc+137,      0, &pspec75 },
/*264: ugenif* at uhub? port -1 configuration 1 interface 1 vendor 0x1050 product 0x406 release -1 */
    { "ugenif",		"ugenif",	 0, STAR, loc+143,      0, &pspec75 },
/*265: ugenif* at uhub? port -1 configuration 1 interface 2 vendor 0x1050 product 0x407 release -1 */
    { "ugenif",		"ugenif",	 0, STAR, loc+149,      0, &pspec75 },
/*266: uhidev* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "uhidev",		"uhidev",	 0, STAR, loc+155,      0, &pspec75 },
/*267: uhid* at uhidev? reportid -1 */
    { "uhid",		"uhid",		 0, STAR, loc+803,      0, &pspec76 },
/*268: ukbd* at uhidev? reportid -1 */
    { "ukbd",		"ukbd",		 0, STAR, loc+804,      0, &pspec76 },
/*269: ums* at uhidev? reportid -1 */
    { "ums",		"ums",		 0, STAR, loc+805,      0, &pspec76 },
/*270: uts* at uhidev? reportid -1 */
    { "uts",		"uts",		 0, STAR, loc+806,      0, &pspec76 },
/*271: uep* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "uep",		"uep",		 0, STAR, loc+161,      0, &pspec74 },
/*272: ucycom* at uhidev? reportid -1 */
    { "ucycom",		"ucycom",	 0, STAR, loc+807,      0, &pspec76 },
/*273: ulpt* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "ulpt",		"ulpt",		 0, STAR, loc+167,      0, &pspec75 },
/*274: umass* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "umass",		"umass",	 0, STAR, loc+173,      0, &pspec75 },
/*275: uirda* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "uirda",		"uirda",	 0, STAR, loc+179,      0, &pspec75 },
/*276: stuirda* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "stuirda",	"stuirda",	 0, STAR, loc+185,      0, &pspec75 },
/*277: ustir* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "ustir",		"ustir",	 0, STAR, loc+191,      0, &pspec74 },
/*278: irmce* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "irmce",		"irmce",	 0, STAR, loc+197,      0, &pspec75 },
/*279: ubt* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "ubt",		"ubt",		 0, STAR, loc+203,      0, &pspec74 },
/*280: aubtfwl* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "aubtfwl",	"aubtfwl",	 0, STAR, loc+209,      0, &pspec74 },
/*281: pseye* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "pseye",		"pseye",	 0, STAR, loc+215,      0, &pspec75 },
/*282: uvideo* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "uvideo",		"uvideo",	 0, STAR, loc+221,      0, &pspec75 },
/*283: auvitek* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "auvitek",	"auvitek",	 0, STAR, loc+227,      0, &pspec74 },
/*284: emdtv* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "emdtv",		"emdtv",	 0, STAR, loc+233,      0, &pspec74 },
/*285: umodeswitch* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "umodeswitch",	"umodeswitch",	 0, STAR, loc+239,      0, &pspec74 },
/*286: urio* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "urio",		"urio",		 0, STAR, loc+245,      0, &pspec74 },
/*287: uipad* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "uipad",		"uipad",	 0, STAR, loc+251,      0, &pspec74 },
/*288: uberry* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "uberry",		"uberry",	 0, STAR, loc+257,      0, &pspec74 },
/*289: uvisor* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "uvisor",		"uvisor",	 0, STAR, loc+263,      0, &pspec74 },
/*290: ugensa* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "ugensa",		"ugensa",	 0, STAR, loc+269,      0, &pspec74 },
/*291: u3g* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "u3g",		"u3g",		 0, STAR, loc+275,      0, &pspec75 },
/*292: uyap* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "uyap",		"uyap",		 0, STAR, loc+281,      0, &pspec74 },
/*293: udsbr* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "udsbr",		"udsbr",	 0, STAR, loc+287,      0, &pspec74 },
/*294: slurm* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "slurm",		"slurm",	 0, STAR, loc+293,      0, &pspec75 },
/*295: uthum* at uhidev? reportid -1 */
    { "uthum",		"uthum",	 0, STAR, loc+808,      0, &pspec76 },
/*296: aue* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "aue",		"aue",		 0, STAR, loc+299,      0, &pspec74 },
/*297: cdce* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "cdce",		"cdce",		 0, STAR, loc+305,      0, &pspec75 },
/*298: cue* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "cue",		"cue",		 0, STAR, loc+311,      0, &pspec74 },
/*299: kue* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "kue",		"kue",		 0, STAR, loc+317,      0, &pspec74 },
/*300: upl* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "upl",		"upl",		 0, STAR, loc+323,      0, &pspec74 },
/*301: url* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "url",		"url",		 0, STAR, loc+329,      0, &pspec74 },
/*302: axe* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "axe",		"axe",		 0, STAR, loc+335,      0, &pspec74 },
/*303: axen* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "axen",		"axen",		 0, STAR, loc+341,      0, &pspec74 },
/*304: mue* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "mue",		"mue",		 0, STAR, loc+347,      0, &pspec74 },
/*305: udav* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "udav",		"udav",		 0, STAR, loc+353,      0, &pspec74 },
/*306: otus* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "otus",		"otus",		 0, STAR, loc+359,      0, &pspec74 },
/*307: ure* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "ure",		"ure",		 0, STAR, loc+365,      0, &pspec74 },
/*308: umodem* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "umodem",		"umodem",	 0, STAR, loc+371,      0, &pspec75 },
/*309: uftdi* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "uftdi",		"uftdi",	 0, STAR, loc+377,      0, &pspec75 },
/*310: uplcom* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "uplcom",		"uplcom",	 0, STAR, loc+383,      0, &pspec74 },
/*311: umct* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "umct",		"umct",		 0, STAR, loc+389,      0, &pspec74 },
/*312: umcs* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "umcs",		"umcs",		 0, STAR, loc+395,      0, &pspec74 },
/*313: uvscom* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "uvscom",		"uvscom",	 0, STAR, loc+401,      0, &pspec74 },
/*314: uxrcom* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "uxrcom",		"uxrcom",	 0, STAR, loc+407,      0, &pspec75 },
/*315: ubsa* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "ubsa",		"ubsa",		 0, STAR, loc+413,      0, &pspec74 },
/*316: uipaq* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "uipaq",		"uipaq",	 0, STAR, loc+419,      0, &pspec74 },
/*317: ukyopon* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "ukyopon",	"ukyopon",	 0, STAR, loc+425,      0, &pspec75 },
/*318: uark* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "uark",		"uark",		 0, STAR, loc+431,      0, &pspec74 },
/*319: uslsa* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "uslsa",		"uslsa",	 0, STAR, loc+437,      0, &pspec75 },
/*320: uchcom* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "uchcom",		"uchcom",	 0, STAR, loc+443,      0, &pspec74 },
/*321: usscanner* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "usscanner",	"usscanner",	 0, STAR, loc+449,      0, &pspec74 },
/*322: atu* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "atu",		"atu",		 0, STAR, loc+455,      0, &pspec74 },
/*323: upgt* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "upgt",		"upgt",		 0, STAR, loc+461,      0, &pspec74 },
/*324: usmsc* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "usmsc",		"usmsc",	 0, STAR, loc+467,      0, &pspec74 },
/*325: ural* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "ural",		"ural",		 0, STAR, loc+473,      0, &pspec74 },
/*326: rum* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "rum",		"rum",		 0, STAR, loc+479,      0, &pspec74 },
/*327: utoppy* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "utoppy",		"utoppy",	 0, STAR, loc+485,      0, &pspec74 },
/*328: zyd* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "zyd",		"zyd",		 0, STAR, loc+491,      0, &pspec74 },
/*329: udl* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "udl",		"udl",		 0, STAR, loc+497,      0, &pspec74 },
/*330: uhso* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "uhso",		"uhso",		 0, STAR, loc+503,      0, &pspec74 },
/*331: urndis* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "urndis",		"urndis",	 0, STAR, loc+509,      0, &pspec75 },
/*332: urtwn* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "urtwn",		"urtwn",	 0, STAR, loc+515,      0, &pspec74 },
/*333: urtw* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "urtw",		"urtw",		 0, STAR, loc+521,      0, &pspec74 },
/*334: run* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "run",		"run",		 0, STAR, loc+527,      0, &pspec74 },
/*335: udsir* at uhub? port -1 configuration -1 interface -1 vendor -1 product -1 release -1 */
    { "udsir",		"udsir",	 0, STAR, loc+533,      0, &pspec75 },
/*336: bthub* at ubt? */
    { "bthub",		"bthub",	 0, STAR,    NULL,      0, &pspec113 },
/*337: bthub* at bcsp? */
    { "bthub",		"bthub",	 0, STAR,    NULL,      0, &pspec127 },
/*338: bthub* at bt3c? */
    { "bthub",		"bthub",	 0, STAR,    NULL,      0, &pspec128 },
/*339: bthub* at btbc? */
    { "bthub",		"bthub",	 0, STAR,    NULL,      0, &pspec129 },
/*340: bthub* at btuart? */
    { "bthub",		"bthub",	 0, STAR,    NULL,      0, &pspec130 },
/*341: bthub* at sbt? */
    { "bthub",		"bthub",	 0, STAR,    NULL,      0, &pspec131 },
/*342: bthidev* at bthub? */
    { "bthidev",	"bthidev",	 0, STAR,    NULL,      0, &pspec132 },
/*343: btkbd* at bthidev? reportid -1 */
    { "btkbd",		"btkbd",	 0, STAR, loc+809,      0, &pspec133 },
/*344: btms* at bthidev? reportid -1 */
    { "btms",		"btms",		 0, STAR, loc+810,      0, &pspec133 },
/*345: btmagic* at bthub? */
    { "btmagic",	"btmagic",	 0, STAR,    NULL,      0, &pspec132 },
/*346: btsco* at bthub? */
    { "btsco",		"btsco",	 0, STAR,    NULL,      0, &pspec132 },
/*347: sdmmc* at sdhc? */
    { "sdmmc",		"sdmmc",	 0, STAR,    NULL,      0, &pspec137 },
/*348: sdmmc* at rtsx? */
    { "sdmmc",		"sdmmc",	 0, STAR,    NULL,      0, &pspec138 },
/*349: sbt* at sdmmc? */
    { "sbt",		"sbt",		 0, STAR,    NULL,      0, &pspec126 },
/*350: ieee1394if* at fwohci? */
    { "ieee1394if",	"ieee1394if",	 0, STAR,    NULL,      0, &pspec114 },
/*351: fwip* at ieee1394if? euihi -1 euilo -1 */
    { "fwip",		"fwip",		 0, STAR, loc+734,      0, &pspec115 },
/*352: sbp* at ieee1394if? euihi -1 euilo -1 */
    { "sbp",		"sbp",		 0, STAR, loc+736,      0, &pspec115 },
/*353: acpi0 at mainbus0 */
    { "acpi",		"acpi",		 0, NORM,    NULL,      0, &pspec7 },
/*354: acpiec* at acpi? */
    { "acpiec",		"acpiec",	 0, STAR,    NULL,      0, &pspec5 },
/*355: acpiecdt* at acpi? */
    { "acpiecdt",	"acpiecdt",	 0, STAR,    NULL,      0, &pspec8 },
/*356: acpilid* at acpi? */
    { "acpilid",	"acpilid",	 0, STAR,    NULL,      0, &pspec5 },
/*357: acpibut* at acpi? */
    { "acpibut",	"acpibut",	 0, STAR,    NULL,      0, &pspec5 },
/*358: acpiacad* at acpi? */
    { "acpiacad",	"acpiacad",	 0, STAR,    NULL,      0, &pspec5 },
/*359: acpibat* at acpi? */
    { "acpibat",	"acpibat",	 0, STAR,    NULL,      0, &pspec5 },
/*360: acpivga* at acpi? */
    { "acpivga",	"acpivga",	 0, STAR,    NULL,      0, &pspec5 },
/*361: acpiout* at acpivga? */
    { "acpiout",	"acpiout",	 0, STAR,    NULL,      0, &pspec9 },
/*362: acpifan* at acpi? */
    { "acpifan",	"acpifan",	 0, STAR,    NULL,      0, &pspec5 },
/*363: acpitz* at acpi? */
    { "acpitz",		"acpitz",	 0, STAR,    NULL,      0, &pspec5 },
/*364: acpiwdrt* at acpi? */
    { "acpiwdrt",	"acpiwdrt",	 0, STAR,    NULL,      0, &pspec10 },
/*365: acpidalb* at acpi? */
    { "acpidalb",	"acpidalb",	 0, STAR,    NULL,      0, &pspec5 },
/*366: ipmi_acpi* at acpi? */
    { "ipmi_acpi",	"ipmi_acpi",	 0, STAR,    NULL,      0, &pspec5 },
/*367: acpiwmi* at acpi? */
    { "acpiwmi",	"acpiwmi",	 0, STAR,    NULL,      0, &pspec5 },
    { NULL,		NULL,		 0,    0,    NULL,      0, NULL }
};

static struct cfattach * const spkr_cfattachinit[] = {
	&spkr_audio_ca, &spkr_pcppi_ca, NULL
};
static struct cfattach * const audio_cfattachinit[] = {
	&audio_ca, NULL
};
static struct cfattach * const midi_cfattachinit[] = {
	&midi_ca, &midi_pcppi_ca, NULL
};
static struct cfattach * const hdaudio_cfattachinit[] = {
	&hdaudio_pci_ca, NULL
};
static struct cfattach * const hdafg_cfattachinit[] = {
	&hdafg_ca, NULL
};
static struct cfattach * const video_cfattachinit[] = {
	&video_ca, NULL
};
static struct cfattach * const dtv_cfattachinit[] = {
	&dtv_ca, NULL
};
static struct cfattach * const iic_cfattachinit[] = {
	&iic_ca, NULL
};
static struct cfattach * const ihidev_cfattachinit[] = {
	&ihidev_ca, NULL
};
static struct cfattach * const ims_cfattachinit[] = {
	&ims_ca, NULL
};
static struct cfattach * const irframe_cfattachinit[] = {
	&irframe_ca, NULL
};
static struct cfattach * const cir_cfattachinit[] = {
	&cir_ca, NULL
};
static struct cfattach * const lpt_cfattachinit[] = {
	&lpt_puc_ca, &lpt_isa_ca, NULL
};
static struct cfattach * const tpm_cfattachinit[] = {
	&tpm_isa_ca, NULL
};
static struct cfattach * const ld_cfattachinit[] = {
	&ld_virtio_ca, &ld_nvme_ca, &ld_ataraid_ca, &ld_iop_ca, &ld_sdmmc_ca, NULL
};
static struct cfattach * const cy_cfattachinit[] = {
	&cy_pci_ca, NULL
};
static struct cfattach * const sm_cfattachinit[] = {
	&sm_mhzc_ca, NULL
};
static struct cfattach * const com_cfattachinit[] = {
	&com_puc_ca, &com_isa_ca, &com_cardbus_ca, &com_pcmcia_ca, &com_pcmcom_ca, &com_mhzc_ca, NULL
};
static struct cfattach * const pckbc_cfattachinit[] = {
	&pckbc_isa_ca, &pckbc_acpi_ca, NULL
};
static struct cfattach * const attimer_cfattachinit[] = {
	&attimer_isa_ca, &attimer_acpi_ca, NULL
};
static struct cfattach * const opl_cfattachinit[] = {
	&opl_fms_ca, &opl_eso_ca, &opl_sv_ca, &opl_cmpci_ca, NULL
};
static struct cfattach * const mpu_cfattachinit[] = {
	&mpu_eso_ca, &mpu_cmpci_ca, &mpu_yds_ca, &mpu_acpi_ca, NULL
};
static struct cfattach * const virtio_cfattachinit[] = {
	&virtio_pci_ca, NULL
};
static struct cfattach * const viomb_cfattachinit[] = {
	&viomb_ca, NULL
};
static struct cfattach * const vioif_cfattachinit[] = {
	&vioif_ca, NULL
};
static struct cfattach * const viornd_cfattachinit[] = {
	&viornd_ca, NULL
};
static struct cfattach * const vioscsi_cfattachinit[] = {
	&vioscsi_ca, NULL
};
static struct cfattach * const wdc_cfattachinit[] = {
	&wdc_isa_ca, &wdc_pcmcia_ca, NULL
};
static struct cfattach * const atabus_cfattachinit[] = {
	&atabus_ca, NULL
};
static struct cfattach * const njata_cfattachinit[] = {
	&njata_cardbus_ca, NULL
};
static struct cfattach * const ahcisata_cfattachinit[] = {
	&ahcisata_pci_ca, &jmahci_ca, NULL
};
static struct cfattach * const siisata_cfattachinit[] = {
	&siisata_pci_ca, &siisata_cardbus_ca, NULL
};
static struct cfattach * const mvsata_cfattachinit[] = {
	&mvsata_pci_ca, NULL
};
static struct cfattach * const dwiic_cfattachinit[] = {
	&pcidwiic_ca, NULL
};
static struct cfattach * const hpet_cfattachinit[] = {
	&amdpcib_hpet_ca, &hpet_acpi_dev_ca, &hpet_acpi_tab_ca, NULL
};
static struct cfattach * const vga_cfattachinit[] = {
	&vga_pci_ca, NULL
};
static struct cfattach * const wsdisplay_cfattachinit[] = {
	&wsdisplay_emul_ca, NULL
};
static struct cfattach * const wskbd_cfattachinit[] = {
	&wskbd_ca, NULL
};
static struct cfattach * const wsmouse_cfattachinit[] = {
	&wsmouse_ca, NULL
};
static struct cfattach * const genfb_cfattachinit[] = {
	&genfb_pci_ca, NULL
};
static struct cfattach * const uhci_cfattachinit[] = {
	&uhci_pci_ca, &uhci_cardbus_ca, NULL
};
static struct cfattach * const ohci_cfattachinit[] = {
	&ohci_pci_ca, &ohci_cardbus_ca, NULL
};
static struct cfattach * const ehci_cfattachinit[] = {
	&ehci_pci_ca, &ehci_cardbus_ca, NULL
};
static struct cfattach * const xhci_cfattachinit[] = {
	&xhci_pci_ca, NULL
};
static struct cfattach * const slhci_cfattachinit[] = {
	&slhci_pcmcia_ca, NULL
};
static struct cfattach * const sdhc_cfattachinit[] = {
	&sdhc_pci_ca, &sdhc_cardbus_ca, &sdhc_acpi_ca, NULL
};
static struct cfattach * const rtsx_cfattachinit[] = {
	&rtsx_pci_ca, NULL
};
static struct cfattach * const radio_cfattachinit[] = {
	&radio_ca, NULL
};
static struct cfattach * const fwohci_cfattachinit[] = {
	&fwohci_pci_ca, &fwohci_cardbus_ca, NULL
};
static struct cfattach * const nvme_cfattachinit[] = {
	&nvme_pci_ca, NULL
};
static struct cfattach * const bwfm_cfattachinit[] = {
	&bwfm_usb_ca, NULL
};
static struct cfattach * const qemufwcfg_cfattachinit[] = {
	&qemufwcfg_acpi_ca, NULL
};
static struct cfattach * const ipmi_cfattachinit[] = {
	&ipmi_ca, NULL
};
static struct cfattach * const joy_cfattachinit[] = {
	&joy_acpi_ca, NULL
};
static struct cfattach * const gpio_cfattachinit[] = {
	&gpio_ca, NULL
};
static struct cfattach * const gpioow_cfattachinit[] = {
	&gpioow_ca, NULL
};
static struct cfattach * const onewire_cfattachinit[] = {
	&onewire_ca, NULL
};
static struct cfattach * const athn_cfattachinit[] = {
	&athn_usb_ca, NULL
};
static struct cfattach * const cpu_cfattachinit[] = {
	&cpu_ca, NULL
};
static struct cfattach * const acpicpu_cfattachinit[] = {
	&acpicpu_ca, NULL
};
static struct cfattach * const coretemp_cfattachinit[] = {
	&coretemp_ca, NULL
};
static struct cfattach * const est_cfattachinit[] = {
	&est_ca, NULL
};
static struct cfattach * const powernow_cfattachinit[] = {
	&powernow_ca, NULL
};
static struct cfattach * const ioapic_cfattachinit[] = {
	&ioapic_ca, NULL
};
static struct cfattach * const scsibus_cfattachinit[] = {
	&scsibus_ca, NULL
};
static struct cfattach * const atapibus_cfattachinit[] = {
	&atapibus_ca, NULL
};
static struct cfattach * const cd_cfattachinit[] = {
	&cd_ca, NULL
};
static struct cfattach * const ch_cfattachinit[] = {
	&ch_ca, NULL
};
static struct cfattach * const sd_cfattachinit[] = {
	&sd_ca, NULL
};
static struct cfattach * const st_cfattachinit[] = {
	&st_scsibus_ca, &st_atapibus_ca, NULL
};
static struct cfattach * const ses_cfattachinit[] = {
	&ses_ca, NULL
};
static struct cfattach * const ss_cfattachinit[] = {
	&ss_ca, NULL
};
static struct cfattach * const uk_cfattachinit[] = {
	&uk_ca, NULL
};
static struct cfattach * const wd_cfattachinit[] = {
	&wd_ca, NULL
};
static struct cfattach * const iop_cfattachinit[] = {
	&iop_pci_ca, NULL
};
static struct cfattach * const iopsp_cfattachinit[] = {
	&iopsp_ca, NULL
};
static struct cfattach * const mainbus_cfattachinit[] = {
	&mainbus_ca, NULL
};
static struct cfattach * const pci_cfattachinit[] = {
	&pci_ca, NULL
};
static struct cfattach * const pciide_cfattachinit[] = {
	&pciide_ca, NULL
};
static struct cfattach * const acardide_cfattachinit[] = {
	&acardide_ca, NULL
};
static struct cfattach * const aceride_cfattachinit[] = {
	&aceride_ca, NULL
};
static struct cfattach * const artsata_cfattachinit[] = {
	&artsata_ca, NULL
};
static struct cfattach * const cmdide_cfattachinit[] = {
	&cmdide_ca, NULL
};
static struct cfattach * const cypide_cfattachinit[] = {
	&cypide_ca, NULL
};
static struct cfattach * const hptide_cfattachinit[] = {
	&hptide_ca, NULL
};
static struct cfattach * const iteide_cfattachinit[] = {
	&iteide_ca, NULL
};
static struct cfattach * const jmide_cfattachinit[] = {
	&jmide_ca, NULL
};
static struct cfattach * const optiide_cfattachinit[] = {
	&optiide_ca, NULL
};
static struct cfattach * const piixide_cfattachinit[] = {
	&piixide_ca, NULL
};
static struct cfattach * const pdcsata_cfattachinit[] = {
	&pdcsata_ca, NULL
};
static struct cfattach * const pdcide_cfattachinit[] = {
	&pdcide_ca, NULL
};
static struct cfattach * const svwsata_cfattachinit[] = {
	&svwsata_ca, NULL
};
static struct cfattach * const satalink_cfattachinit[] = {
	&satalink_ca, NULL
};
static struct cfattach * const siside_cfattachinit[] = {
	&siside_ca, NULL
};
static struct cfattach * const slide_cfattachinit[] = {
	&slide_ca, NULL
};
static struct cfattach * const viaide_cfattachinit[] = {
	&viaide_ca, NULL
};
static struct cfattach * const ixpide_cfattachinit[] = {
	&ixpide_ca, NULL
};
static struct cfattach * const toshide_cfattachinit[] = {
	&toshide_ca, NULL
};
static struct cfattach * const ppb_cfattachinit[] = {
	&ppb_ca, NULL
};
static struct cfattach * const cz_cfattachinit[] = {
	&cz_ca, NULL
};
static struct cfattach * const bktr_cfattachinit[] = {
	&bktr_ca, NULL
};
static struct cfattach * const clcs_cfattachinit[] = {
	&clcs_ca, NULL
};
static struct cfattach * const clct_cfattachinit[] = {
	&clct_ca, NULL
};
static struct cfattach * const fms_cfattachinit[] = {
	&fms_ca, NULL
};
static struct cfattach * const eap_cfattachinit[] = {
	&eap_ca, NULL
};
static struct cfattach * const auacer_cfattachinit[] = {
	&auacer_ca, NULL
};
static struct cfattach * const auich_cfattachinit[] = {
	&auich_ca, NULL
};
static struct cfattach * const auvia_cfattachinit[] = {
	&auvia_ca, NULL
};
static struct cfattach * const auixp_cfattachinit[] = {
	&auixp_ca, NULL
};
static struct cfattach * const neo_cfattachinit[] = {
	&neo_ca, NULL
};
static struct cfattach * const esa_cfattachinit[] = {
	&esa_ca, NULL
};
static struct cfattach * const eso_cfattachinit[] = {
	&eso_ca, NULL
};
static struct cfattach * const esm_cfattachinit[] = {
	&esm_ca, NULL
};
static struct cfattach * const sv_cfattachinit[] = {
	&sv_ca, NULL
};
static struct cfattach * const cmpci_cfattachinit[] = {
	&cmpci_ca, NULL
};
static struct cfattach * const yds_cfattachinit[] = {
	&yds_ca, NULL
};
static struct cfattach * const emuxki_cfattachinit[] = {
	&emuxki_ca, NULL
};
static struct cfattach * const autri_cfattachinit[] = {
	&autri_ca, NULL
};
static struct cfattach * const puc_cfattachinit[] = {
	&puc_ca, NULL
};
static struct cfattach * const cbb_cfattachinit[] = {
	&cbb_pci_ca, NULL
};
static struct cfattach * const piixpm_cfattachinit[] = {
	&piixpm_ca, NULL
};
static struct cfattach * const amdpm_cfattachinit[] = {
	&amdpm_ca, NULL
};
static struct cfattach * const hifn_cfattachinit[] = {
	&hifn_ca, NULL
};
static struct cfattach * const ubsec_cfattachinit[] = {
	&ubsec_ca, NULL
};
static struct cfattach * const weasel_cfattachinit[] = {
	&weasel_pci_ca, NULL
};
static struct cfattach * const nfsmbc_cfattachinit[] = {
	&nfsmbc_ca, NULL
};
static struct cfattach * const nfsmb_cfattachinit[] = {
	&nfsmb_ca, NULL
};
static struct cfattach * const ichsmb_cfattachinit[] = {
	&ichsmb_ca, NULL
};
static struct cfattach * const cxdtv_cfattachinit[] = {
	&cxdtv_ca, NULL
};
static struct cfattach * const coram_cfattachinit[] = {
	&coram_ca, NULL
};
static struct cfattach * const pwdog_cfattachinit[] = {
	&pwdog_ca, NULL
};
static struct cfattach * const i915drmkms_cfattachinit[] = {
	&i915drmkms_ca, NULL
};
static struct cfattach * const intelfb_cfattachinit[] = {
	&intelfb_ca, NULL
};
static struct cfattach * const radeon_cfattachinit[] = {
	&radeon_ca, NULL
};
static struct cfattach * const radeondrmkmsfb_cfattachinit[] = {
	&radeondrmkmsfb_ca, NULL
};
static struct cfattach * const nouveau_cfattachinit[] = {
	&nouveau_pci_ca, NULL
};
static struct cfattach * const nouveaufb_cfattachinit[] = {
	&nouveaufb_ca, NULL
};
static struct cfattach * const ismt_cfattachinit[] = {
	&ismt_ca, NULL
};
static struct cfattach * const agp_cfattachinit[] = {
	&agp_ca, NULL
};
static struct cfattach * const aapic_cfattachinit[] = {
	&aapic_ca, NULL
};
static struct cfattach * const pchb_cfattachinit[] = {
	&pchb_ca, NULL
};
static struct cfattach * const pcib_cfattachinit[] = {
	&pcib_ca, NULL
};
static struct cfattach * const amdpcib_cfattachinit[] = {
	&amdpcib_ca, NULL
};
static struct cfattach * const amdnb_misc_cfattachinit[] = {
	&amdnb_misc_ca, NULL
};
static struct cfattach * const amdsmn_cfattachinit[] = {
	&amdsmn_ca, NULL
};
static struct cfattach * const amdzentemp_cfattachinit[] = {
	&amdzentemp_ca, NULL
};
static struct cfattach * const amdtemp_cfattachinit[] = {
	&amdtemp_ca, NULL
};
static struct cfattach * const ichlpcib_cfattachinit[] = {
	&ichlpcib_ca, NULL
};
static struct cfattach * const tco_cfattachinit[] = {
	&tco_ca, NULL
};
static struct cfattach * const fwhrng_cfattachinit[] = {
	&fwhrng_ca, NULL
};
static struct cfattach * const isa_cfattachinit[] = {
	&isa_ca, NULL
};
static struct cfattach * const pcppi_cfattachinit[] = {
	&pcppi_ca, &pcppi_acpi_ca, NULL
};
static struct cfattach * const pckbd_cfattachinit[] = {
	&pckbd_ca, NULL
};
static struct cfattach * const pms_cfattachinit[] = {
	&pms_ca, NULL
};
static struct cfattach * const sysbeep_cfattachinit[] = {
	&sysbeep_ca, NULL
};
static struct cfattach * const fdc_cfattachinit[] = {
	&fdc_isa_ca, NULL
};
static struct cfattach * const fd_cfattachinit[] = {
	&fd_ca, NULL
};
static struct cfattach * const cardslot_cfattachinit[] = {
	&cardslot_ca, NULL
};
static struct cfattach * const cardbus_cfattachinit[] = {
	&cardbus_ca, NULL
};
static struct cfattach * const pcmcia_cfattachinit[] = {
	&pcmcia_ca, NULL
};
static struct cfattach * const pcmcom_cfattachinit[] = {
	&pcmcom_ca, NULL
};
static struct cfattach * const mhzc_cfattachinit[] = {
	&mhzc_ca, NULL
};
static struct cfattach * const bt3c_cfattachinit[] = {
	&bt3c_ca, NULL
};
static struct cfattach * const btbc_cfattachinit[] = {
	&btbc_ca, NULL
};
static struct cfattach * const usb_cfattachinit[] = {
	&usb_ca, NULL
};
static struct cfattach * const uhub_cfattachinit[] = {
	&uroothub_ca, &uhub_ca, NULL
};
static struct cfattach * const uaudio_cfattachinit[] = {
	&uaudio_ca, NULL
};
static struct cfattach * const umidi_cfattachinit[] = {
	&umidi_ca, NULL
};
static struct cfattach * const ucom_cfattachinit[] = {
	&ucom_ca, NULL
};
static struct cfattach * const ugen_cfattachinit[] = {
	&ugen_ca, NULL
};
static struct cfattach * const ugenif_cfattachinit[] = {
	&ugenif_ca, NULL
};
static struct cfattach * const uhidev_cfattachinit[] = {
	&uhidev_ca, NULL
};
static struct cfattach * const uhid_cfattachinit[] = {
	&uhid_ca, NULL
};
static struct cfattach * const ukbd_cfattachinit[] = {
	&ukbd_ca, NULL
};
static struct cfattach * const ums_cfattachinit[] = {
	&ums_ca, NULL
};
static struct cfattach * const uts_cfattachinit[] = {
	&uts_ca, NULL
};
static struct cfattach * const uep_cfattachinit[] = {
	&uep_ca, NULL
};
static struct cfattach * const ucycom_cfattachinit[] = {
	&ucycom_ca, NULL
};
static struct cfattach * const ulpt_cfattachinit[] = {
	&ulpt_ca, NULL
};
static struct cfattach * const umass_cfattachinit[] = {
	&umass_ca, NULL
};
static struct cfattach * const uirda_cfattachinit[] = {
	&uirda_ca, NULL
};
static struct cfattach * const stuirda_cfattachinit[] = {
	&stuirda_ca, NULL
};
static struct cfattach * const ustir_cfattachinit[] = {
	&ustir_ca, NULL
};
static struct cfattach * const irmce_cfattachinit[] = {
	&irmce_ca, NULL
};
static struct cfattach * const ubt_cfattachinit[] = {
	&ubt_ca, NULL
};
static struct cfattach * const aubtfwl_cfattachinit[] = {
	&aubtfwl_ca, NULL
};
static struct cfattach * const pseye_cfattachinit[] = {
	&pseye_ca, NULL
};
static struct cfattach * const uvideo_cfattachinit[] = {
	&uvideo_ca, NULL
};
static struct cfattach * const auvitek_cfattachinit[] = {
	&auvitek_ca, NULL
};
static struct cfattach * const emdtv_cfattachinit[] = {
	&emdtv_ca, NULL
};
static struct cfattach * const umodeswitch_cfattachinit[] = {
	&umodeswitch_ca, NULL
};
static struct cfattach * const urio_cfattachinit[] = {
	&urio_ca, NULL
};
static struct cfattach * const uipad_cfattachinit[] = {
	&uipad_ca, NULL
};
static struct cfattach * const uberry_cfattachinit[] = {
	&uberry_ca, NULL
};
static struct cfattach * const uvisor_cfattachinit[] = {
	&uvisor_ca, NULL
};
static struct cfattach * const ugensa_cfattachinit[] = {
	&ugensa_ca, NULL
};
static struct cfattach * const u3g_cfattachinit[] = {
	&u3g_ca, NULL
};
static struct cfattach * const uyap_cfattachinit[] = {
	&uyap_ca, NULL
};
static struct cfattach * const udsbr_cfattachinit[] = {
	&udsbr_ca, NULL
};
static struct cfattach * const slurm_cfattachinit[] = {
	&slurm_ca, NULL
};
static struct cfattach * const uthum_cfattachinit[] = {
	&uthum_ca, NULL
};
static struct cfattach * const aue_cfattachinit[] = {
	&aue_ca, NULL
};
static struct cfattach * const cdce_cfattachinit[] = {
	&cdce_ca, NULL
};
static struct cfattach * const cue_cfattachinit[] = {
	&cue_ca, NULL
};
static struct cfattach * const kue_cfattachinit[] = {
	&kue_ca, NULL
};
static struct cfattach * const upl_cfattachinit[] = {
	&upl_ca, NULL
};
static struct cfattach * const url_cfattachinit[] = {
	&url_ca, NULL
};
static struct cfattach * const axe_cfattachinit[] = {
	&axe_ca, NULL
};
static struct cfattach * const axen_cfattachinit[] = {
	&axen_ca, NULL
};
static struct cfattach * const mue_cfattachinit[] = {
	&mue_ca, NULL
};
static struct cfattach * const udav_cfattachinit[] = {
	&udav_ca, NULL
};
static struct cfattach * const otus_cfattachinit[] = {
	&otus_ca, NULL
};
static struct cfattach * const ure_cfattachinit[] = {
	&ure_ca, NULL
};
static struct cfattach * const umodem_cfattachinit[] = {
	&umodem_ca, NULL
};
static struct cfattach * const uftdi_cfattachinit[] = {
	&uftdi_ca, NULL
};
static struct cfattach * const uplcom_cfattachinit[] = {
	&uplcom_ca, NULL
};
static struct cfattach * const umct_cfattachinit[] = {
	&umct_ca, NULL
};
static struct cfattach * const umcs_cfattachinit[] = {
	&umcs_ca, NULL
};
static struct cfattach * const uvscom_cfattachinit[] = {
	&uvscom_ca, NULL
};
static struct cfattach * const uxrcom_cfattachinit[] = {
	&uxrcom_ca, NULL
};
static struct cfattach * const ubsa_cfattachinit[] = {
	&ubsa_ca, NULL
};
static struct cfattach * const uipaq_cfattachinit[] = {
	&uipaq_ca, NULL
};
static struct cfattach * const ukyopon_cfattachinit[] = {
	&ukyopon_ca, NULL
};
static struct cfattach * const uark_cfattachinit[] = {
	&uark_ca, NULL
};
static struct cfattach * const uslsa_cfattachinit[] = {
	&uslsa_ca, NULL
};
static struct cfattach * const uchcom_cfattachinit[] = {
	&uchcom_ca, NULL
};
static struct cfattach * const usscanner_cfattachinit[] = {
	&usscanner_ca, NULL
};
static struct cfattach * const atu_cfattachinit[] = {
	&atu_ca, NULL
};
static struct cfattach * const upgt_cfattachinit[] = {
	&upgt_ca, NULL
};
static struct cfattach * const usmsc_cfattachinit[] = {
	&usmsc_ca, NULL
};
static struct cfattach * const ural_cfattachinit[] = {
	&ural_ca, NULL
};
static struct cfattach * const rum_cfattachinit[] = {
	&rum_ca, NULL
};
static struct cfattach * const utoppy_cfattachinit[] = {
	&utoppy_ca, NULL
};
static struct cfattach * const zyd_cfattachinit[] = {
	&zyd_ca, NULL
};
static struct cfattach * const udl_cfattachinit[] = {
	&udl_ca, NULL
};
static struct cfattach * const uhso_cfattachinit[] = {
	&uhso_ca, NULL
};
static struct cfattach * const urndis_cfattachinit[] = {
	&urndis_ca, NULL
};
static struct cfattach * const urtwn_cfattachinit[] = {
	&urtwn_ca, NULL
};
static struct cfattach * const urtw_cfattachinit[] = {
	&urtw_ca, NULL
};
static struct cfattach * const run_cfattachinit[] = {
	&run_ca, NULL
};
static struct cfattach * const udsir_cfattachinit[] = {
	&udsir_ca, NULL
};
static struct cfattach * const bthub_cfattachinit[] = {
	&bthub_ca, NULL
};
static struct cfattach * const bthidev_cfattachinit[] = {
	&bthidev_ca, NULL
};
static struct cfattach * const btkbd_cfattachinit[] = {
	&btkbd_ca, NULL
};
static struct cfattach * const btms_cfattachinit[] = {
	&btms_ca, NULL
};
static struct cfattach * const btmagic_cfattachinit[] = {
	&btmagic_ca, NULL
};
static struct cfattach * const btsco_cfattachinit[] = {
	&btsco_ca, NULL
};
static struct cfattach * const sdmmc_cfattachinit[] = {
	&sdmmc_ca, NULL
};
static struct cfattach * const sbt_cfattachinit[] = {
	&sbt_ca, NULL
};
static struct cfattach * const ieee1394if_cfattachinit[] = {
	&ieee1394if_ca, NULL
};
static struct cfattach * const fwip_cfattachinit[] = {
	&fwip_ca, NULL
};
static struct cfattach * const sbp_cfattachinit[] = {
	&sbp_ca, NULL
};
static struct cfattach * const acpi_cfattachinit[] = {
	&acpi_ca, NULL
};
static struct cfattach * const acpiec_cfattachinit[] = {
	&acpiec_ca, NULL
};
static struct cfattach * const acpiecdt_cfattachinit[] = {
	&acpiecdt_ca, NULL
};
static struct cfattach * const acpilid_cfattachinit[] = {
	&acpilid_ca, NULL
};
static struct cfattach * const acpibut_cfattachinit[] = {
	&acpibut_ca, NULL
};
static struct cfattach * const acpiacad_cfattachinit[] = {
	&acpiacad_ca, NULL
};
static struct cfattach * const acpibat_cfattachinit[] = {
	&acpibat_ca, NULL
};
static struct cfattach * const acpivga_cfattachinit[] = {
	&acpivga_ca, NULL
};
static struct cfattach * const acpiout_cfattachinit[] = {
	&acpiout_ca, NULL
};
static struct cfattach * const acpifan_cfattachinit[] = {
	&acpifan_ca, NULL
};
static struct cfattach * const acpitz_cfattachinit[] = {
	&acpitz_ca, NULL
};
static struct cfattach * const acpiwdrt_cfattachinit[] = {
	&acpiwdrt_ca, NULL
};
static struct cfattach * const acpidalb_cfattachinit[] = {
	&acpidalb_ca, NULL
};
static struct cfattach * const ipmi_acpi_cfattachinit[] = {
	&ipmi_acpi_ca, NULL
};
static struct cfattach * const acpiwmi_cfattachinit[] = {
	&acpiwmi_ca, NULL
};

const struct cfattachinit cfattachinit[] = {
	{ "spkr", spkr_cfattachinit },
	{ "audio", audio_cfattachinit },
	{ "midi", midi_cfattachinit },
	{ "hdaudio", hdaudio_cfattachinit },
	{ "hdafg", hdafg_cfattachinit },
	{ "video", video_cfattachinit },
	{ "dtv", dtv_cfattachinit },
	{ "iic", iic_cfattachinit },
	{ "ihidev", ihidev_cfattachinit },
	{ "ims", ims_cfattachinit },
	{ "irframe", irframe_cfattachinit },
	{ "cir", cir_cfattachinit },
	{ "lpt", lpt_cfattachinit },
	{ "tpm", tpm_cfattachinit },
	{ "ld", ld_cfattachinit },
	{ "cy", cy_cfattachinit },
	{ "sm", sm_cfattachinit },
	{ "com", com_cfattachinit },
	{ "pckbc", pckbc_cfattachinit },
	{ "attimer", attimer_cfattachinit },
	{ "opl", opl_cfattachinit },
	{ "mpu", mpu_cfattachinit },
	{ "virtio", virtio_cfattachinit },
	{ "viomb", viomb_cfattachinit },
	{ "vioif", vioif_cfattachinit },
	{ "viornd", viornd_cfattachinit },
	{ "vioscsi", vioscsi_cfattachinit },
	{ "wdc", wdc_cfattachinit },
	{ "atabus", atabus_cfattachinit },
	{ "njata", njata_cfattachinit },
	{ "ahcisata", ahcisata_cfattachinit },
	{ "siisata", siisata_cfattachinit },
	{ "mvsata", mvsata_cfattachinit },
	{ "dwiic", dwiic_cfattachinit },
	{ "hpet", hpet_cfattachinit },
	{ "vga", vga_cfattachinit },
	{ "wsdisplay", wsdisplay_cfattachinit },
	{ "wskbd", wskbd_cfattachinit },
	{ "wsmouse", wsmouse_cfattachinit },
	{ "genfb", genfb_cfattachinit },
	{ "uhci", uhci_cfattachinit },
	{ "ohci", ohci_cfattachinit },
	{ "ehci", ehci_cfattachinit },
	{ "xhci", xhci_cfattachinit },
	{ "slhci", slhci_cfattachinit },
	{ "sdhc", sdhc_cfattachinit },
	{ "rtsx", rtsx_cfattachinit },
	{ "radio", radio_cfattachinit },
	{ "fwohci", fwohci_cfattachinit },
	{ "nvme", nvme_cfattachinit },
	{ "bwfm", bwfm_cfattachinit },
	{ "qemufwcfg", qemufwcfg_cfattachinit },
	{ "ipmi", ipmi_cfattachinit },
	{ "joy", joy_cfattachinit },
	{ "gpio", gpio_cfattachinit },
	{ "gpioow", gpioow_cfattachinit },
	{ "onewire", onewire_cfattachinit },
	{ "athn", athn_cfattachinit },
	{ "cpu", cpu_cfattachinit },
	{ "acpicpu", acpicpu_cfattachinit },
	{ "coretemp", coretemp_cfattachinit },
	{ "est", est_cfattachinit },
	{ "powernow", powernow_cfattachinit },
	{ "ioapic", ioapic_cfattachinit },
	{ "scsibus", scsibus_cfattachinit },
	{ "atapibus", atapibus_cfattachinit },
	{ "cd", cd_cfattachinit },
	{ "ch", ch_cfattachinit },
	{ "sd", sd_cfattachinit },
	{ "st", st_cfattachinit },
	{ "ses", ses_cfattachinit },
	{ "ss", ss_cfattachinit },
	{ "uk", uk_cfattachinit },
	{ "wd", wd_cfattachinit },
	{ "iop", iop_cfattachinit },
	{ "iopsp", iopsp_cfattachinit },
	{ "mainbus", mainbus_cfattachinit },
	{ "pci", pci_cfattachinit },
	{ "pciide", pciide_cfattachinit },
	{ "acardide", acardide_cfattachinit },
	{ "aceride", aceride_cfattachinit },
	{ "artsata", artsata_cfattachinit },
	{ "cmdide", cmdide_cfattachinit },
	{ "cypide", cypide_cfattachinit },
	{ "hptide", hptide_cfattachinit },
	{ "iteide", iteide_cfattachinit },
	{ "jmide", jmide_cfattachinit },
	{ "optiide", optiide_cfattachinit },
	{ "piixide", piixide_cfattachinit },
	{ "pdcsata", pdcsata_cfattachinit },
	{ "pdcide", pdcide_cfattachinit },
	{ "svwsata", svwsata_cfattachinit },
	{ "satalink", satalink_cfattachinit },
	{ "siside", siside_cfattachinit },
	{ "slide", slide_cfattachinit },
	{ "viaide", viaide_cfattachinit },
	{ "ixpide", ixpide_cfattachinit },
	{ "toshide", toshide_cfattachinit },
	{ "ppb", ppb_cfattachinit },
	{ "cz", cz_cfattachinit },
	{ "bktr", bktr_cfattachinit },
	{ "clcs", clcs_cfattachinit },
	{ "clct", clct_cfattachinit },
	{ "fms", fms_cfattachinit },
	{ "eap", eap_cfattachinit },
	{ "auacer", auacer_cfattachinit },
	{ "auich", auich_cfattachinit },
	{ "auvia", auvia_cfattachinit },
	{ "auixp", auixp_cfattachinit },
	{ "neo", neo_cfattachinit },
	{ "esa", esa_cfattachinit },
	{ "eso", eso_cfattachinit },
	{ "esm", esm_cfattachinit },
	{ "sv", sv_cfattachinit },
	{ "cmpci", cmpci_cfattachinit },
	{ "yds", yds_cfattachinit },
	{ "emuxki", emuxki_cfattachinit },
	{ "autri", autri_cfattachinit },
	{ "puc", puc_cfattachinit },
	{ "cbb", cbb_cfattachinit },
	{ "piixpm", piixpm_cfattachinit },
	{ "amdpm", amdpm_cfattachinit },
	{ "hifn", hifn_cfattachinit },
	{ "ubsec", ubsec_cfattachinit },
	{ "weasel", weasel_cfattachinit },
	{ "nfsmbc", nfsmbc_cfattachinit },
	{ "nfsmb", nfsmb_cfattachinit },
	{ "ichsmb", ichsmb_cfattachinit },
	{ "cxdtv", cxdtv_cfattachinit },
	{ "coram", coram_cfattachinit },
	{ "pwdog", pwdog_cfattachinit },
	{ "i915drmkms", i915drmkms_cfattachinit },
	{ "intelfb", intelfb_cfattachinit },
	{ "radeon", radeon_cfattachinit },
	{ "radeondrmkmsfb", radeondrmkmsfb_cfattachinit },
	{ "nouveau", nouveau_cfattachinit },
	{ "nouveaufb", nouveaufb_cfattachinit },
	{ "ismt", ismt_cfattachinit },
	{ "agp", agp_cfattachinit },
	{ "aapic", aapic_cfattachinit },
	{ "pchb", pchb_cfattachinit },
	{ "pcib", pcib_cfattachinit },
	{ "amdpcib", amdpcib_cfattachinit },
	{ "amdnb_misc", amdnb_misc_cfattachinit },
	{ "amdsmn", amdsmn_cfattachinit },
	{ "amdzentemp", amdzentemp_cfattachinit },
	{ "amdtemp", amdtemp_cfattachinit },
	{ "ichlpcib", ichlpcib_cfattachinit },
	{ "tco", tco_cfattachinit },
	{ "fwhrng", fwhrng_cfattachinit },
	{ "isa", isa_cfattachinit },
	{ "pcppi", pcppi_cfattachinit },
	{ "pckbd", pckbd_cfattachinit },
	{ "pms", pms_cfattachinit },
	{ "sysbeep", sysbeep_cfattachinit },
	{ "fdc", fdc_cfattachinit },
	{ "fd", fd_cfattachinit },
	{ "cardslot", cardslot_cfattachinit },
	{ "cardbus", cardbus_cfattachinit },
	{ "pcmcia", pcmcia_cfattachinit },
	{ "pcmcom", pcmcom_cfattachinit },
	{ "mhzc", mhzc_cfattachinit },
	{ "bt3c", bt3c_cfattachinit },
	{ "btbc", btbc_cfattachinit },
	{ "usb", usb_cfattachinit },
	{ "uhub", uhub_cfattachinit },
	{ "uaudio", uaudio_cfattachinit },
	{ "umidi", umidi_cfattachinit },
	{ "ucom", ucom_cfattachinit },
	{ "ugen", ugen_cfattachinit },
	{ "ugenif", ugenif_cfattachinit },
	{ "uhidev", uhidev_cfattachinit },
	{ "uhid", uhid_cfattachinit },
	{ "ukbd", ukbd_cfattachinit },
	{ "ums", ums_cfattachinit },
	{ "uts", uts_cfattachinit },
	{ "uep", uep_cfattachinit },
	{ "ucycom", ucycom_cfattachinit },
	{ "ulpt", ulpt_cfattachinit },
	{ "umass", umass_cfattachinit },
	{ "uirda", uirda_cfattachinit },
	{ "stuirda", stuirda_cfattachinit },
	{ "ustir", ustir_cfattachinit },
	{ "irmce", irmce_cfattachinit },
	{ "ubt", ubt_cfattachinit },
	{ "aubtfwl", aubtfwl_cfattachinit },
	{ "pseye", pseye_cfattachinit },
	{ "uvideo", uvideo_cfattachinit },
	{ "auvitek", auvitek_cfattachinit },
	{ "emdtv", emdtv_cfattachinit },
	{ "umodeswitch", umodeswitch_cfattachinit },
	{ "urio", urio_cfattachinit },
	{ "uipad", uipad_cfattachinit },
	{ "uberry", uberry_cfattachinit },
	{ "uvisor", uvisor_cfattachinit },
	{ "ugensa", ugensa_cfattachinit },
	{ "u3g", u3g_cfattachinit },
	{ "uyap", uyap_cfattachinit },
	{ "udsbr", udsbr_cfattachinit },
	{ "slurm", slurm_cfattachinit },
	{ "uthum", uthum_cfattachinit },
	{ "aue", aue_cfattachinit },
	{ "cdce", cdce_cfattachinit },
	{ "cue", cue_cfattachinit },
	{ "kue", kue_cfattachinit },
	{ "upl", upl_cfattachinit },
	{ "url", url_cfattachinit },
	{ "axe", axe_cfattachinit },
	{ "axen", axen_cfattachinit },
	{ "mue", mue_cfattachinit },
	{ "udav", udav_cfattachinit },
	{ "otus", otus_cfattachinit },
	{ "ure", ure_cfattachinit },
	{ "umodem", umodem_cfattachinit },
	{ "uftdi", uftdi_cfattachinit },
	{ "uplcom", uplcom_cfattachinit },
	{ "umct", umct_cfattachinit },
	{ "umcs", umcs_cfattachinit },
	{ "uvscom", uvscom_cfattachinit },
	{ "uxrcom", uxrcom_cfattachinit },
	{ "ubsa", ubsa_cfattachinit },
	{ "uipaq", uipaq_cfattachinit },
	{ "ukyopon", ukyopon_cfattachinit },
	{ "uark", uark_cfattachinit },
	{ "uslsa", uslsa_cfattachinit },
	{ "uchcom", uchcom_cfattachinit },
	{ "usscanner", usscanner_cfattachinit },
	{ "atu", atu_cfattachinit },
	{ "upgt", upgt_cfattachinit },
	{ "usmsc", usmsc_cfattachinit },
	{ "ural", ural_cfattachinit },
	{ "rum", rum_cfattachinit },
	{ "utoppy", utoppy_cfattachinit },
	{ "zyd", zyd_cfattachinit },
	{ "udl", udl_cfattachinit },
	{ "uhso", uhso_cfattachinit },
	{ "urndis", urndis_cfattachinit },
	{ "urtwn", urtwn_cfattachinit },
	{ "urtw", urtw_cfattachinit },
	{ "run", run_cfattachinit },
	{ "udsir", udsir_cfattachinit },
	{ "bthub", bthub_cfattachinit },
	{ "bthidev", bthidev_cfattachinit },
	{ "btkbd", btkbd_cfattachinit },
	{ "btms", btms_cfattachinit },
	{ "btmagic", btmagic_cfattachinit },
	{ "btsco", btsco_cfattachinit },
	{ "sdmmc", sdmmc_cfattachinit },
	{ "sbt", sbt_cfattachinit },
	{ "ieee1394if", ieee1394if_cfattachinit },
	{ "fwip", fwip_cfattachinit },
	{ "sbp", sbp_cfattachinit },
	{ "acpi", acpi_cfattachinit },
	{ "acpiec", acpiec_cfattachinit },
	{ "acpiecdt", acpiecdt_cfattachinit },
	{ "acpilid", acpilid_cfattachinit },
	{ "acpibut", acpibut_cfattachinit },
	{ "acpiacad", acpiacad_cfattachinit },
	{ "acpibat", acpibat_cfattachinit },
	{ "acpivga", acpivga_cfattachinit },
	{ "acpiout", acpiout_cfattachinit },
	{ "acpifan", acpifan_cfattachinit },
	{ "acpitz", acpitz_cfattachinit },
	{ "acpiwdrt", acpiwdrt_cfattachinit },
	{ "acpidalb", acpidalb_cfattachinit },
	{ "ipmi_acpi", ipmi_acpi_cfattachinit },
	{ "acpiwmi", acpiwmi_cfattachinit },
	{ NULL, NULL }
};

const short cfroots[] = {
	140 /* mainbus0 */,
	-1
};

/* pseudo-devices */

const struct pdevinit pdevinit[] = {
	{ cpuctlattach, 1 },
	{ rndattach, 1 },
	{ vcodaattach, 1 },
	{ putterattach, 1 },
	{ nsmbattach, 1 },
	{ ataraidattach, 1 },
	{ cryptoattach, 1 },
	{ swcryptoattach, 1 },
	{ bioattach, 1 },
	{ ccdattach, 1 },
	{ cgdattach, 1 },
	{ raidattach, 1 },
	{ fssattach, 1 },
	{ mdattach, 1 },
	{ vndattach, 1 },
	{ bpfilterattach, 1 },
	{ carpattach, 1 },
	{ loopattach, 1 },
	{ pppattach, 1 },
	{ pppoeattach, 1 },
	{ slattach, 1 },
	{ stripattach, 1 },
	{ irframettyattach, 1 },
	{ tunattach, 1 },
	{ tapattach, 1 },
	{ greattach, 1 },
	{ gifattach, 1 },
	{ ipsecifattach, 1 },
	{ stfattach, 1 },
	{ vlanattach, 1 },
	{ bridgeattach, 1 },
	{ agrattach, 1 },
	{ l2tpattach, 1 },
	{ npfattach, 1 },
	{ accf_dataattach, 1 },
	{ accf_httpattach, 1 },
	{ ptyattach, 1 },
	{ sequencerattach, 1 },
	{ clockctlattach, 1 },
	{ ksymsattach, 1 },
	{ lockstatattach, 1 },
	{ bcspattach, 1 },
	{ btuartattach, 1 },
	{ wsmuxattach, 1 },
	{ wsfontattach, 1 },
	{ padattach, 1 },
	{ drvctlattach, 1 },
	{ veriexecattach, 1 },
	{ 0, 0 }
};
